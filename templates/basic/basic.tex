% Depending on the size of your thesis, "report" might be more appropriate as it
% provides "\chapter"
\documentclass{article}

% === packages =================================================================

\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[babel=true]{csquotes}

\usepackage{geometry}
\usepackage{enumitem}
\usepackage{booktabs}

\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage[mleftright]{diffcoeff}
\usepackage{tikz-cd}
\usepackage[locale=US]{siunitx}

\usepackage{tikz}
\usepackage[style=alphabetic-verb]{biblatex}
\usepackage[hidelinks]{hyperref}
\usepackage[english]{cleveref}

% === page settings ============================================================

\pagestyle{headings}
\geometry{scale=.666, marginratio=1:1, heightrounded}

% === theorem-like environments ================================================

% "Theorem" environments (bold header, italic body)
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}

% "Definition" environments (bold header, upright body)
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{example}[theorem]{Example}
\newtheorem{notation}[theorem]{Notation}

% "Remark" environments (italic header, upright body)
\theoremstyle{remark}
\newtheorem*{note}{Note}

% define reference name for "\cref{...}"
\crefname{notation}{Notation}{Notations}

% === math settings ============================================================

% equations are numbered with preceding section
\counterwithin{equation}{section}

% operators
\DeclareMathOperator{\End}{End} % endomorphisms
\DeclareMathOperator{\Hom}{Hom} % homomorphisms
\NewDocumentCommand{\id}{}{\mathrm{id}} % identity
\NewDocumentCommand{\Z}{}{\mathbb{Z}} % integers
\NewDocumentCommand{\Q}{}{\mathbb{Q}} % rational numbers
\NewDocumentCommand{\R}{}{\mathbb{R}} % real numbers
\NewDocumentCommand{\C}{}{\mathbb{C}} % complex numbers
\NewDocumentCommand{\Cf}{}{\mathcal{C}} % continuous functions
\NewDocumentCommand{\D}{}{\mathrm{D}} % Differential operator
\let\Re\relax
\DeclareMathOperator{\Re}{Re} % real part
\let\Im\relax
\DeclareMathOperator{\Im}{Im} % imaginary part
\DeclareMathOperator{\tr}{tr} % trace
\DeclareMathOperator{\rk}{rk} % rank
\DeclareMathOperator{\Span}{span} % span
\NewDocumentCommand{\tp}{}{t} % transpose
\DeclareMathOperator{\GL}{\mathrm{GL}} % general linear group

% commands
\NewDocumentCommand{\close}{m}{\overline{#1}} % closure

% delimiters
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\Set}{\lbrace}{\rbrace}
\DeclarePairedDelimiter{\pn}{\lparen}{\rparen} % generic parentheses
\DeclarePairedDelimiter{\bk}{\lbrack}{\rbrack} % generic brackets

% "diffcoeff" options
\difdef{f, fp}{}{
    long-var-wrap=dv,
    outer-Ldelim=\left., % Settings for evaluation at a point
    outer-Rdelim=\right\rvert,
    sub-nudge=0mu
}
\difdef{s, sp, c, cp}{}{
    long-var-wrap=dv,
}

% === list settings ============================================================

\setlist[enumerate, 1]{
    label=\textup{(\roman*)},
    leftmargin=*
}
\setlist[enumerate, 2]{
    label=\textup{(\alph*)},
    leftmargin=*
}

% === bibliography settings ====================================================

\ExecuteBibliographyOptions{
    sorting=anyvt, % sort according to label
    maxbibnames=99, % list all authors in bibliography
}
\nocite{*} % include all references, even if not cited in text
\addbibresource{ref.bib} % .bib file

% Pro tip: many publication sites have a "cite as" button or similar where you
% can copy-paste the BibTeX information into your .bib file

% === miscellaneous ============================================================

% add "\clearpage" before each "\section" (even with star)
\AddToHook{cmd/section/before}{\clearpage}

% pro hack: add "htbp" and "\centering" to each float
\makeatletter
\renewcommand*{\fps@figure}{htbp}
\renewcommand*{\fps@table}{htbp}
% Quite upsetting: Why is there no hook for the *end* of the environment start?
% Hence this weird command "\@floatboxreset".
\AddToHook{cmd/@floatboxreset/after}{\centering}
\AddToHook{cmd/@floatboxreset/after}{\centering}
\makeatother

% title page: I think it is better to type this explicitly and to not specify
% variables as the various text elements can become very long (it's all about
% the look)
\NewDocumentCommand{\MakeTitlepage}{}{
    \begin{titlepage}
        \centering
        {\Large\scshape % faculty
            Faculty of Mathematics and Natural Sciences at\\
            the Rhenish Friedrich Wilhelm University of Bonn\\
        }

        \vspace{10ex} % thesis type
        {\large\itshape Bachelor's Thesis\\ }

        \vspace{2ex} % title
        \rule{.9\textwidth}{1.2pt}\\
        {\Huge\bfseries
            A Successful Attempt\\
            at Creating a Title Page\\
        }

        \vspace{1ex}
        \rule{.4\textwidth}{.8pt}\\

        \vspace{1ex} % subtitle
        {\Large\bfseries
            (Or maybe not)\\
        }
        \rule[-.333\baselineskip]{.9\textwidth}{1.2pt}\\

        \vspace{5ex}
        {\itshape by\\}
        \vspace{1ex} % author
        {\Large\scshape Myself\\}

        \vspace{2ex} % data
        \textit{Born on} April~1, 1900 \textit{in} Bonn, Germany\\


        \vfill
        First advisor\\ % advisors
        {\Large\scshape Emmy Noether~(PhD)\\}
        \vspace{3ex}
        Second advisor\\
        {\Large\scshape Prof.~Dr.\ David Hilbert\\}

        \vspace{6ex} % institute
        {\Large\itshape Mathematical Institute\\}
        \vspace{1ex}
        Endenicher Allee~60, 53115 Bonn\\

        \vspace{3ex} % submission date
        {\large December~24, 2000}
    \end{titlepage}
}

% standard title
\title{A successful attempt at creating a title page}
\author{Myself\thanks{University of Bonn, Mathematical Institute}}
\date{\today}

\begin{document}

\MakeTitlepage

\maketitle

A~normal title instead of a whole title page is also possible.
This looks more like published papers on e.\,g.\ arXiv.

\begin{abstract}
    Amazing.
\end{abstract}

\tableofcontents

\section{First section}

\subsection{First subsection}

\subsubsection{First sub-subsection}

Some text.
\begin{equation*}
    a^2 + b^2 = c^2.
\end{equation*}

\begin{theorem}[{Euler-Lagrange equation, see \href{https://en.wikipedia.org/wiki/Euler\%E2\%80\%93Lagrange_equation}{Wikipedia}}]
    Let \((X,L)\) be a real dynamical system with \(n\)~degrees of freedom.
    Here \(X\) is the configuration space and \(L = L(t,\boldsymbol{q},\boldsymbol{v})\) the \emph{Lagrangian}, i.\,e.\ a smooth real-valued function such that \(\boldsymbol{q} \in X\), and \(\boldsymbol{v}\)~is an \(n\)-dimensional \enquote{vector of speed}.
    (For those familiar with differential geometry, \(X\)~is a smooth manifold, and \(L \colon \R_t \times TX \to \R\), where \(TX\) is the tangent bundle of~\(X\)\@.)

    Let \(\mathcal{P}(a,b, \boldsymbol{x}_a, \boldsymbol{x}_b)\) be the set of smooth paths \(\boldsymbol{q} \colon [a,b] \to X\) for which \(\boldsymbol{q}(a) = \boldsymbol{x}_a\) and \(\boldsymbol{q}(b) = \boldsymbol{x}_b\).
    The \emph{action functional} \(S \colon \mathcal{P}(a,b, \boldsymbol{x}_a, \boldsymbol{x}_b) \to \R\) is defined via
    \begin{equation*}
        S[\boldsymbol{q}]
        \coloneqq \int_a^b L(t, \boldsymbol{q}(t), \boldsymbol{q}'(t)) \dl{t}.
    \end{equation*}
    Then a path \(\boldsymbol{q} \in \mathcal{P}(a,b, \boldsymbol{x}_a, \boldsymbol{x}_b)\) is a stationary point of~\(S\) if and only if
    \begin{equation}
        \boxed{
            \diffp{L}{q^i} (t, \boldsymbol{q}(t), \boldsymbol{q}'(t))
            - \diff{}{t} \diffp{L}{(q')^i} (t, \boldsymbol{q}(t), \boldsymbol{q}'(t))
            = 0, \quad
            i = 1, \dots, n.
        }
        \label{eqn}
    \end{equation}
    Here, \(\boldsymbol{q}'(t) = \difs{\boldsymbol{q}(t)}{t} = \difc{\boldsymbol{q}(t)}{t}\) is the time derivative of~\(\boldsymbol{q}(t)\).
    When we say \emph{stationary point}, we mean a stationary point of~\(S\) w.\,r.\,t.\ any small perturbation in~\(\boldsymbol{q}\).
\end{theorem}

The \cref{eqn} is very important.

\begin{theorem}[five lemma]
    Consider the following commutative diagram with exact rows in the category of modules \(\mathsf{Mod}_A\) over a commutative ring~\(A\):
    \begin{equation*}
        \begin{tikzcd}
            M_1 \ar[r] \ar[d, "f_1"]
            & M_2 \ar[r] \ar[d, "f_2"]
            & M_3 \ar[r] \ar[d, dashed, "f_3"]
            & M_4 \ar[r] \ar[d, "f_4"]
            & M_5 \ar[d, "f_5"] \\
            N_1 \ar[r]
            & N_2 \ar[r]
            & N_3 \ar[r]
            & N_4 \ar[r]
            & N_5.
        \end{tikzcd}
    \end{equation*}

    \begin{enumerate}
        \item If \(f_2\)~and~\(f_4\) are epimorphisms and if \(f_5\) is a monomorphism, then \(f_3\) is an epimorphism.
        \item If \(f_2\)~and~\(f_4\) are monomorphisms and if \(f_1\) is an epimorphism, then \(f_3\) is a monomorphism.
    \end{enumerate}
    In fact, this holds over any abelian category~\(\mathcal{A}\), not only \(\mathsf{Mod}_A\).
\end{theorem}

\begin{proof}
    Diagram chase.
    For the general case, one can use \emph{Mitchell's embedding theorem}.
\end{proof}

\begin{theorem}[Krull's theorem]
    \label{thm:krull}
    Any non-zero commutative unital ring~\(A\) has a maximal ideal~\(\mathfrak{m}\).
\end{theorem}

\begin{note}
    \Cref{thm:krull} uses the \emph{axiom of choice}.
    In fact, it is equivalent to it.
\end{note}

\begin{proposition}
    The following statements are equivalent:
    \begin{enumerate}
        \item 
        \item 
        \item 
    \end{enumerate}
\end{proposition}

\begin{proof}
    This can be found in \cite[ch.~I, thm.~999]{sga1}.
\end{proof}

\begin{table}
    \begin{tabular}{ccc} \toprule
        Some entries & Its data & Other data \\ \midrule
        1 & a & b \\
        2 & c & d \\ \bottomrule
    \end{tabular}
    \caption{Random data.}
    \label{tab}
\end{table}

\begin{figure}
    \begin{tikzpicture}
        \foreach \i in {0,...,5} {
            \draw[very thick] ({\i * 360/5 + 90}:2) -- ({(\i+3) * 360/5 + 90}:2);
        }
        \draw[dashed] (0,0) circle[radius=2];
    \end{tikzpicture}
    \caption{Some picture.}
    \label{fig}
\end{figure}

\begin{remark}
    Some random units: \qty{-2.5}{\cm} or \qty{2.634e-19}{\ohm}, but also \qty{12}{\degree} and \qty{97.99999}{\percent}.
    Random \cref{tab,fig}.
\end{remark}

\section{New section}

This starts on a new page.
To avoid this, comment out the line:
\begin{quote}
    \verb|\AddToHook{cmd/section/before}{\clearpage}|
\end{quote}

\begin{theorem}
    Even theorem numbering resets.
\end{theorem}

\appendix

\section{Appendix section}

\printbibliography

\end{document}

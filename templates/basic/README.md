# `basic` template

This basic template is meant to be a quickstart or to serve as an inspiration
for writing a thesis in *LaTeX* (or any document, really).
Essentially, this is a package list with exemplifying settings that I *strongly*
recommend.
These packages will probably reflect most packages that I will explain in the
upcoming LaTeX course at the beginning of April 2024.
A [compiled PDF](https://gargantuar314.gitlab.io/latex/templates/basic/basic.pdf)
of the provided example PDF is also available.

## Complaints

In light of the upcoming LaTeX course, some students at my university already
think that it happens way to late and are "pressing" me on a LaTeX template I
could provide.
Hence, in a hurry, I created one which could hopefully help someone.

In my opinion, one should really attend my course first and then start to TeX in
order to avoid many pitfalls while TeX-ing: the internet has sometimes dubious,
sometimes deprecated solutions.
Moreover, the given solutions might not be the correct one for your given
situation, since LaTeX works very differently from other typesetting programs
like Microsoft Word, LibreOffice Writer or Apple pages.

And to defend the late date of the LaTeX course: I am personally very
flexible regarding the date, but thinking that many students still have exams in
March or that they are on vacation, I opted for April, one week before the next
semester starts.

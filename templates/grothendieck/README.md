# `grothendieck` package

This package is my attempt at recreating the design and layout of the first
volume of *Alexander Grothendieck*'s
[*Éléments de géométrie algébrique (EGA)*](http://www.numdam.org/item/?id=PMIHES_1960__4__5_0)
and
[*Séminaire de géométrie algébrique (SGA)*](https://arxiv.org/abs/math/0206203)
each.
As both designs differ in certain aspects, this template is obviously some
middle ground between the two.
I find this French style very pleasing (and others probably as well), so this
project is a must.

## Documentation

The [documentation](https://gargantuar314.gitlab.io/latex/templates/grothendieck/grothendieck.pdf)
is written with `grothendieck` since a document layout is best understood
visually.
To be even closer to EGA, I chose a modern extension of the font
*BaskervaldADF*, named *Baservaldx*, and changed mathematical notation with
the package `newtx` accordingly.
Note that if you want to have your document to look exactly like that, then you
must load these two packages, which are also explained in the introduction of
the documentation.
Lastly, the documentation contains a short section of EGA, but typed with
`grothendieck`.

## Installation

To use this package, simply copy (or clone) the file `grothendieck.sty` to your
working directory.
Then use it like any other LaTeX package, i.e. use

```latex
\usepackage[<options>]{grothendieck}
```

in the preamble.

To have `grothendieck` available globally for any project you already have or
will create in the future, you can add this to your `$TEXMFHOME` directory.
To determine the variable `$TEXMFHOME`, run:

```shell
kpsewhich -var-value TEXMFHOME
```

In most cases, this will `~/texmf/`.
All files put into `$TEXMFHOME` must abide by the *TeX Directory Structure*.
Hence create the directory

```shell
`$TEXMFHOME/tex/latex/`
```

and save `grothendieck.sty` into this newly created directory.
I would recommend to create a subdirectory like `./grothendieck/` and to save
the file in that subdirectory.
Then you are ready to go.
For more information, visit the official
(TeXLive documentation)<https://tug.org/texlive/doc/texlive-en/texlive-en.html#x1-350003.4.6>.

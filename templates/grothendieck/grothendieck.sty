\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{grothendieck}[2024-02-25 EGA/SGA-inspired layout]

% === Options ==================================================================

\newlength{\blocksep} % Vertical space between theorems
\newlength{\headersep} % Horizontal separation for various things
\DeclareKeys{
    blocksep.store      = \input@blocksep,
    headersep.store     = \input@headersep,
    enumega.if          = @enumega
}
\SetKeys{
    blocksep=\topsep,
    headersep=.5em
}
\ProcessKeyOptions\relax
\setlength{\blocksep}{\input@blocksep}
\setlength{\headersep}{\input@headersep}

% === Loading packages =========================================================

\RequirePackage{fancyhdr}
\RequirePackage[explicit]{titlesec}
\RequirePackage{titletoc}
\RequirePackage{enumitem}
\RequirePackage{amsthm}
\@ifundefined{abstract}{}{
    \RequirePackage[runin]{abstract}
}
\RequirePackage{etoolbox} % For fixing some \addvspace problems in titlesec
\RequirePackage[user, hyperref]{zref}
\RequirePackage{hyperref}

% Left equation numbers if math package is loaded
\newcommand{\sga@mathleqno}[1]{
    \IfPackageLoadedTF{#1}{
        \IfPackageLoadedWithOptionsTF{#1}{leqno}{}{
            \PackageWarning{grothendieck}{Package `#1` loaded without option
                `leqno`. Does not adhere to EGA/SGA style.}
        }
    }{}
}
\sga@mathleqno{mathtools}
\sga@mathleqno{amsmath}

% === fancyhdr settings ========================================================

% For some reason, fancyhdr redefines \chaptermark. Reverting this definition.
\@ifundefined{chapter}{}{
    \renewcommand{\chaptermark}[1]{%
        \markboth{\MakeUppercase{%
            \ifnum \c@secnumdepth > -1
                \if@mainmatter
                    \chaptername\ \thechapter. \ %
                \fi
            \fi
        #1}}{}%
    }
}

% Defining header (depending on "twoside") and clearing footer
\pagestyle{fancy}
\if@twoside
    \fancyhead[EL, OR]{\thepage}
    \fancyhead[ER, OL]{}
    \fancyhead[EC]{\leftmark}
    \fancyhead[OC]{\rightmark}
\else
    \fancyhead[L]{\rightmark}
    \fancyhead[R]{\thepage}
\fi
\renewcommand{\headrulewidth}{0pt}
\fancyfoot{}

% Title pages and chapter pages use "plain" page style
\fancypagestyle{plain}{ \fancyhf{} }

% === titlesec settings ========================================================

% Fixing \addvspace problem in titlesec. Courtesy to
% <https://tex.stackexchange.com/questions/37258/>.
\patchcmd{\ttl@straight@ii}{\vspace{\@tempskipb}}{\vskip \@tempskipb}{}{}

% Numbered chapters
\@ifundefined{chapter}{}{
    \titleformat{\chapter}[display]
        {\normalfont\Large\filcenter}
        {\MakeUppercase{\chaptertitlename~\thechapter}}
        {\baselineskip}
        {\LARGE\bfseries\MakeUppercase{#1}}
    \renewcommand{\thechapter}{\Roman{chapter}}
}

% Unnumbered chapters
\@ifundefined{chapter}{}{
    \titleformat{name=\chapter, numberless}[display]
        {\normalfont\Large\filcenter}
        {}
        {0pt}
        {\MakeUppercase{#1}}
}

% Sections
\titleformat{\section}[hang]
    {\normalfont\large\bfseries\filcenter}
    {§~\thesection.}
    {\headersep}
    {\MakeUppercase{#1}}
\@ifundefined{chapter}{}{
    \counterwithout{section}{chapter}
    \counterwithin*{section}{chapter}
}

% Subsections
\titleformat{\subsection}[hang]
    {\normalfont\bfseries}
    {\thesubsection.}
    {\headersep}
    {#1.}

% Sub-subsections. This style was inferred from the SMF.
\titleformat{\subsubsection}[hang]
    {\normalfont\itshape}
    {\thesubsubsection.}
    {\headersep}
    {#1.}

% === titletoc settings ========================================================

% Horizontal space for page numbers. Enough for three digits
\newlength{\@tocpagemargin}
\settowidth{\@tocpagemargin}{\hspace{\headersep}000}
\contentsmargin{\@tocpagemargin}

% Horizontal space for section numbers. Depends on existence of chapters.
\newlength{\@tocsepsec} % Minimum space for section number
\newlength{\@tocindentsec} % Indent from left border for sections
\@ifundefined{chapter}{
    \settowidth{\@tocsepsec}{§~00.00.0\hspace{\headersep}}
    \setlength{\@tocindentsec}{\@tocsepsec}
}{
    \settowidth{\@tocsepsec}{§~00.00\hspace{\headersep}}
    \setlength{\@tocindentsec}{1.5\@tocsepsec}
}
\newlength{\@tocskip}
\setlength{\@tocskip}{\baselineskip}

% Chapters
\@ifundefined{chapter}{}{
    \titlecontents{chapter}
        [0pt]
        {\addvspace{\@tocskip}\normalfont}
        {\contentspush{\textsc{\chaptertitlename~\thecontentslabel.}%
            \hspace{\headersep}---\hspace{\headersep}}\textbf}
        {\textsc}
        {\titlerule*[\headersep]{.}\contentspage}
}

% Sections
\newcommand{\formattocsec}[1]{\@ifundefined{chapter}{\textbf{#1}}{#1}}
\titlecontents{section}
    [\@tocindentsec]
    {\addvspace{\@ifundefined{chapter}{1}{.5}\@tocskip}\normalfont}
    {\contentslabel[§~\thecontentslabel.]{\@tocsepsec}\formattocsec}
    {\formattocsec}
    {\titlerule*[\headersep]{.}\contentspage}

% Subsections
\titlecontents{subsection}
    [\@tocindentsec]
    {\addvspace{\@ifundefined{chapter}{.5}{0}\@tocskip}\normalfont}
    {\contentslabel[\phantom{§}~\thecontentslabel.]{\@tocsepsec}}
    {}
    {\titlerule*[\headersep]{.}\contentspage}

% Sub-subsections
\titlecontents{subsubsection}
    [\@tocindentsec]
    {\normalfont}
    {\contentslabel[\phantom{§}~\thecontentslabel.]{\@tocsepsec}}
    {}
    {\titlerule*[\headersep]{.}\contentspage}

% === enumitem settings ========================================================

% New defaults
\setlist[enumerate]{
    align=left,
    wide,
    labelsep=\headersep,
    topsep=.5\blocksep,
    %after={\addvspace{.5\blocksep}},
    parsep=0pt,
    itemsep=0pt
}
\setlist[enumerate, 2]{
    wide=2\parindent
}
\setlist[itemize]{
    align=left,
    wide,
    labelsep=\headersep,
    topsep=.5\blocksep,
    parsep=0pt,
    itemsep=0pt
}
\setlist[itemize, 2]{
    wide=2\parindent
}
\setlist[enumerate, 1]{
    label=\textnormal{(\roman*)},
    ref=\textnormal{\roman*}
}
\setlist[itemize, 1]{
    label=\textnormal{--}
}
\newlist{enumerate*}{enumerate*}{1}
\setlist[enumerate*]{
    label=\textnormal{(\roman*)},
    ref=\textnormal{\roman*}
}
\newlist{itemize*}{itemize}{1}
\setlist[itemize*]{
    label=\textnormal{--}
}

% Fixing the problem that \topsep is ignored if list starts in vertical mode
% (e.g. nested lists).
\AddToHook{env/enumerate/after}{\addvspace{.5\blocksep}}

% Other list types. Use "\emph" if "enumega" option is set.
\if@enumega
    \SetEnumitemKey{a}{ label=\emph{\alph*)} }
    \SetEnumitemKey{1}{ label=\emph{\arabic*)} }
\else
    \SetEnumitemKey{a}{ label=\textnormal{\alph*)} }
    \SetEnumitemKey{1}{ label=\textnormal{\arabic*)} }
\fi

% Removes indentation of first item. Use it as a key to any list.
% Intended for theorem environments starting with a list.
\SetEnumitemKey{noind}{before*={
    \AddToHookNext{cmd/@item/before}{\addtolength{\itemindent}{-\parindent}}
    \AddToHookNext{cmd/@item/after}{\addtolength{\itemindent}{\parindent}}
}}
\SetEnumitemKey{valign}{ itemindent=* }

% Fixing the problem that optional labels in enumerate environments cannot be
% referenced.
\NewCommandCopy{\@tmp@item}{\item}
\RenewDocumentCommand{\item}{ !o }{%
    \IfValueTF{#1}{%
        \@tmp@item[#1]
            \if@nmbrlist
                \protected@edef\@currentlabel{#1}
            \fi
    }{%
        \@tmp@item
    }
}

% === amsthm settings ========================================================

\ProvideDocumentCommand{\theoremname}{}{Theorem}
\ProvideDocumentCommand{\lemmaname}{}{Lemma}
\ProvideDocumentCommand{\corollaryname}{}{Corollary}
\ProvideDocumentCommand{\propositionname}{}{Proposition}
\ProvideDocumentCommand{\definitionname}{}{Definition}
\ProvideDocumentCommand{\remarkname}{}{Remark}
\ProvideDocumentCommand{\examplename}{}{Example}

% Theorem-like style (italic body)
\newtheoremstyle{sgatheorem}
    {\blocksep}{\blocksep}
    {\itshape}{}
    {\bfseries\itshape}{.\hspace{\headersep}---}
    {\headersep}
    {}
\theoremstyle{sgatheorem}
\newtheorem{theorem}{\theoremname}[subsection]
\newtheorem{lemma}[theorem]{\lemmaname}
\newtheorem{corollary}[theorem]{\corollaryname}
\newtheorem{proposition}[theorem]{\propositionname}
\newtheorem{definition}[theorem]{\definitionname}

% Remark-like style (normal body)
\newtheoremstyle{sgaremark}
    {\blocksep}{\blocksep}
    {}{}
    {\bfseries\itshape}{.\hspace{\headersep}---}
    {\headersep}
    {}
\theoremstyle{sgaremark}
\newtheorem{remark}[theorem]{\remarkname}
\newtheorem*{remark*}{\remarkname}
\newtheorem{example}[theorem]{\examplename}

% Numbered paragraphs.
% Can be provided with different header via optional argument.
\newtheoremstyle{sgaparagraph}
    {\blocksep}{\blocksep}
    {}{}
    {\bfseries}{.}
    {\headersep}
    {\thmnote{#3 }\thmnumber{\textup{#2}}}
\theoremstyle{sgaparagraph}
\newtheorem{numpara}[theorem]{}

% Altered definition of proof environment according to style
\RenewDocumentEnvironment{proof}{ O{\proofname} }{\par
    \setlength{\labelsep}{0pt}
    \setlength{\topsep}{6pt plus 6pt}
    \pushQED{\qed}%
    \normalfont
    \begin{trivlist}
        \@tmp@item[\itshape #1\@addpunct{.\hspace{\headersep}---}\hspace{\headersep}]
            \ignorespaces
}{
    \popQED
    \end{trivlist}%
    \@endpefalse
}

% Interface for defining new theorem-like environments
\NewDocumentCommand{\NewTheorem}{ s m m }{
    \theoremstyle{sgatheorem}
    \IfBooleanTF{#1}{
        \newtheorem*{#2}{#3}
    }{
        \newtheorem{#2}[theorem]{#3}
    }
}

% Interface for defining new definition-like environments
\NewDocumentCommand{\NewRemark}{ s m m }{
    \theoremstyle{sgaremark}
    \IfBooleanTF{#1}{
        \newtheorem*{#2}{#3}
    }{
        \newtheorem{#2}[theorem]{#3}
    }
}

% === Referencing settings =====================================================

% NEW
\zref@newprop{theorem}{\thetheorem}
\zref@newprop{chapter}{\thechapter}
\zref@newprop{type}{\@currentcounter} % Only for differentiating enumi from theorem
\zref@addprops{main}{theorem, chapter, type}
\NewCommandCopy{\orig@label}{\label}
\renewcommand{\label}[1]{%
    \zlabel{#1}%
    \orig@label{#1}%
}
\newcommand{\check@type}[2]{\equal{#2}{\zref@extract{#1}{type}}} % key, type
\newcommand{\check@change}[2]{\equal{\csname the#2\endcsname}{\zref@extract{#1}{#2}}} % key, type
\NewDocumentCommand{\g@itmref}{sm}{\begingroup%
    \newcommand{\temp@reftext}{%
        \@ifundefined{chapter}{}{%
            \ifthenelse{\check@change{#2}{chapter}}{}
                {\zref[chapter]{#2}, }%
        }%
        \ifthenelse{\check@change{#2}{chapter} \AND \check@change{#2}{theorem}}{}
            {\zref[theorem]{#2}, }%
        \ifthenelse{\check@change{#2}{chapter} \AND \check@change{#2}{theorem}}
            {\ref*{#2}}{(\ref*{#2})}%
    }%
    (\IfBooleanTF{#1}{\temp@reftext}{\hyperref[#2]{\temp@reftext}})%
\endgroup}
\NewDocumentCommand{\g@thmref}{sm}{\begingroup%
    \newcommand{\temp@reftext}{%
        \@ifundefined{chapter}{}{%
            \ifthenelse{\check@change{#2}{chapter}}{}
                {\zref[chapter]{#2}, }%
        }%
        \ref*{#2}%
    }%
    (\IfBooleanTF{#1}{\temp@reftext}{\hyperref[#2]{\temp@reftext}})%
\endgroup}
% Sections and chapters must be done by hand as to account for plural versions.
\NewDocumentCommand{\gref}{sm}{%
    \ifthenelse{\check@type{#2}{theorem}}{%
        \IfBooleanTF{#1}{\g@thmref*{#2}}{\g@thmref{#2}}%
    }{\ifthenelse{\check@type{#2}{enumi} \OR \check@type{#2}{enumii}}{%
        \IfBooleanTF{#1}{\g@itmref*{#2}}{\g@itmref{#2}}%
    }{%
        \IfBooleanTF{#1}{\ref*{#2}}{\ref{#2}}%
    }}%
}

% === Other settings ===========================================================

% Adjust abstract environment to style
\@ifundefined{abstract}{}{
    \setlength{\abstitleskip}{-\parindent}
    \abslabeldelim{.\hspace{\headersep}---\hspace{\headersep}}
    \renewcommand{\abstractnamefont}{\normalfont\small\bfseries\itshape}
}

% Footnote number is put in parentheses
%\renewcommand{\thefootnote}{(\arabic{footnote})}
\renewcommand{\footnoterule}{%
    \kern -3pt
    \hrule width .5in
    \kern 2.6pt
}
\newcommand{\@footnote@wrap}[1]{\textsuperscript{\normalfont(}#1\textsuperscript{\normalfont)}}
\renewcommand{\footnote}{%
    \@ifnextchar[\@xfootnote{\stepcounter\@mpfn
     \protected@xdef\@thefnmark{\thempfn}%
     \@footnote@wrap{\@footnotemark}\@footnotetext}}
\newlength{\g@footskip}
\setlength{\g@footskip}{\parindent}
\renewcommand{\@makefntext}[1]{%
    \noindent\makebox[1.2\g@footskip][r]{\@footnote@wrap{\@makefnmark}}\hspace*{\headersep}#1%
}

% Equation numbers depend on theorem counter
\counterwithin{equation}{theorem}

\ProvideDocumentCommand{\phantomsection}{}{}
\AddToHook{env/thebibliography/before}{
    \phantomsection
    \@ifundefined{chapter}
    {\addcontentsline{toc}{section}{\bibname}}
    {\addcontentsline{toc}{chapter}{\bibname}}
}

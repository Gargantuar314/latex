# `latex`

## Content

This repository contains various LaTeX files that I created.
As of now, this will mainly be the home for three projects:

+ The LaTeX course that I will hold from the 2nd to the 6th of April 2024 at the
  University of Bonn, which will be in English.
  The presentation's PDF as well as code will be published
  [here](https://gitlab.com/Gargantuar314/latex/-/tree/main/2024_SS_course).
+ Some LaTeX templates I created in my spare time.
  At the moment, these are:
  + [`basic`](https://gitlab.com/Gargantuar314/latex/-/tree/main/templates/basic)
    (published):
    A basic template which, in my opinion, contains a pretty "minimal" setup for
    writing a thesis.
    Of course, this will not be sufficient at all for your particular thesis,
    but it might serve as a good starting point for your preamble.
  + `wagner` (in development):
    These two templates try to recreate *Ferdinand Wagner*'s amazing style used
    for his [lecture notes](https://github.com/FlorianAdler/AlgebraBonn) and
    [master's thesis](https://guests.mpim-bonn.mpg.de/ferdinand/q-deRham.pdf),
    but without using *KOMA-Script* classes.
    I personally hate KOMA-Script classes as they are really inflexible when it
    comes to more low-level hacking and altering how KOMA-Script works.
  + [`grothendieck`](https://gitlab.com/Gargantuar314/latex/-/tree/main/templates/grothendieck)
    (published):
    This template tries to be a compromise between the designs found in
    *Alexander Grothendieck*'s
    [*Éléments de géométrie algébrique (EGA)*](http://www.numdam.org/item/?id=PMIHES_1960__4__5_0)
    and
    [*Séminaire de géométrie algébrique (SGA)*](https://arxiv.org/abs/math/0206203),
    in particular the first volume of each series.
    Many algebraic geometers will encounter these works and probably find this
    French style aesthetically very pleasing (me included).
+ A collection of interesting drawings and diagrams I created over the years
  during my studies at university, which can be found
  [here](https://gitlab.com/Gargantuar314/latex/-/tree/main/tikz).
  I am by no means an authoritative person when it comes to Ti*k*Z, PGFPlots or
  Asymptote, and this collection is more like a bunch of hacks.

## Licenses

Regarding licenses, look at the respective projects/templates, as they might be
licensed differently.

## Contributing

Any comments, suggestions and corrections can be submitted via
[Issues](https://gitlab.com/Gargantuar314/latex/-/issues) directly on this
project site.
And if you like, you can support me by awarding me a star 😄 (top right corner).

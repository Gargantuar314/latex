# Collection of advanced drawings

This project is a collection of interesting, more advanced drawings I made
during my studies at university.
They mostly use *TikZ*, but there is also one *PGFPlots* and one *Asymptote*
picture.
Note that I am by no means an expert or an authoritative person on Ti*k*Z,
PGFPlots or Asymptote.

The code for each group of pictures is contained in a separate `.tex` file.
Some comments on the pictures, both mathematically as well as programmatically,
are documented [here](https://gargantuar314.gitlab.io/latex/tikz/tikz.pdf).

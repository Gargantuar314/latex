\documentclass{article}

\usepackage[T1]{fontenc}
\usepackage[british]{babel}
\usepackage{lmodern}
\usepackage{csquotes}

\usepackage[heightrounded, marginratio=1:1, scale=.666]{geometry}

\usepackage{mathtools}
\usepackage{amssymb}

\immediate\write18{mkdir figures}
\usepackage{tikz}
\usetikzlibrary{external, math, calc}
\tikzexternalize[prefix=figures/]
\usepackage{pgfplots}
\pgfplotsset{
    compat=1.18,
    colormap/cool
}
\usepackage[inline]{asymptote}
%\renewcommand{\asylatexdir}{figures}
%\def\asydir{figures}

\usepackage{hyperref}

\pagestyle{headings}

\makeatletter
\renewcommand*{\fps@figure}{p}
\AddToHook{cmd/@floatboxreset/after}{\centering}
\makeatother

\AddToHook{cmd/part/before}{\clearpage}
\AddToHook{cmd/section/before}{\clearpage}

\newcommand{\TikZ}{Ti\textit{k}Z}

\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}

\title{A collection of advanced drawings}
\author{Tien Nguyen Thanh}
\date{1st May 2024}

\begin{document}

\maketitle

\begin{abstract}
    My goal with this short document is to collect interesting and advanced drawings I made during my time at university.
    They look really cool and are mostly based on loops, one of my most favourite features in \TikZ.
    Some examples will have short comments, describing the pictures itself or describing features of \TikZ{} (or other drawing facilities).

    I~am by no means a \TikZ{} expert or would call these examples good code.
    They are mostly more like hacks that I collected over the years.
    Please refer to the PGF or \TikZ{} manual for more accurate info; the explanations will certainly be incomplete or even wrong.
\end{abstract}

\paragraph{To do}
\begin{itemize}
    \item \TikZ{} \verb|externalize| crops the arrow tips since they do not belong to the current bounding box.
    \item Asymptote version~2.89 creates gaps in surfaces.
    \item Asymptote, latexmk and \TikZ{} \verb|externalize| do not work well together.
        Setting \verb|\asydir| will not compile properly.
        Asymptote is run by latexmk on files in the auxiliary subdirectory \verb|figures/| as well.
\end{itemize}

\tableofcontents

\part{\texorpdfstring{\TikZ}{TikZ}}

\section{Circles in \texorpdfstring{\(L^p\)}{Lp} space}

\paragraph{Source file}
\href{run:./lp.tex}{\texttt{lp.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{tikz}|
\end{quote}

\paragraph{Description}
I~created this during my analysis~II course, for which I was paid.
It describes the unit circle, i.\,e.\ a two-dimensional sphere, with respect to an \(L^p\)-norm for \(p \ge 1\), which is defined by
\begin{equation*}
    \norm{\cdot}_p \colon \mathbb{R}^2 \to \mathbb{R}_{\ge 0}, \qquad
    (x_1, x_2)_p \mapsto (\abs{x_1}^p + \abs{x_2}^p)^{1/p}.
\end{equation*}
In the special case of \(p = 1\), we obtain the \enquote{Taxicab norm}, and in the case of \(p = \infty\), we obtain the \emph{maximum norm}, which determines the maximum among the~\(\abs{x_i}\).
The case \(p = 2\) agrees with the usual Euclidean norm.

\paragraph{Implementation}
The naive implementation via
\begin{quote}
    \verb|\draw[smooth, domain=-1:1] plot (\x, {(1 - abs(\x)^p)^(1/p)});|\\
    \verb|\draw[smooth, domain=-1:1] plot (\x, {-(1 - abs(\x)^p)^(1/p)});|
\end{quote}
where \verb|p| stands for the value of~\(p\) does not work very well.
The reason is that \TikZ{} tries to approximate \verb|a^b| by \(\exp(b \ln a)\) if \verb|b| is not an integer.
In our case, \verb|p|~and \verb|1/p| are generally not integers, so this applies.
So if \verb|\x| approaches~\verb|1|, we essentially calculate \verb|0^(1/p)|.
However, the magnitude of \(\ln 0\) becomes very large, so large that \TikZ{} cannot handle this.
As a consequence, the right side of the circle cannot touch the \(x\)-axis, whatever the number of samples is.

The key is to use polar coordinates, similar to drawing a circle.
Namely, the set of points \((x_1, x_2)\) satisfying
\begin{equation*}
    (\abs{x_1}^p + \abs{x_2}^p)^{1/p} = 1
    \iff
    \abs{x_1}^p + \abs{x_2}^p = 1
\end{equation*}
can be described by
\begin{equation*}
    \{ (\pm \cos^{2/p} \alpha, \pm \sin^{2/p} \alpha), (\pm \cos^{2/p} \alpha, \mp \sin^{2/p} \alpha) \mid \alpha \in [0, 90] \}.
\end{equation*}
This set is described as such since \(\ln\) in \TikZ{} cannot accept negative arguments.
For the same reason, only values \(p \in (1, \infty)\) are allowed.

\begingroup
\input{files/lp}
\endgroup

\section{Two-dimensional vector fields}

\paragraph{Source file}
\href{run:./vecfield.tex}{\texttt{vecfield.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{tikz}|\\
    \verb|\usetikzlibrary{math, calc}|
\end{quote}

\paragraph{Description}
I~created this during my analysis~II course, for which I was paid.
A~two-dimensional real \emph{vector field} is a function \(v \colon \mathbb{R}^2 \to \mathbb{R}^2\).
Usually, they are depicted by drawing an arrow from \(x \in \mathbb{R}^2\) to~\(v(x)\), but scaling down the length to make it more readable.
Other ways to encode the magnitude is to use colours, or to discard this information all together.
We have chosen the latter option.

\paragraph{Implementation}
We used the \verb|calc| library in order to scale down the length of the arrows to a fixed length of \verb|.4cm|.
The points are drawn through two \verb|for| loops, and the \verb|if| statement is used to exclude arrows at stationary points, which are drawn upwards by default.

\begingroup
\input{files/vecfield}
\endgroup

\section{Bruhat-Tits tree for \texorpdfstring{\(\mathrm{GL}_2\)}{GL2}}

\paragraph{Source file}
\href{run:./bruhattits.tex}{\texttt{bruhattits.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{tikz}|\\
    \verb|\usetikzlibrary{math}|
\end{quote}

\paragraph{Description}
I~made this in the course of my Bachelor's thesis.
This represents the so-called \emph{Bruhat-Tits tree} of \(\mathrm{GL}_2(K)\) of some \(p\)-adic local field~\(K\).
The theory is not important at this point.
The only property we need is that the Bruhat-Tits tree of \(\mathrm{GL}_2(K)\) is a \((q+1)\)-regular infinite tree, where \(q\) is the cardinality of the residue field \(k = \mathcal{O}_K / \pi_K \mathcal{O}_K\), which is always a prime power.

\paragraph{Implementation}
We make use of functions in the \verb|math| library in order to create a recursive definition.
One challenge was to name nodes uniquely.
In the end, we use the \((q+1)\)-nary number system and represent each node by a unique integer.
Expressed correctly, each number is a sequence of digits \(\{1, \dots, q\}\), except for one branch, where the numbers are negative.
The digit~\(0\) is avoided as adding~\(0\) to some number will not create a new unique number, so if we would not revert to one negative branch, multiple nodes with number~\(0\) are possible.

\begingroup
\input{files/bruhattits}
\endgroup

\section{Cantor function}

\paragraph{Source file}
\href{run:./cantor.tex}{\texttt{cantor.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{tikz}|\\
    \verb|\usetikzlibrary{math}|
\end{quote}

\paragraph{Description}
This was created during my analysis~I course.
The \emph{Cantor function}, also known as the \emph{Devil's staircase}, is a famous counterexample in analysis.
It is uniformly continuous and almost everywhere constant, yet it is still increasing.
The construction is done by separating the interval \([0,1]\) in thirds and drawing a line in the middle third \([\frac{1}{3}, \frac{2}{3}]\).
Then the construction is repeated recursively on the two outer thirds \([0, \frac{1}{3}]\) and \([\frac{2}{3}, 1]\).

\paragraph{Implementation}
This is again a recursive definition using \verb|math|.
The implementation is straight forward: draw the middle third, and call the function twice with the appropriate bounding boxes again.
What I have not done is to connect the endpoints of the horizontally drawn lines, which is why this implementation relies on a high number of iterations.

\begingroup
\input{files/cantor}
\endgroup

\section{Discrete dynamic systems}

\paragraph{Source file}
\href{run:./dynamic.tex}{\texttt{dynamic.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{tikz}|\\
    \verb|\usetikzlibrary{math, calc}|
\end{quote}

\paragraph{Description}
This was an assignment during my linear algebra~I course.
The goal was to visualise orbits of certain \(2 \times 2\)-matrices~\(A\) over~\(\mathbb{R}\).
These matrices were the following:
\begin{enumerate}
    \item Let
        \begin{equation*}
            A =
            \begin{pmatrix}
                \lambda_1 \\
                & \lambda_2
            \end{pmatrix}
            \qquad \text{with} \qquad
            \lambda_1 > \lambda_2 > 0.
        \end{equation*}
        There are five cases:
        \begin{enumerate}
            \item \emph{Nodal expansion:} \(\lambda_1 > \lambda_2 > 1\).
            \item \emph{Linear expansion:} \(\lambda_1 > 1 = \lambda_2\).
            \item \emph{Hyperbolic transformation:} \(\lambda_1 > 1 > \lambda_2\).
            \item \emph{Linear contraction:} \(\lambda_1 = 1 > \lambda_2\).
            \item \emph{Nodal contraction:} \(1 > \lambda_1 > \lambda_2\).
        \end{enumerate}
    \item Let
        \begin{equation*}
            A =
            \begin{pmatrix}
                \lambda \\
                & \lambda
            \end{pmatrix}
            \qquad \text{with} \qquad
            \lambda > 0.
        \end{equation*}
        There are three cases:
        \begin{enumerate}
            \item \emph{Expansive homothety:} \(\lambda > 1\).
            \item \emph{Identity:} \(\lambda = 1\).
            \item \emph{Contractive homothety:} \(\lambda < 1\).
        \end{enumerate}
    \item Let
        \begin{equation*}
            A =
            \begin{pmatrix}
                 \lambda & 1 \\
                 & \lambda
            \end{pmatrix}
            \qquad \text{with} \qquad \lambda > 0.
        \end{equation*}
        There are three cases:
        \begin{enumerate}
            \item \emph{Parabolic expansion:} \(\lambda > 1\).
            \item \emph{Shearing:} \(\lambda = 1\).
            \item \emph{Parabolic contraction:} \(\lambda < 1\).
        \end{enumerate}
    \item Let
        \begin{equation*}
            A =
            \begin{pmatrix}
                a & -b \\
                b & a
            \end{pmatrix}
            \qquad \text{with} \qquad
            b \ne 0.
        \end{equation*}
        There are three cases:
        \begin{enumerate}
            \item \emph{Spiral expansion:} \(\sqrt{a^2 + b^2} > 1\).
            \item \emph{Elliptic expansion:} \(\sqrt{a^2 + b^2} = 1\).
            \item \emph{Spiral contraction:} \(\sqrt{a^2 + b^2} < 1\).
        \end{enumerate}
\end{enumerate}

To visualise them, we chose twelve equidistant vectors on the unit circle (blue) and another twelve equidistant vectors on \(\frac{3}{2}\)~times the unit circle (red).
For each vector, the orbit is drawn for the first three steps (thick), and also the orbit of~\(A^{-1}\) is drawn for the first three steps (opaque).
To elevate the diagrams, we draw a vector field in the background where the magnitude of the arrows does not indicate anything, only the direction.

\paragraph{Implementation}
My implementation back then was terrible.
The reason is that functions defined with the \TikZ{} library \verb|math| cannot return more complex values like \verb|coordinate|.
Hence we need to provide a function for each coordinate.
To circumvent all limitations of \verb|math|, especially that functions cannot be redefined, we revert to defining \LaTeX{} commands, which can be defined inside \verb|tikzpicture| and can be used within \verb|\tikzmath| without issues.
The challenge was to avoid redundancy as far as possible.

\begingroup
\input{files/dynamic}
\endgroup

\part{PGFPlots}

\section{Surface of revolution}

\paragraph{Source file}
\href{run:./surface.tex}{\texttt{surface.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{pgfplots}|\\
    \verb|\pgfplotsset{compat=1.18, colormap/cool}|
\end{quote}

\paragraph{Description}
This picture was created during my analysis~II course, for which I was paid.
Given some curve \(f \colon [a,b] \to \mathbb{R}\) in the \(xy\)-plane, one can rotate this shape around the \(x\)-axis in order to create a surface which is rotation invariant.
These surfaces are fairly easy to describe, e.\,g.\ there is a closed formula for the surface area if \(f\) is integrable.

The following picture appeared in the context of the Euler-Lagrange equation in order to minimise the energy of a soap film in between two coaxial circles.
Solving the equation, the describing curve of the surface of revolution is of the form
\begin{equation*}
    f(t)
    = c \cosh \biggl( \frac{t}{c} \biggr)
\end{equation*}
for some \(c > 0\) where \(c\) depends on the distance of the two circles.

\paragraph{Implementation}
In order to orient the surface as depicted, the axes are rotated such that the \(z\)-axis points to the right and the \(x\)-axis points up; reason being that PGFPlots plots the value of functions on the \(z\)-axis.
Of course, we used polar coordinates.
Some effort was made in layering the drawn elements in the right order.
For instance, the right oval is drawn before the left one, and the bounding curve is split in two.
The horizontal axis is faked: we actually draw from the tip of the axis, through the nearest point of the surface from our perspective (which lies on the left oval) then to the origin.
These two lines lie in the \(yz\)-plane and actually form an angle; however, since the \(yz\)-line appears as one line from our perspective, this will look like one line.

\begingroup
\input{files/surface}
\endgroup

\part{Asymptote}

\section{Riemann surface of the square root}

\paragraph{Source file}
\href{run:./riemann.tex}{\texttt{riemann.tex}}

\paragraph{Requirements}
\begin{quote}
    \verb|\usepackage{asymptote}|
\end{quote}

\paragraph{Description}
This was created when I attended the course algebra~I.
The \enquote{function} \(z \mapsto \sqrt{z}\) on~\(\mathbb{C}\) is one of the most famous examples for \emph{ramification}.
In this case, each argument \(z \ne 0\) has precisely two possibilities for~\(\sqrt{z}\), producing a \(2\)-covering of~\(\mathbb{C}\), which itself is a Riemann surface.

To represent this four-dimensional data in three dimensions, we revert to using colours as one dimension.
This mean the following in the picture:
\begin{enumerate}
    \item The right horizontal axis depicts \(\Re z\) of the argument.
    \item The left horizontal axis depicts \(\Im z\) of the argument.
    \item The vertical axis depicts \(\Re \sqrt{z}\) of the possible values.
    \item The colour depicts \(\Im \sqrt{z}\) of the possible values.
\end{enumerate}

\paragraph{Implementation}
Actually, I~intended to use PGFPlots's \verb|wave| option to colour the surface depending on~\(z\).
However, it is not as flexible as I wanted: it cannot take arbitrary variables (in this case, parametrisation variables for polar coordinates for~\(z\)) for calculation, but it will take the \(x\)-~and \(y\)-coordinates of the arguments (in this case, Cartesian coordinates of~\(z\)).

Hence we use the more powerful Asymptote drawing library, which integrates nicely with \LaTeX.
I~have absolutely no knowledge about Asymptote.
The main idea is to create a \verb|surface|, and then equipping the surface with colour by means of a \verb|pen|.
Note that since the surface is a \(2\)-covering, we need to iterate through \(4 \pi\)'s worth of angles, here from \verb|-pi| to \verb|3pi|.

\paragraph{Caveat}
Compiling with Asymptote version~2.89, the surface is not properly closed and gaps appear.
I~reckon that this is a bug since this problem disappears if compiling with the previous version~2.88.

\tikzexternaldisable
\begingroup
\input{files/riemann}
\endgroup
\tikzexternalenable

\end{document}

\section{Formatting}

\begin{frame}[fragile]{Writing paragraphs}
    \begin{itemize}[<+->]
        \item Type as you normally would
        \item<.-> Leave blank line to start new paragraph
    \end{itemize}

    \begin{code}<+->
This is a sentence.
This is another sentence.

We start a new paragraph.
Just type ö ß é ô ñ š ø ł.
    \end{code}

    \begin{exampleblock}{Note}<+->
        Alternative for the above special characters:\\
        \lstinline|Just type \"o \ss{} \'e \^o \~n \v{s} \o{} \l.|
    \end{exampleblock}

    \begin{alertblock}{Rookie mistake}<+->
        \LaTeX{} replaces empty lines between paragraphs with \lstinline|\par|.\\
        \alert{Don't use \texttt{\textbackslash\textbackslash} to make paragraphs!}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Breaking lines and pages}
    \begin{alertblock}{Warning}<+->
        The following commands are evil!
        Use only if \alert{ABSOLUTELY} necessary.
    \end{alertblock}

    \begin{codeexp}<+->
        \lstinline|\\[§skip§]|
        \begin{itemize}
            \item Force new line (has other meanings in certain environments)
            \item \a{skip}: additional \emph{rubber space} after line break
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<.->
        \lstinline|\\*[§skip§]|
        \begin{itemize}
            \item Force new line, but prevent page break at this point
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\newpage|
        \begin{itemize}
            \item Force new page
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<.->
        \lstinline|\clearpage|
        \begin{itemize}
            \item Force new page and place all \emph{floats} (on extra pages if necessary)
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Mind paragraph breaks}
    \begin{code}
Then we have
\[ a^2 + b^2 = c^2, \]
which proves the theorem.
    \end{code}
    \hrulefill
    \begin{code}
Then we have
\[ a^2 + b^2 = c^2. \]

On the other hand, \dots
    \end{code}
    \hrulefill
    \begin{code}
\dots as desired.

\[ a^2 + b^2 = c^2 \]
describes another \dots
    \end{code}
\end{frame}

\begin{frame}[fragile]{Mind paragraph breaks (what's the difference?)}
    Outline \alert{logical} structure of text\\[\topsep]
    \begin{minipage}[t]{.5\textwidth}
    \begin{codealt}[listing above text]
Then we have
\[ a^2 + b^2 = c^2, \]
which proves the theorem.

    \end{codealt}
    \end{minipage}%
    \begin{minipage}[t]{.5\textwidth}
    \begin{codealt}[listing above text]
\dots as desired.

\[ a^2 + b^2 = c^2 \]
describes another \dots
    \end{codealt}
    \end{minipage}

    \vspace{-9.7ex}
    \hrulefill\\[1.75ex]
    \hrulefill\\[-2.4ex]
    \hrulefill\\[1.3ex]
    \hrulefill
\end{frame}

\begin{frame}[fragile]{Computer Modern font family}
    \lstinline|\usepackage{lmodern}|
    \begin{itemize}
        \item Latin Modern: high quality vector fonts for Computer Modern
    \end{itemize}

    \begin{block}{Fonts}<2->
        \begin{itemize}[<2->]
            \item Three font families
                \begin{enumerate}
                    \item Roman \lstinline|\textrm| (default)
                    \item Sans serif \lstinline|\textsf|
                    \item Typewriter \lstinline|\texttt|
                \end{enumerate}
        \end{itemize}
        \begin{minipage}[t]{.5\textwidth}
            \begin{itemize}[<3->]
                \item Each in one of two weights
                    \begin{enumerate}
                        \item Medium \lstinline|\textmd| (default)
                        \item Boldface \lstinline|\textbf|
                    \end{enumerate}
            \end{itemize}
        \end{minipage}%
        \begin{minipage}[t]{.5\textwidth}
            \begin{itemize}[<4->]
                \item Each in one of three shapes
                    \begin{enumerate}
                        \item Upshape \lstinline|\textup| (default)
                        \item Italics \lstinline|\textit|
                        \item Slanted \lstinline|\textsl|
                    \end{enumerate}
            \end{itemize}
        \end{minipage}
        \begin{itemize}[<5->]
            \item Only Roman: small caps \lstinline|\textsc| (can only be combined with slanted)
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Computer Modern font family}
    \begin{center}
        \begin{tabular}{@{}ccc@{}}\toprule
            Shape/series & \LaTeX & Output \\ \midrule
            (default) & \lstinline|Test| & \textrm{Test} \\
            bold & \lstinline|\textbf{Test}| & \textrm{\textbf{Test}} \\
            italics & \lstinline|\textit{Test}| & \textrm{\textit{Test}} \\
            bold italics & \lstinline|\textbf{\textit{Test}}| & \textrm{\textbf{\textit{Test}}} \\
            slanted & \lstinline|\textsl{Test}| & \textrm{\textsl{Test}} \\
            bold slanted & \lstinline|\textbf{\textsl{Test}}| & \textrm{\textbf{\textsl{Test}}} \\
            small caps & \lstinline|\textsc{Test}| & \textrm{\textsc{Test}} \\
            small caps slanted & \lstinline|\textsc{\textsl{Test}}| & \textrm{\textsc{\textsl{Test}}} \\
            sans serif & \lstinline|\textsf{Test}| & \textsf{Test} \\
            sans serif bold & \lstinline|\textsf{\textbf{Test}}| & \textbf{Test} \\
            sans serif italics & \lstinline|\textsf{\textit{Test}}| & \textit{Test} \\
            typewriter & \lstinline|\texttt{Test}| & \texttt{Test} \\
            typewriter bold & \lstinline|\texttt{\textbf{Test}}| & \texttt{\textbf{Test}} \\
            typewriter italics & \lstinline|\texttt{\textit{Test}}| & \texttt{\textit{Test}} \\ \bottomrule
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}[fragile]{Computer Modern font family}
    \begin{block}{Special commands}
        \begin{itemize}
            \item \lstinline|\textnormal{...}|: default font
            \item \lstinline|\emph{...}|: emphasise text
        \end{itemize}
    \end{block}

    \medskip
    \begin{code}[lefthand width=.5\textwidth]
Some \emph{emphasised} text.

\textit{Some \emph{emphasised} text.}
    \end{code}
\end{frame}

\section{Figures and tables}

\begin{frame}[fragile]{Floats}
    \begin{block}{The idea}<+->
        \begin{itemize}[<+->]
            \item Figures and tables cannot be broken across pages
            \item \LaTeX{} uses \alert{floats}
                \begin{itemize}
                    \item Automatic (optimal) placement amidst the surrounding text
                    \item Each float type has its own float queue
                    \item With each new page, \LaTeX{} tries to place some floats
                    \item For each type, \LaTeX{} respects the relative order of the floats
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{alertblock}{Warning}<+->
        Floats might appear anywhere!
        Do not expect them to appear where you want them to.
    \end{alertblock}

    \begin{exampleblock}{Strategy}<+->
        Set a \alert{caption} for each float and reference it in the text.
        Referencing will be explained later.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Floats}
    \begin{exampleblock}{Recall}<+->
        \begin{itemize}
            \item \lstinline|\newpage|: issue page break
            \item \lstinline|\clearpage|: issue page break and place all pending floats
            \item \lstinline|\cleardoublepage| (for \lstinline|twoside| class option): same as \lstinline|\clearpage|, but insert empty page (if necessary) so that new page is on the right side
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Float environment}
    \begin{actionenv}<+->
        \lstinline|\begin{§float§}[§pos§]|\\
        \tabstrut\a{content}\\
        \lstinline|\end{§float§}|
    \end{actionenv}
    \begin{itemize}[<+->]
        \item \a{float}: float type, either \lstinline|figure| or \lstinline|table|\\[\itemsep]
            other packages provide other float types, e.\,g.\ \lstinline|algorithm| from package \lstinline|algorithm|, \lstinline|lstlisting| from \lstinline|listings|
        \item \a{pos}: subset of string \lstinline|htbp| (default \lstinline|tbp|)\\[\itemsep]
            \LaTeX{} only tries positions in \a{pos} as placement options for float, order doesn't matter
            \begin{itemize}[<1->]
                \item \lstinline|h|:~\emph{here} (where source code is);\\
                    is by itself illegal, \lstinline|t|~automatically added
                \item \lstinline|t|:~\emph{top} (of page),
                \item \lstinline|b|:~\emph{bottom} (of page),
                \item \lstinline|p|:~\emph{page} (consisting of floats only)
            \end{itemize}
        \item \a{content}: literally anything
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Captions}
    \begin{codeexp}<+->
        \lstinline|\caption[§loftcaption§]{§caption§}|
        \begin{itemize}[<+->]
            \item \a{caption}: caption text\\
                centred if only one line, otherwise normal paragraph
            \item \a{loftcaption}: optional caption (default \a{caption})\\
                Often shorter text used for the \emph{list of figures\slash tables}
            \item Caption must be in float environment
            \item<.-> Caption text preceded by \enquote{Figure~\a{num}:} or \enquote{Table~\a{num}:}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\listoffigures|\\
        \lstinline|\listoftables|
        \begin{itemize}
            \item Often issued after \lstinline|\tableofcontents|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Figures}
    \begin{codeexp}<+->
        \lstinline|\begin{figure}[§pos§]|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{figure}|
        \begin{itemize}[<+->]
            \item Reasonable content for \a{\dots}:
                \begin{itemize}
                    \item \lstinline|\begin{tikzpicture}§\dots§\end{tikzpicture}|:\\
                        introduction to \TikZ{} later
                    \item \lstinline|\begin{asy}§\dots§\end{asy}|:\\
                        \lstinline|asymptote| is out of scope of these talks
                    \item \lstinline|\includegraphics{§\dots§}|:\\
                        from the package \lstinline|graphicx|
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\usepackage{graphicx}|
        \begin{itemize}
            \item Useful facilities for everything graphics related
            \item Automatically included by \TikZ
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Graphics}
    \begin{codeexp}<+->
        \lstinline|\includegraphics[§key-val§]{§file§}|
        \begin{itemize}[<+->]
            \item \a{file}: path of the to be included file relative to \emph{current working directory}
                \begin{itemize}
                    \item pdf\LaTeX{} only allows \verb|.pdf|, \verb|.png|, \verb|.jpg| files
                    \item<.-> Best practice: \a{file} contains no dots or spaces, no extension
                    \item Example: consider the document structure
                        \begin{lstlisting}[language=bash, identifierstyle=\color{black}]
                            file1.png
                            project/
                                main.tex
                                file2.jpg
                                dir1/dir2/
                                        file3.pdf
                        \end{lstlisting}
                        Then use
                        \begin{lstlisting}
                            \includegraphics{../file1}
                            \includegraphics{file2}
                            \includegraphics{dir1/dir2/file3}
                        \end{lstlisting}
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Graphics}
    \begin{codeexp}<+->
        \lstinline|\includegraphics[§key-val§]{§file§}|
        \begin{itemize}[<+->]
            \item \a{key-val}: all of the form \lstinline|§option§=§value§| (except \lstinline|clip|)
                \begin{itemize}
                    \item \lstinline|width| (type: length, default: true width)\\
                        E.\,g.\ \lstinline|width=6cm| or \lstinline|width=.8\textwidth|
                    \item \lstinline|height| (type: length, default: true height)
                    \item \lstinline|scale| (type: real number, default:~\lstinline|1|)\\
                        E.\,g.\ \lstinline|.2|, \lstinline|2.5| or \lstinline|-1| (central reflection)
                    \item \lstinline|angle| (type: real number, default:~\lstinline|0|)\\
                        Rotate \a{value} degrees anticlockwise
                    \item \lstinline|clip| (type: boolean, specify without \a{value}, default: false)\\
                        Clip graphic if it's too large, otherwise it will overlap with surroundings
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\graphicspath{ {§dir1§} {§dir2§} §\dots§ }|
        \begin{itemize}[<+->]
            \item \LaTeX{} searches through \a{dir1}, \a{dir2},~\dots{} for \a{file}
            \item Each \a{dirn} must be wrapped in braces, e.\,g.\ \lstinline|\graphicspath{{dir/}}|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Graphics}
    \begin{exampleblock}{Example (what I do)}
        \begin{itemize}
            \item Optional \a{pos} is almost always \lstinline|htb| or \lstinline|htbp|
            \item First line is almost always \lstinline|\centering|
            \item Last line is almost always \lstinline|\caption{§\dots§}|
        \end{itemize}
    \end{exampleblock}
    \begin{pdflist}[lefthand width=.55\textwidth, comment style={scale=2}, listing options={firstline=5,lastline=9}]
\documentclass{article}
\usepackage[papersize={4cm,4cm},scale=.9]{geometry}
\usepackage{graphicx}
\begin{document}
\begin{figure}[htbp]
  \centering
  \includegraphics[scale=.2]{../kirby}
  \caption{It's Kirby.}
\end{figure}
\end{document}
    \end{pdflist}

    \footnotetext{Source (plz don't sue me): \url{https://en.wikipedia.org/wiki/File:SSU_Kirby_artwork.png}.}
\end{frame}

\begin{frame}[fragile]{Tables}
    \begin{codeexp}<+->
        \lstinline|\begin{table}[§pos§]|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{table}|
    \end{codeexp}

    \begin{block}{\a{\dots} is (almost) always:}<+->
        \lstinline|\begin{tabular}[§pos§]{§colspec§}|\\
        \tabstrut\a{content}\\
        \lstinline|\end{tabular}|
        \begin{itemize}[<+->]
            \item Actual table content (\alert{attention:} \lstinline|tabular| vs.\ \lstinline|table|)
            \item \a{pos}: one of \lstinline|t| (top) or \lstinline|b| (bottom), default centre\\
                Specifies which part of the table is aligned with the baseline of the surrounding text (\alert{usually irrelevant})
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Tables}
    \begin{codeexp}<+->
        \lstinline|\begin{tabular}[§pos§]{§colspec§}|\\
        \tabstrut\a{content}\\
        \lstinline|\end{tabular}|
        \begin{itemize}[<+->]
            \item \a{colspec}: column specification; string can consists of:
                \begin{itemize}
                    \item \lstinline|c|,~\lstinline|l|,~\lstinline|r|:
                        centred\slash left\slash right aligned column
                    \item \lstinline+|+:
                        vertical bar
                    \item \lstinline|@{§colsep§}|:
                        put \a{colsep} between each column\\
                        E.\,g.\ \lstinline|@{ a }|, \lstinline|@{\hspace{2em}}|, \lstinline|@{}|
                    \item \lstinline|p{§width§}|:
                        column of width \a{width}
                    \item \lstinline|*{§num§}{§colspec§}|:
                        shorthand for \a{num} copies of \a{colspec}\\
                        E.\,g.\ \lstinline+|*{20}{c|}+ vs.\ \lstinline+|c|c|c|§\dots§|c|+
                \end{itemize}
            \item \a{content}: table data
                \begin{itemize}[<1->]
                    \item separate columns with ampersand~\lstinline|&|
                    \item separate lines with \lstinline|\\|
                    \item add horizontal line with \lstinline|\hline| after~\lstinline|\\|
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Table}
    \begin{exampleblock}{Example}
        \begin{enumerate}
            \item First line is (almost) always \lstinline|\centering|
            \item \lstinline|\caption{§\dots§}| should be placed above the table
        \end{enumerate}
    \end{exampleblock}

    \begin{pdflist}[lefthand width=.55\textwidth, comment style={scale=2}, listing options={firstline=5,lastline=13}]
\documentclass{article}
\usepackage[papersize={4cm,3cm}, scale=.9]{geometry}
\usepackage{caption}
\begin{document}
\begin{table}[htbp]
  \centering
  \caption{Old-school (bad) table.}
  \begin{tabular}{|l||r|} \hline
    I & 0 \\ \hline \hline
    A & 2.5 \\ \hline
    B & 12.2 \\ \hline
  \end{tabular}
\end{table}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Typographical remarks}
    \begin{enumerate}[<+->]
        \item For font\slash font size\slash colour consistency with the document, make drawings\slash diagrams\slash tables in \LaTeX{}
        \item Western reading habits dictate how captions should be placed
            \begin{itemize}
                \item Captions of figures should be placed \emph{below} the figure\\
                    (we generally look at pictures before we read any text)
                \item Captions of tables should be placed \emph{above} the table\\
                    (general information first, specific information later)
            \end{itemize}
        \item Standard \LaTeX{} document only support placing captions below figures\slash tables (otherwise wrong vertical spacing)\\
            \textrightarrow{} use package \lstinline|caption|
        \item Make text in figures not to small, make figures large enough
    \end{enumerate}

    \begin{exampleblock}{Note}<+->
        Checkout the package \lstinline|longtable| for tables longer than a page.
    \end{exampleblock}
\end{frame}

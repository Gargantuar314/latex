\section{Structuring the document}

\begin{frame}[fragile]{Sectioning}
    \begin{pdflist}[lefthand width=.36\textwidth, listing options={firstline=7,lastline=14}]
\documentclass{report}
\usepackage[a6paper,scale=.8]{geometry}

\begin{document}
\pagestyle{plain}

% in book class
\part{Title}
\chapter{Title}
\section{Title}
\subsection{Title}
\subsubsection{Title}
\paragraph{Title}
\subparagraph{Title}

\end{document}
    \end{pdflist}

    \begin{itemize}[<2->]
        \item \lstinline|chapter| not available for \lstinline|article|
        \item \lstinline|subsubsection| is only numbered in \lstinline|article|
        \item \lstinline|paragraph|, \lstinline|subparagraph| are unnumbered and \enquote{inlined} in text
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Sectioning}
    \begin{codeexp}
        \lstinline|\§secname§[§toctitle§]{§title§}|
        \begin{itemize}
            \item \a{secname}: section type
            \item \a{toctitle}: optional title (default \a{title})\\
                Will be used in table of contents
            \item \a{title}: title printed in document
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<2->
        \lstinline|\§secname§*{§title§}|
        \begin{itemize}
            \item Unnumbered and does not appear in table of contents
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{Warning}<3->
        There are complications with the package \lstinline|hyperref|.
    \end{alertblock}

    \begin{codeexp}<4->
        \lstinline|\tableofcontents|: table of contents
        \begin{itemize}
            \item Only contains numbered sectioning commands\\
                (class \lstinline|book| has some exceptions)
            \item Must be issued in document body
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Simple document title}
    The following appear in the preamble:
    \begin{itemize}[<+->]
        \item \lstinline|\title{§title§}|: title of document (mandatory)\\
            Can contain \lstinline|\\| to obtain line breaks
        \item \lstinline|\author{§name1§ \and §name2§ \and §\dots§}|: (mandatory)\\
            Each \a{namei} can contain \lstinline|\\| to have line breaks
        \item \lstinline|\thanks{§note§}|: footnote for any author (optional)\\
            Often for information on some\slash each author (e.\,g.\ mail, university)
        \item \lstinline|\date{§date§}|: (optional)\\
            \a{date} is often explicit or \lstinline|\today|
    \end{itemize}

    \begin{codeexp}<+->
        \lstinline|\maketitle|: print title\\
        \begin{itemize}
            \item Must be issued in document body
            \item Appears on own page if and only if \lstinline|titlepage| option used
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Simple document title}
    \begin{pdflist}[listing options={basicstyle=\tiny\ttfamily}, comment style={scale=2}]
\documentclass{article}

\usepackage[a6paper]{geometry}

\title{\bfseries
    Learning \LaTeX\\[.2\baselineskip]
    \normalsize (Maybe not)
}
\author{
    You\thanks{you@email.com}
    \\{\small Uni Bonn} \and
    I\thanks{i@email.com}
    \\{\small Uni Bonn}
}
\date{\today}

\begin{document}

\maketitle

The goal is to prove \dots

\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Advanced title page}
    \begin{codeexp}<+->
        \lstinline|\begin{titlepage}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{titlepage}|
        \begin{itemize}[<+->]
            \item Custom title page on dedicated page without page number
            \item Must be issued in document body
            \item \a{\dots} can be literally anything
            \item \alert{More advanced}: you should know what you are doing
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Advanced title page}
    \begin{pdflist}[listing options={basicstyle=\tiny\ttfamily}, comment style={scale=1.9}]
\documentclass{article}
\usepackage[a5paper]{geometry}
\begin{document}
\begin{titlepage} \centering
\vspace{2ex}
{\Large\scshape University Bonn}\\
Endenicher Allee~60, 53115 Bonn\\
\vspace{2ex} 
{\large\itshape Mathematical Institute\\}
\vspace{8ex}
{\large \itshape Thesis\\} \vspace{2ex}
\rule{1\textwidth}{.8pt}\\
{\Huge\bfseries A Successful Attempt\\
at Creating a Title Page\\} \vspace{1ex}
\rule{.4\textwidth}{.8pt}\\ \vspace{1ex}
{\Large\bfseries (Or maybe not)\\}
\rule[-.5\baselineskip]{1\textwidth}
    {.8pt}\\ \vspace{5ex}
{\Large\scshape From Me\\} \vfill \hfill
\parbox[t]{.45\textwidth}{\centering
1st Advisor\\ \Large\scshape Emmy Noether
} \hfill
\parbox[t]{.45\textwidth}{\centering
2nd Advisor\\ \Large\scshape David Hilbert
} \hspace*{\fill}\\ \vspace{10ex}
\today \vspace*{2ex}
\end{titlepage}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Appendix and friends}
    \begin{codeexp}<+->
        \lstinline|\appendix|: marks start of appendix
        \begin{itemize}
            \item Subsequent chapters\slash sections are numbered with letters
        \end{itemize}
    \end{codeexp}

    \begin{block}{In \lstinline|book|}<+->
        \begin{itemize}[<+->]
            \item \lstinline|\frontmatter|: Marks begin of front matter
                \begin{itemize}[<1->]
                    \item Typically for preface, introduction, table of contents, etc.
                    \item Pages numbered in small Roman numerals
                    \item Chapters unnumbered, but appear in table of contents
                \end{itemize}
            \item \lstinline|\mainmatter|: Marks begin of normal text
                \begin{itemize}[<1->]
                    \item Pages numbered with Arabic numbers
                    \item Chapters numbered as usual
                \end{itemize}
            \item \lstinline|\backmatter|: Marks begin of back matter
                \begin{itemize}[<1->]
                    \item Typically for index, bibliography, etc., \alert{excluding appendix}
                    \item Pages continue to be numbered in Arabic numbers
                    \item Chapters unnumbered, but appear in table of contents
                \end{itemize}
        \end{itemize}
    \end{block}
\end{frame}

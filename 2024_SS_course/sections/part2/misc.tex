\section{Miscellanea}

\begin{frame}[fragile]{Micro-typography}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{microtype}|
        \begin{itemize}[<+->]
            \item Enables \emph{micro-typography:} subtle typographical enhancements
            \item Makes the text more aesthetic and shorter
            \item \a{options}:
                \begin{itemize}[<1->]
                    \item \lstinline|babel| (default \lstinline|false|): enable \lstinline|babel| support
                    \item Many more advanced options in the \lstinline|microtype| docs
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Special characters}
    \begin{block}{Special characters}<+->
        \begin{table}
            \begin{tabular}{@{}|*{9}{r|}l|@{}} \hline
                \lstinline|#| & \lstinline|%| & \lstinline|$| & \lstinline|^| & \lstinline|&| & \lstinline|_| & \lstinline|{| & \lstinline|}| & \lstinline|~| & \lstinline|\| \\ \hline
                \lstinline|\#| & \lstinline|\%| & \lstinline|\$| & \lstinline|\^| & \lstinline|\&| & \lstinline|\_| & \lstinline|\{| & \lstinline|\}| & \lstinline|\~| & \lstinline|\textbackslash| \\ \hline
            \end{tabular}
        \end{table}
        Top: special characters; bottom: escape sequence
    \end{block}

    \begin{exampleblock}{Exercise}<+->
        What special characters do you recognise?\\
        Why is \lstinline|\textbackslash| the escape sequence for~\lstinline|\|?
    \end{exampleblock}

    \begin{actionenv}<+->
        \emph{Solution}\\
        So far: \lstinline|\|,~\lstinline|{|,~\lstinline|}| in commands, \lstinline|&|~in tabular, \lstinline|%|~for comments.\\
        The command \lstinline|\\| is already reserved for line breaks.
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Dashes and hyphens}
    \begin{itemize}
        \item \lstinline|-| (short hyphen): use for compound words
        \item \lstinline|--| (\emph{en} dash): use for range of numbers
        \item \lstinline|---| (\emph{em} dash): use to set of source of citation
        \item Use en dash with enclosing spaces or em dash without spaces for parenthetical constructions, thought pauses, interruptions
    \end{itemize}

    \begin{code}<2->
finite-dimensional

pages 2--14

---Peter Scholze
    \end{code}

    \begin{alertblock}{Warning}<3->
        Writing \lstinline|finite-dimensional| deletes all hyphenation breakpoints!
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Other stuff}
    \begin{itemize}
        \item \lstinline|\dots|: \emph{ellipsis} (\enquote{dot-dot-dot})
        \item \lstinline|\slash|: breakable slash
        \item \lstinline|\LaTeXe|, \lstinline|\LaTeX|, \lstinline|\TeX|, \lstinline|\today|
    \end{itemize}

    \medskip
    \begin{actionenv}<2->
        \begin{code}
\LaTeXe, \LaTeX, \TeX

\today

a, ..., z; a, \dots, z
        \end{code}
    \end{actionenv}

    \medskip
    \begin{actionenv}<3->
        \hbadness=10000%
        \begin{code}
% Warning: `Underfull \hbox (badness 6284)`
Going to school by foot and/or by bus.

Going to school by foot and\slash or by bus.
        \end{code}
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Parsing rules~II}
    \begin{exampleblock}{Exercise}<+->
        Why \lstinline|\LaTeXe| and not \lstinline|\LaTeX2e|?
        What would \lstinline|\LaTeX2e| output?
    \end{exampleblock}

    \begin{actionenv}<+->
        \emph{Solution}\\
        Command names are either single characters or a string of letters.
        \lstinline|\LaTeX2e| results in \textrm{\LaTeX 2e}, not \textrm{\LaTeXe}.
    \end{actionenv}

    \medskip
    \begin{exampleblock}{Exercise* (\alert{important})}<+->
        \hbadness=10000
        \begin{code}
\LaTeXe, \LaTeX and \TeX are cool.
        \end{code}
        Explain the spacing.
        How can we fix this?
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Parsing rules~II}
    \begin{actionenv}<+->
        \emph{Solution}\\
        Commands like \lstinline|\cmd| gobble the following white space (regardless of number of arguments).
        To prevent this, \lstinline|\cmd| must read a group which does nothing.
        This is the \alert{empty group}~\lstinline|{}|.
        \begin{code}
\LaTeXe, \LaTeX{} and \TeX{} are cool.
        \end{code}
    \end{actionenv}

    \medskip
    \begin{actionenv}<+->
        \emph{Alternative***}\\
        \lstinline|\cmd| can read an \emph{explicit space} \lstinline[showspaces=true]|\ |, or \lstinline|\cmd| is inside a group so that it reads a \emph{group end} \lstinline|}| (or \lstinline|\egroup| in plain \TeX).
        \begin{code}
\LaTeXe, \LaTeX\ and {\TeX} are cool.
        \end{code}
        Note that \lstinline|{ \TeX }| instead of \lstinline|{\TeX}| is wrong.
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Quoting text}
    \begin{codeexp}<+->
        \lstinline|\begin{quotation}|\\
        \tabstrut\a{text}\\
        \lstinline|\end{quotation}|
        \begin{itemize}[<+->]
            \item \a{text} is typed with wider margins
            \item<.-> First line of each paragraph indented (like normal paragraphs)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\begin{quote}|\\
        \tabstrut\a{text}\\
        \lstinline|\end{quote}|
        \begin{itemize}[<+->]
            \item Same as \lstinline|quotation|, except that the first line of paragraphs is not indented; instead, gap between paragraphs is increased
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Better quotations}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{csquotes}|
        \begin{itemize}[<+->]
            \item Automatic handling of (nested) quotations
            \item Supports \lstinline|babel|
            \item For \a{options}, see the \lstinline|csquotes| docs (none is good enough)
            \item For direct quotations as in the humanities, see the \lstinline|csquotes| docs
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\enquote{§text§}|
        \begin{itemize}
            \item Put quotation marks around \a{text}, can be nested (default two levels)
            \item Depends on \lstinline|babel| language
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\enquote{Text.}

\enquote{Text \enquote{text} Text.}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Abstract}
    \begin{codeexp}<+->
        \lstinline|\begin{abstract}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{abstract}|
        \begin{itemize}[<+->]
            \item Only for document classes \lstinline|report|, \lstinline|article|
            \item<.-> With \lstinline|titlepage| option: is placed on dedicated page
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Footnotes}
    \begin{codeexp}<+->
        \lstinline|\footnote[§label§]{§text§}|
        \begin{itemize}[<+->]
            \item \a{text}: text of footnote
            \item<.-> \a{label}: custom number or symbol for footnote\\
                If specified, number for footnotes is not increased
            \item \LaTeX{} puts restrictions on \lstinline|\footnote|
                \begin{itemize}[<+->]
                    \item Can't use in \lstinline|tabular| \textrightarrow{} use package \lstinline|threeparttablex|
                    \item Can't use in \lstinline|\footnote| \textrightarrow{} use package \lstinline|bigfoot|
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{Tip}<+->
        \lstinline|\footnote| and paragraph text should not have white space in between.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Margin notes}
    \begin{codeexp}<+->
        \lstinline|\marginpar[§left§]{§right§}|
        \begin{itemize}[<+->]
            \item Puts \a{right} on \enquote{right margin}, \a{left} on opposite margin
            \item Vertically aligns the first line of \a{right}\slash \a{left} with the paragraph line it was declared on
            \item With \lstinline|onesided| class option, \a{right} is on the right;\\
                with \lstinline|twosided|, \a{right} is on the outer margin;\\
                with \lstinline|twocolumn|, \a{right} is on the nearest margin
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{Warning}<+->
        \lstinline|\marginpar| can \enquote{fall off} the lower page border.
        Either reformulate the text surrounding \lstinline|\marginpar| or shorten \a{right}.
    \end{alertblock}

    \begin{alertblock}{Tip}<+->
        Works best in \emph{horizontal mode}, i.\,e.\ place \lstinline|\marginpar| in between paragraph text, not e.\,g.\ directly after paragraph break or \lstinline|\item|.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Footnotes and margin notes}
    \begin{exampleblock}{Example}
        The percent sign~\lstinline|%| prevents white space.
        More about this later.
    \end{exampleblock}
    \begin{pdflist}[lefthand width=.45\textwidth, comment style={scale=2}, listing options={firstline=5, lastline=10}]
\documentclass{article}
\pagestyle{empty}
\usepackage[showframe, papersize={6cm,4cm}, left=.5cm, right=2cm, marginparsep=.5cm]{geometry}
\begin{document}
Some text.%
\footnote{A footnote.}
Some other text.
\marginpar{Margin.}
More text.
\footnote{Two footnotes.}
\end{document}
    \end{pdflist}
\end{frame}

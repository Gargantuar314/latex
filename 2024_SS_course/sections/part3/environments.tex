\section{Math environments}

\begin{frame}{Math environments}
    \begin{block}{The goal: fill in 5~environments}
        \begin{center}
            \begin{tabular}{@{}w{c}{6em}!{\vrule width \heavyrulewidth}w{c}{6em}|w{c}{6em}|w{c}{6em}@{}}
                & \multicolumn{2}{c|}{Single equation}
                & \multirow{2}{=}{\makecell{Equation\\ group}}
                \\ \cline{2-3}
                & single line
                & multiple lines
                &
                \\ \specialrule{\heavyrulewidth}{0pt}{0pt}
                \makecell{Without\\ alignment}
                & 
                &
                &
                \\ \hline
                \makecell{With\\ alignment}
                & ---
                &
                &
            \end{tabular}
        \end{center}
    \end{block}
\end{frame}

\begin{frame}[fragile]{General structure of math environments}
    \begin{codeexp}<+->
        \lstinline|\begin{§mathenv§}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{§athenv§}|
        \begin{itemize}[<+->]
            \item Prints equations in display math mode
            \item Use \lstinline|&|~and~\lstinline|\\| to specify alignment and break points resp.,
                depending on \a{mathenv} (similar, but not identical to \lstinline|tabular|)
            \item \alert{Tag}: number of one equation line, by default to the right
            \item For very long lines, tag is placed directly under the line (if possible)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\begin{§mathenv§*}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{§athenv§*}|
        \begin{itemize}
            \item Untagged version of \a{mathenv}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\texttt{equation}: single equation, single line}
    \begin{codeexp}<+->
        \lstinline|\begin{equation}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{equation}|
        \begin{itemize}
            \item Tag on the same line
            \item If line is very long, content is cramped 
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=6, lastline=9}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\begin{equation}
    a + b + c + d + e
    = f + g + h + i
\end{equation}
\end{document}
    \end{pdflist}

    \begin{exampleblock}{Note}<+->
        \lstinline|amsmath| redefines \lstinline|\[§\dots§\]| to be \lstinline|equation*|
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\texttt{multline}: Single equation, multiple lines, without alignment}
    \begin{codeexp}<+->
        \lstinline|\begin{multline}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{multline}|
        \begin{itemize}[<+->]
            \item First line left-aligned, last line right-aligned (both with indent), all other lines centred
            \item \lstinline|\\|~to set manual break points
            \item Tag on last line
            \item If line is very long, content is cramped (see \lstinline|equation|)
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-3\baselineskip}
\begin{multline}
    a \\
    b c d \\
    e \\
    f
\end{multline}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\texttt{multline}: single equation, multiple lines, with alignment}
    \begin{codeexp}<+->
        \lstinline|\shoveleft{§line§}|\\
        \lstinline|\shoveright{§line§}|
        \begin{itemize}[<+->]
            \item \a{line}: entire line in \lstinline|multline| except \lstinline|\\|
            \item Shifts entire line to the left/right
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-3\baselineskip}
\begin{multline}
    a \\
    \shoveright{b c d} \\
    \shoveleft{e} \\
    f
\end{multline}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\texttt{equation} + \texttt{split}: single equation, multiple lines, without alignment}
    \begin{codeexp}<+->
        \lstinline|\begin{split}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{split}|
        \begin{itemize}[<+->]
            \item Provides a single alignment point via \emph{ampersand}~\lstinline|&|
            \item Has to be used in other environments, e.\,g.\ \lstinline|equation|, treated like one line
            \item Has no tag \textrightarrow{} no \lstinline|split*| environment
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=13}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-\baselineskip}
\begin{equation}
    \begin{split}
        a + b
        &= c \\
        &= d + e
    \end{split}
\end{equation}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\texttt{split} options}
    \a{options} for \lstinline|\usepackage[§options§]{§mathtools§}|

    \begin{codeexp}<+->
        \lstinline|centertags| (default)
        \begin{itemize}
            \item With \lstinline|split|, vertically centres the tag
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|tbtags|
        \begin{itemize}
            \item With \lstinline|split|, places the tag at the top\slash bottom (top with \lstinline|leqno| option)
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\texttt{gather}: equation group, without alignment}
    \begin{codeexp}<+->
        \lstinline|\begin{gather}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{gather}|
        \begin{itemize}[<+->]
            \item Each line is centred
            \item \lstinline|\\|~to mark break points
            \item Provides tag for each line
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=14}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{gather}
    a + b
    = c \\
    d
    = e + f \\
    g
    = h
\end{gather}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\texttt{align}: equation group, with alignment}
    \begin{codeexp}<+->
        \lstinline|\begin{align}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{align}|
        \begin{itemize}[<+->]
            \item \lstinline|\\|~to mark break points
            \item Each line must have the \alert{same odd} number of~\lstinline|&|
                \begin{itemize}[<1->]
                    \item Odd: alignment point
                    \item Even: column separator (stretchable space)
                \end{itemize}
            \item Provides tag for each line
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-\baselineskip}
\begin{align}
    a + b &= c &
    d &= e \\
    d &= e + f &
    g &= h
\end{align}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\texttt{alignat}: \texttt{align} variant}
    \begin{codeexp}<+->
        \lstinline|\begin{alignat}{§numcols§}|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{alignat}|
        \begin{itemize}[<+->]
            \item \a{numcols}: number of \enquote{equation columns}
            \item Identical to \lstinline|align|, except that no space is inserted between columns\\
                \textrightarrow~specify space yourself
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-\baselineskip}
\begin{alignat}{3}
    a + b &= c, & \qquad
    d &= e, \\
    d &= e + f, &
    g &= h
\end{alignat}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Math environments}
    \begin{block}{Summary}<+->
        \begin{center}
            \begin{tabular}{@{}w{c}{6em}!{\vrule width \heavyrulewidth}w{c}{6em}|w{c}{6em}|w{c}{6em}@{}}
                & \multicolumn{2}{c|}{Single equation}
                & \multirow{2}{=}{\makecell{Equation\\ group}}
                \\ \cline{2-3}
                & single line
                & multiple lines
                &
                \\ \specialrule{\heavyrulewidth}{0pt}{0pt}
                \makecell{Without\\ alignment}
                & \lstinline|equation|
                & \lstinline|multline|
                & \lstinline|gather|
                \\ \hline
                \makecell{With\\ alignment}
                & ---
                & \makecell{\lstinline|equation|\\ +~\lstinline|split|}
                & \makecell{\lstinline|align|\\ (\lstinline|alignat|)}
            \end{tabular}
        \end{center}
    \end{block}

    \begin{exampleblock}{Remark}<+->
        Many use for \enquote{single equation, multiple lines, with alignment} \lstinline|align| instead of the correct \lstinline|equation| + \lstinline|split|
    \end{exampleblock}

    \begin{exampleblock}{Typographical remark}<+->
        Why should I use \lstinline|equation| instead of, say, always \lstinline|align|?\\
        Better vertical spacing before\slash after the equations.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Typographical remark on breaking equations}
    \begin{itemize}[<+->]
        \item Align multiple equations along (main) relation symbol
        \item Break long equation primarily before relation symbol, secondarily before \(+\),~\(-\),~\(\pm\)
        \item Align \(+\)~and~\(-\) behind relation symbols or behind opening delimiter
        \item Add \alert{empty Ord}~\lstinline|{}| between relation and~\lstinline|&| for correct spacing
    \end{itemize}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=14}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{equation*}
    \begin{split}
        a
        =& b + c \\
        ={}& b + c \\
        &+ d + e
    \end{split}
\end{equation*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Math environment \enquote{building blocks}}
    \begin{codeexp}<+->
        \lstinline|\begin{§mathenved§}[§valign§]|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{§mathenved§}|
        \begin{itemize}[<+->]
            \item \a{mathenved}: \lstinline|gathered|, \lstinline|aligned|, \lstinline|alignedat|, \lstinline|multlined|\alert{\dag}
            \item \a{valign}: vertical alignment with surrounding lines, either \lstinline|t|~top, \lstinline|c|~centre (default), \lstinline|b|~bottom
            \item Must be used in other math environment \textrightarrow~\alert{rarely useful}
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=6, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={6cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\begin{equation*}
    \left. \begin{aligned}
    a &= \alpha, \\
    b &= \beta,
    \end{aligned} \right\}
    \text{cool equations}
\end{equation*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Interrupting math environments}
    \begin{codeexp}<+->
        \lstinline|\intertext{§text§}|\\
        \lstinline|\shortintertext{§text§}|\alert{\dag}
        \begin{itemize}[<+->]
            \item Interrupt math environment with normal text \a{text}
            \item \lstinline|\shortintertext| has less space before and after
            \item Useful in \lstinline|align| to keep alignment
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-\baselineskip}
We have
\begin{align}
    a &= b + c \\
    \shortintertext{and}
    d + e &= f.
\end{align}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Interrupting math environments}
    \begin{codeexp}<+->
        \lstinline|\displaybreak|\\
        \lstinline|\allowdisplaybreak|
        \begin{itemize}[<+->]
            \item By default, math environments do not break across pages
            \item \lstinline|\displaybreak|: allow break after single line, must go before~\lstinline|\\|
            \item \lstinline|\allowdisplaybreak|: allow break after every line\\
                \alert{\textrightarrow~not recommended}
            \item Don't work for \lstinline|split|, \lstinline|aligned|, \lstinline|gathered|, \lstinline|multlined|\alert{\dag}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Ellipsis for equation groups}
    \begin{exampleblock}{Problem}<+->
        We like to use \lstinline|\vdots| for \emph{ellipsis} of equation groups.
        Aligning \lstinline|\vdots| looks bad
    \end{exampleblock}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=11}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{align*}
    a &= b \\
    &\vdots \\
    c &= d
\end{align*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Ellipsis for equation groups}
    \begin{codeexp}<+->
        \lstinline|\vdotswithin{§rel§}|\alert{\dag}\\
        \begin{itemize}
            \item \a{rel}: relation symbol
            \item Increases the width of \lstinline|\vdots| to the width of \a{rel}
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=11}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{align*}
    a &= b \\
    &\vdotswithin{=} \\
    c &= d
\end{align*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Ellipsis for equation groups}
    \begin{codeexp}<+->
        \lstinline|\MTFlushSpaceAbove|\alert{\dag}\\
        \lstinline|\MTFlushSpaceBelow|\alert{\dag}
        \begin{itemize}[<+->]
            \item Reduce excessive vertical space before\slash after \lstinline|\vdotswithin|
            \item \lstinline|\MTFlushSpaceAbove| appears \alert{directly after}~\lstinline|\\| of previous line
            \item \lstinline|\MTFlushSpaceBelow| \alert{replaces}~\lstinline|\\| of line with \lstinline|\vdotswithin|
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=13}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{align*}
    a &= b \\
    \MTFlushSpaceAbove
    &\vdotswithin{=}
    \MTFlushSpaceBelow
    c &= d
\end{align*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Ellipsis for equation groups}
    Useful shorthands for equation groups with \alert{single} alignment point~\lstinline|&|

    \begin{codeexp}<+->
        \lstinline|\shortvdotswithin{§rel§}|\alert{\dag}
        \begin{itemize}
            \item Shorthand for
                \begin{lstlisting}
                    \MTFlushSpaceAbove
                    & \vdotswithin{=}
                    \MTFlusSpaceBelow
                \end{lstlisting}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\shortvdotswithin*{§rel§}|\alert{\dag}
        \begin{itemize}
            \item Shorthand for (note the placement of~\lstinline|&|)
                \begin{lstlisting}
                    \MTFlushSpaceAbove
                    \vdotswithin{=} &
                    \MTFlusSpaceBelow
                \end{lstlisting}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Long chain of equations}
    \begin{codeexp}<+->
        \lstinline|\MoveEqLeft[§num§]|\alert{\dag}
        \begin{itemize}[<+->]
            \item \a{num}: some number
            \item For very long chain of equations, especially with very long first line: align all subsequent lines at the left side with some indent to the right (usually \lstinline|\qquad|)
            \item \lstinline|\MoveEqLeft| indents the first line to the \emph{left} by \lstinline|§num§em|
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=14}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{equation*}
\begin{split}
    \MoveEqLeft
    a+b+c+d+e \\
    &= a+b+c+d+e \\
    &\le a+b+c+d
\end{split}
\end{equation*}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Tags}
    \begin{codeexp}<+->
        \lstinline|\tag{§tag§}|, \lstinline|\tag*{§tag§}|, \lstinline|\notag|
        \begin{itemize}[<+->]
            \item \lstinline|\tag|: produces tag with parentheses, doesn't increase equation number
            \item<.-> \lstinline|\tag*|: same as \lstinline|\tag|, but without parentheses
            \item<.-> \lstinline|\notag|: suppresses tag
            \item Only work in unstarred math environments, must be placed before~\lstinline|\\|
            \item<.-> \a{tag}: any symbol
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=12}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{gather}
    a \tag{\(*\)} \\
    b \tag*{\(**\)} \\
    c \\
    d \notag
\end{gather}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Subequations}
    \begin{codeexp}<+->
        \lstinline|\begin{subequations}|\\
        \tabstrut\a{mathenv}\\
        \lstinline|\end{subequation}|
        \begin{itemize}[<+->]
            \item \a{mathenv}: other math environment like \lstinline|align|
            \item Tags subequations like (2a), (2b), \dots
            \item<.-> Labelling \lstinline|subequations|, i.\,e.\ outside of \a{mathenv}, only gives e.\,g.~(2)
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=7, lastline=13}]<+->
\documentclass{article}
\usepackage[papersize={5cm,2.5cm}, scale=.9]{geometry}
\pagestyle{empty}
\usepackage{mathtools}
\begin{document}
\vspace*{-2\baselineskip}
\begin{subequations}
    \begin{align}
        a \\
        b \\
        c
    \end{align}
\end{subequations}
\end{document}
    \end{pdflist}
\end{frame}

\section{How does math work in \LaTeX{}?}

\begin{frame}[fragile]{Math mode}
    \begin{quote}
        \alert{Math mode} works fundamentally different from \alert{text mode}.
    \end{quote}

    \begin{itemize}[<2->]
        \item Math and text mode use almost disjoint set of commands
        \item Ignores almost all spaces in code, forbids empty lines
        \item Letters are in italics and \enquote{separated} (i.\,e.\ no \emph{ligatures})
    \end{itemize}

    \begin{code}<3->
office,3-2=1

\(office,3-2=1\)

\textit{office}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Inline and display math}
    \begin{block}{Inline math mode}<+->
        \lstinline|\(§math§\)|: \a{math} on the same line as text; mostly \emph{text style}
        \begin{code}
text \(\sum_{k=1}^n \frac{1}{k!}\) text
        \end{code}
    \end{block}

    \begin{block}{Display math mode}<+->
        \lstinline|\[§math§\]|: \a{math} on separate line; mostly in \emph{display style}
        \begin{code}
text
\[\sum_{k=1}^n \frac{1}{k!}\]
text
        \end{code}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Inline and display math}
    \begin{alertblock}{Tip}<+->
        When to use which?
        Use display math mode if one of the following holds:
        \begin{enumerate}[<+->]
            \item Inline math formula is vertically higher than a paragraph line
            \item Inline math formula takes more then half of a paragraph line (my rule of thumb)
            \item You want to emphasise the formula
            \item You want to enumerate the formula
        \end{enumerate}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Inline and display math}
    \begin{exampleblock}{Remark}<+->
        Plain \TeX{} uses \lstinline|$§math§$| and \lstinline|$$§math§$$| instead.
        Only \lstinline|$§math§$| is supported by \LaTeX{} for convenience.
    \end{exampleblock}

    \begin{exampleblock}{Pros and cons for \LaTeX{} Syntax}<+->
        \begin{itemize}[<+->]
            \item[+] Better error handling when nested , i.\,e.\ \lstinline|\( \( \) \)|
            \item[+]<.-> \lstinline|mathtools| provides automatic italic correction with \lstinline|mathic|
            \item[+]<.-> More conceptual (like \lstinline|\begin{§\dots§} \end{§\dots§}|) 
            \item[\textminus] More to type
            \item[\textminus]<.-> Hard to read, e.\,g.\ \lstinline|$g(f(x))$| vs.\ \lstinline|\(g(f(x))\)|
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{The \texttt{amsmath} and \texttt{mathtools} packages}
    \begin{itemize}[<+->]
        \item \emph{American Mathematical Society} (AMS) propelled the development for typesetting (even higher) mathematics
        \item \AmS-\LaTeX{} (1990): suite of document classes and packages for publication under the AMS
            \begin{itemize}[<1->]
                \item Document classes: \lstinline|amsart|, \lstinline|amsproc|, \lstinline|amsbook| (very popular)
                \item Standalone packages: \lstinline|amsmath|, \lstinline|amsthm|, \lstinline|amsref|, etc.\
            \end{itemize}
        \item \lstinline|amsmath| is the \alert{\emph{de facto} standard} for math in \LaTeX
        \item AMS is \emph{veeery slooow} in developing \AmS-\LaTeX{}
        \item \lstinline|mathtools|: community-driven collection of bug fixes and extensions of \lstinline|amsmath|, automatically loads \lstinline|amsmath|
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{The \texttt{amsmath} and \texttt{mathtools} packages}
    \begin{alertblock}{IN THE FOLLOWING, WE WILL ALWAYS ASSUME}<+->
        \begin{lstlisting}
            \usepackage{mathtools}
        \end{lstlisting}
        \begin{itemize}[<+->]
            \item Features \alert{only} present in \lstinline|mathtools| are marked with a \alert{\emph{dagger}~\dag}
            \item<.-> Features available without \lstinline|amsmath| won't be marked
        \end{itemize}
    \end{alertblock}

    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{mathtools}|
        \begin{itemize}[<+->]
            \item \lstinline|mathtools| accepts \a{options} from \lstinline|amsmath|:
                \begin{itemize}
                    \item \lstinline|leqno|: number equations in display mode on the left
                    \item<.-> \lstinline|fleqn| (\enquote{flush left}): align display mode on the left with indent
                    \item Later: \lstinline|centertags|, \lstinline|tbtags|\\
                        \lstinline|sumlimits|, \lstinline|intlimits|, \lstinline|namelimits|\\
                        \lstinline|nosumlimits|, \lstinline|nointlimits|, \lstinline|nonamelimits|
                \end{itemize}
            \item Options exclusive to \lstinline|mathtools| are set with\\
                \lstinline|\mathtools{§options§}|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{The \texttt{amsmath} and \texttt{mathtools} packages}
    \begin{pdflist}[comment style={scale=2}, listing options={firstline=5, lastline=7}]
\documentclass{article}
\usepackage[papersize={5cm,1.5cm}, scale=.9]{geometry}
\usepackage{mathtools}
\begin{document}
\begin{equation}
    a+b % default
\end{equation}
\end{document}
    \end{pdflist}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=5, lastline=7}]
\documentclass{article}
\usepackage[papersize={5cm,1.5cm}, scale=.9]{geometry}
\usepackage[leqno]{mathtools}
\begin{document}
\begin{equation}
    a+b % leqno
\end{equation}
\end{document}
    \end{pdflist}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=5, lastline=7}]
\documentclass{article}
\usepackage[papersize={5cm,1.5cm}, scale=.9]{geometry}
\usepackage[fleqn]{mathtools}
\begin{document}
\begin{equation}
    a+b % fleqn
\end{equation}
\end{document}
    \end{pdflist}

    \begin{pdflist}[comment style={scale=2}, listing options={firstline=5, lastline=7}]
\documentclass{article}
\usepackage[papersize={5cm,1.5cm}, scale=.9]{geometry}
\usepackage[leqno, fleqn]{mathtools}
\begin{document}
\begin{equation}
    a+b % leqno + fleqn
\end{equation}
\end{document}
    \end{pdflist}
\end{frame}

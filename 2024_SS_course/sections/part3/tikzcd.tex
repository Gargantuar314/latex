\section{Commutative diagrams}

\begin{frame}[fragile]{Introduction}
    \begin{codeexp}<+->
        \lstinline|\usepackage{tikz-cd}|
        \begin{itemize}[<+->]
            \item Nowadays the most modern and powerful package
            \item Uses and loads \TikZ{}
            \item All features of \TikZ{} can be used with \lstinline|tikz-cd|\\
                \textrightarrow~these are marked with with a \alert{\emph{pilcrow}~\P}
            \item Graphical interface: \href{https://q.uiver.app/}{quiver}
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{Warning}<+->
        \lstinline|tikz-cd| uses quotes~\lstinline|"| syntax from \TikZ.
        \lstinline|babel| sometimes makes this an \emph{active character} which trips up \TikZ.
        Add
        \begin{lstlisting}
            \usetikzlibrary{babel}
        \end{lstlisting}
    \end{alertblock}

    \begin{codeexp}<+->
        \lstinline|\tikzcdset{§options§}|
        \begin{itemize}
            \item Set global default \a{options} for each diagram
            \item \a{options}: comma-separated key-value list
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Creating commutative diagrams}
    \begin{codeexp}<+->
        \lstinline|\begin{tikzcd}[§options§]|\\
        \tabstrut\a{diagram}\\
        \lstinline|\end{tikzcd}|
        \begin{itemize}[<+->]
            \item Types commutative \a{diagram}
            \item Can appear inside or without math environments
            \item \a{options}: local options which can overwrite global ones; comma-separated key-value list
            \item \a{diagram} follows syntax from \lstinline|tabular|
                \begin{itemize}
                    \item \lstinline|&|~separates columns
                    \item<.-> \lstinline|\\|~separates lines
                    \item \lstinline|&[§length§]|, \lstinline|\\[§length§]| accept extra space of \a{length} (can be negative)
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{Tip}<+->
        (Almost) always put \lstinline|tikzcd| environments inside \lstinline|equation| or \lstinline|\[ \]|.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{\a{options} for commutative diagrams}
    \begin{itemize}[<+->]
        \item \lstinline|cells={§options§}|: apply extra \a{options} to each cell
        \item \lstinline|arrows={§options§}|: apply extra \a{options} to each arrow
        \item \lstinline|row sep=§length§|: set row separation to \a{length}
            \begin{itemize}
                \item \a{length}: any length or one of \lstinline|tiny|, \lstinline|small|, \lstinline|scriptsize|, \lstinline|normal|, \lstinline|large|, \lstinline|huge|
                \item As global option, set e.\,g.\
                    \begin{lstlisting}
                        \tikzcdset{row sep/normal=1cm}
                    \end{lstlisting}
            \end{itemize}
        \item \lstinline|column sep=§length§|: set column separation to \a{length}, similar to \lstinline|row sep|
        \item \lstinline|sep=§length§|: set row \emph{and} column separation to \a{length}
        \item \lstinline|cramped|: reduce white space around cells for large diagrams
        \item \lstinline|math mode=§bool§| (default \lstinline|true|): if all cells are in math mode
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Arrows}
    \begin{codeexp}<+->
        \lstinline|\arrow[§options§]|, \lstinline|\ar[§options§]| (both are synonyms)
        \begin{itemize}[<+->]
            \item Each cell can have multiple arrows \lstinline|\ar| starting at that cell
            \item \a{options} is comma-separated list; any options from \TikZ's \lstinline|\path| is possible
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: placement}
    \begin{codeexp}<+->
        \a{endspec}
        \begin{itemize}[<+->]
            \item Specify end of arrow relative to start
            \item String containing any number of letters \lstinline|u|~(up), \lstinline|d|~(down), \lstinline|l|~(left), \lstinline|r|~(right) in any order
            \item E.\,g.\ \lstinline|ddr| specifies arrow to cell two lines below and one column to the right
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    [column sep=small]
    A \ar[rr] \ar[dr]
    && B \ar[dl] \\
    & C 
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: placement}
    \begin{codeexp}<+->
        \lstinline|shift left=§length§|\\
        \lstinline|shift right=§length§|
        \begin{itemize}[<+->]
            \item Shift arrow left\slash right by \a{length}
            \item \a{length}: a~length
                \begin{itemize}
                    \item Default: \lstinline|.56ex|
                    \item If a dimensionless number is given, then it uses this multiple of the default 
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, shift left]
    \ar[r, shift right]
    & B \ar[r]
    \ar[r, shift left=2]
    \ar[r, shift right=2]
    & C
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: placement}
    \begin{codeexp}<+->
        \lstinline|start anchor={ [§transf§] §anchor§ }|\\
        \lstinline|end anchor={ [§transf§] §anchor§ }|
        \begin{itemize}[<+->]
            \item Specify start\slash end of arrow based on \a{anchor} of cell
            \item \a{anchor}: possible anchors (can be empty) (selection)
                \begin{itemize}[<1->]
                    \item \lstinline|north west|, \lstinline|north|, \lstinline|north east|, \lstinline|south east|, \lstinline|south|, \lstinline|south west|
                    \item \lstinline|east|, \lstinline|center|, \lstinline|west| based on text
                    \item \lstinline|real east|, \lstinline|real center|, \lstinline|real west| based on bounding box of cell
                    \item \lstinline|base east|, \lstinline|base|, \lstinline|base west| based on baseline of text
                \end{itemize}
            \item \a{transf}: transformations applied to arrow part, e.\,g.\
                \begin{itemize}[<1->]
                    \item \lstinline|xshift=§length§|\alert{\P}: shift x-coordinate by \a{length}
                    \item \lstinline|yshift=§length§|\alert{\P}: shift y-coordinate by \a{length}
                    \item \lstinline|shift=§coord§|\alert{\P}: shift by coordinate \a{coord}, e.\,g.\ \lstinline|shift={(2,3)}| (default unit is \lstinline|cm|)
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: bending}
    \begin{codeexp}<+->
        \lstinline|bend left=§angle§|\alert{\P}\\
        \lstinline|bend right=§angle§|\alert{\P}
        \begin{itemize}[<+->]
            \item Bend arrow to left\slash right where arrow leaves start cell and enters end cell by \a{angle} degrees
            \item \a{angle} (default~\lstinline|30|): real number, interpreted in degrees relative to straight line
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, bend left=50]
    \ar[r, bend left]
    \ar[r, bend right]
    & B 
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: bending}
    \begin{codeexp}<+->
        \lstinline|in=§angle§|\alert{\P}\\
        \lstinline|out=§angle§|\alert{\P}
        \begin{itemize}[<+->]
            \item Specify \a{angle} by which arrow comes out\slash enters in the start\slash end cell
            \item \a{angle}: real number, relative to straight line
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, bend left]
    \ar[r, bend right, in=-160]
    & B = C
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: bending}
    \begin{codeexp}<+->
        \lstinline|loop|\alert{\P}\\
        \lstinline|loop left|\alert{\P}, \lstinline|loop right|\alert{\P}, \lstinline|loop above|\alert{\P}, \lstinline|loop below|\alert{\P}
        \begin{itemize}[<+->]
            \item Add loop to cell
            \item \lstinline|loop| requires setting \lstinline|in=§angle§| and \lstinline|out=§angle§|;\\
                best results are achieved when difference of the angles is \qty{30}{\degree}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[loop left]
    \ar[r]
    & B \ar[loop right]
\end{tikzcd}
    \end{code}

    \begin{exampleblock}{Note}<+->
        More settings in the \TikZ{} docs,~§74
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: arrow tips, tails, shafts (selection)}
    \begin{code}[lefthand width=.6\textwidth]
\begin{tikzcd}[row sep=.4ex]
    A \ar[r, hook] & B \\
    A \ar[r, hook'] & B \\
    A \ar[r, tail] & B \\
    A \ar[r, two heads] & B \\
    A \ar[r, dash] & B \\
    A \ar[r, equal] & B \\
    A \ar[r, dashed] & B \\
    A \ar[r, dotted] & B \\
    A \ar[r, mapsto] & B \\
    A \ar[r, Rightarrow] & B \\
    A \ar[r, leftrightarrow] & B \\
    A \ar[r, Leftrightarrow] & B \\
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: arrow tips, tails, shafts (selection)}
    \begin{itemize}[<+->]
        \item Arrow tips, tails and shafts can be combined
    \end{itemize}

    \begin{code}[lefthand width=.6\textwidth]<.->
\begin{tikzcd}
    A \ar[r, hook, dashed,
    two heads] & B
\end{tikzcd}
    \end{code}

    \begin{exampleblock}{Example}<+->
        Default arrow style
        \begin{code}[lefthand width=.6\textwidth]
\begin{tikzcd}[arrows=mapsto]
    x \ar[r] \ar[d]
    & y \ar[d] \\
    z \ar[r]
    & w
\end{tikzcd}
        \end{code}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: labels}
    \begin{codeexp}<+->
        \lstinline|"{§label§}" {§options§}|\\
        \lstinline|"{§label§}"' {§options§}|
        \begin{itemize}[<+->]
            \item \a{label} for an arrow, each arrow can have multiple labels
            \item \a{label} is placed to the right side relative to the direction of the arrow
            \item ticked~\lstinline|'| version places \a{label} on the other side
            \item Braces of \a{label} or \a{options} can be dropped if it doesn't contain \alert{commas}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, "f", "\sim"']
    & B
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for labels}
    \begin{codeexp}<+->
        \lstinline|pos=§frac§|\alert{\P}\\
        \lstinline|very near start|\alert{\P},
        \lstinline|near start|\alert{\P},
        \lstinline|near end|\alert{\P},
        \lstinline|very near end|\alert{\P}
        \begin{itemize}[<+->]
            \item Relative placement of label along length of the arrow
            \item \a{frac}: real number between \lstinline|0|~and~\lstinline|1|
            \item \lstinline|pos=§frac§|: place label at \a{frac} of arrow length
            \item Other commands, in order, are shorthands for \lstinline|pos=.125|, \lstinline|pos=.25|, \lstinline|pos=.75|, \lstinline|pos=.875|
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, "c"',
    "a" near start,
    "b" near end]
    & B
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for labels}
    \begin{codeexp}<+->
        \lstinline|sloped|\alert{\P}
        \begin{itemize}[<+->]
            \item Rotate label in direction of the arrow
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[dr,
    "\sim" sloped] \\
    & B
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for labels}
    \begin{codeexp}<+->
        \lstinline|marking|\\
        \lstinline|description|
        \begin{itemize}[<+->]
            \item \lstinline|marking|: places label on top of arrow
            \item \lstinline|description|: same as \lstinline|marking|, but clears the background
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzcd}
    A \ar[r, "/" marking]
    & B \\
    A \ar[r, "\phi" description]
    & B
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for arrows: invisible arrows}
    \begin{codeexp}<+->
        \lstinline|phantom|
        \begin{itemize}[<+->]
            \item Makes invisible arrows
            \item Sometimes useful to place labels \enquote{mid-air}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
% \ulcorner is part of amssymb
\begin{tikzcd}
    A \ar[r] \ar[d]
    \ar[dr, phantom,
    "\ulcorner" near start]
    & B \ar[d] \\
    C \ar[r]
    & D
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{\a{options} for commutative diagrams}
    \begin{exampleblock}{Problem}<+->
        The \lstinline|tikzcd| environment uses \lstinline|&| to separate cells.
        This doesn't work it is used in tandem with \lstinline|matrix| or inside \lstinline|\footnote|.
    \end{exampleblock}

    \begin{codeexp}<+->
        \lstinline|ampersand replacement=§string§|
        \begin{itemize}[<+->]
            \item Replaces \lstinline|&| with \a{string} as column separator in \lstinline|tikzcd| environment
            \item \a{string} is most often \lstinline|\&|
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.65\textwidth]<+->
\begin{tikzcd}
    [ampersand replacement=\&]
    K^2 \ar[r, "{\begin{psmallmatrix}
        0 & 1 \\ 1 & 0
    \end{psmallmatrix}}"]
    \& K^2
\end{tikzcd}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Defining styles}
    \begin{alertblock}{Tip}
        Define macros for common styles.
        E.\,g.\ for pushout signs and isomorphisms:
        \begin{lstlisting}
            \tikzcdset{
                push/.style = {dr, phantom, "\ulcorner" near start},
                iso/.style = {"\sim" sloped},
                iso'/.style = {"\sim"' sloped},
            }
        \end{lstlisting}
        \begin{actionenv}<2->
            Then use
            \begin{lstlisting}
                \ar[push]
                \ar[d, iso]
                \ar[d, iso']
            \end{lstlisting}
        \end{actionenv}
    \end{alertblock}
\end{frame}

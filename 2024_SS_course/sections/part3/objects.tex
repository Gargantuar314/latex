\section{Mathematical objects}

\begin{frame}[fragile]{Fractions}
    \begin{codeexp}<+->
        \lstinline|\frac{§num§}{§den§}|: fraction \enquote{\a{num} over \a{den}}\\
        \lstinline|\tfrac{§num§}{§den§}|: \emph{text style} fraction\\
        \lstinline|\dfrac{§num§}{§den§}|: \emph{display style} fraction
        \begin{itemize}[<+->]
            \item Size of \lstinline|\frac| will depend on math mode and position in formula
            \item Variants \lstinline|\tfrac| and \lstinline|\dfrac| type fraction in fixed size
                \begin{itemize}[<1->]
                    \item \lstinline|\tfrac| as if in inline math mode
                    \item \lstinline|\dfrac| as if in display math mode
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ \frac{1}{2} (a+b),
   \tfrac{1}{2} (a+b) \]
\( \frac{1}{2} (a+b),
   \dfrac{1}{2} (a+b) \)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Fractions}
    \begin{exampleblock}{Typographical remark}
        Avoid simple double fractions (even in display math) for readability.
        Try e.\,g.\ the slash form or inverse notation.
        \begin{code}<+->
\[ \frac{ \frac{a}{b} }
        { \frac{c}{d} },
   \frac{ ab^{-1} }
        { c/d     } \]
        \end{code}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Fractions}
    \begin{codeexp}<+->
        \lstinline|\splitfrac{§start§}{§cont§}|\alert{\dag}: split fraction\\
        \lstinline|\splitdfrac{§start§}{§cont§}|\alert{\dag}: wider line separation
        \begin{itemize}[<+->]
            \item Use this when the numerator\slash denominator is to long
            \item \a{start}: first line of numerator\slash denominator
            \item<.-> \a{cont}: second line of numerator\slash denominator
            \item Both \lstinline|\splitfrac|, \lstinline|\splitdfrac| in text style
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ \frac{
  \splitfrac{a+b+c}{
    \splitfrac{+d+e}{+f+g}
  } }{z} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Continued fractions}
    \begin{codeexp}
        \lstinline|\cfrac[§align§]{§num§}{§den§}|: continued fraction
        \begin{itemize}
            \item \a{align}: alignment of \a{num}, either \lstinline|l| (left) or \lstinline|r| (right)
        \end{itemize}
    \end{codeexp}

    \begin{code}<2->
\[ \Phi =
  \cfrac{1}{1 +
    \cfrac{1}{1 +
      \cfrac[l]{1}{1 +
        \cfrac[r]{1}{1 + \dotsb
  }}}} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Binomial coefficients}
    \begin{codeexp}
        \lstinline|\binom{§top§}{§bot§}|: binomial coefficient \enquote{\a{top} choose \a{bot}}\\
        \lstinline|\tbinom{§top§}{§bot§}|: \emph{text style} binomial coefficient\\
        \lstinline|\dbinom{§top§}{§bot§}|: \emph{display style} binomial coefficient
        \begin{itemize}
            \item Same properties as \lstinline|\frac| apply
        \end{itemize}
    \end{codeexp}

    \begin{code}<2->
\[ \binom{n}{k},
   \tbinom{n}{k} \]
\( \binom{n}{k},
   \dbinom{n}{k} \)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Super- and subscript}
    \begin{codeexp}<+->
        \lstinline|§base§^{§sup§}|: superscript\\
        \lstinline|§base§_{§sub§}|: subscript
        \begin{itemize}[<+->]
            \item \a{sup}: superscript, \a{sub}: subscript
            \item Double superscripts like \lstinline|a^b^c| are illegal (same for subscripts)
                \begin{itemize}
                    \item Group with braces to make clear what \a{base} and what \a{sup} is
                    \item \lstinline|a^{b^c}| is legal
                    \item \lstinline|{a^b}^c| is legal, but rather write \lstinline|(a^b)^c|
                \end{itemize}
            \item One super- and one subscript can appear in either order
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth, righthand width=.35\textwidth]<+->
\( {a^b}^c, (a^b)^c, a^{b^c} \)

\( {a_b}_c, (a_b)_c, a_{b_c} \)

\( a_b^c = a^c_b \)

\( a_{b^c}, a^{b^{c^d}} \)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Super- and subscript}
    \begin{exampleblock}{Convention}
        Drop braces of \a{sup}/\a{sub} if it is a single character
    \end{exampleblock}

    \begin{alertblock}{Warning}<2->
        Some people recommend to write \lstinline|{(a^b)}^d|, \({(a^b)}^d\) instead of simply \lstinline|(a^b)^d|, \((a^b)^d\).
        This is \alert{wrong} since it increases interline space to much (especially in inline math mode).
    \end{alertblock}

    \begin{exampleblock}{Typographical remark}<3->
        Avoid fractions in super- and subscripts for readability (even in display math).
        Try e.\,g.\ the slash form for fractions.
        \begin{code}
\[
    a^{\frac{b}{c}}, a^{b/c}
\]
        \end{code}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Super- and subscript}
    \begin{codeexp}<+->
        \lstinline|\prescript{§sup§}{§sub§}{§arg§}|\alert{\dag}: left super- and subscript
        \begin{itemize}[<+->]
            \item Set formatting with \lstinline|\mathtoolsset| options
                \begin{itemize}[<1->]
                    \item \lstinline|prescript-sup-format=§cmd§|
                    \item \lstinline|prescript-sub-format=§cmd§|
                    \item \lstinline|prescript-arg-format=§cmd§|
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\mathtoolsset{
  prescript-sup-format=\mathit,
  prescript-sub-format=\mathbf,
  prescript-arg-format=\mathrm,
}
\(\prescript{A}{Z}{X} \to \prescript{A-4}{Z-2}{Y} + \prescript{4}{2}{\alpha}\)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Decorate symbols}
    \begin{codeexp}<+->
        \lstinline|\overset{§above§}{§symb§}|\\
        \lstinline|\underset{§below§}{§symb§}|\\
        \lstinline|\overunderset{§above§}{§below§}{§symb§}|
        \begin{itemize}[<+->]
            \item \a{symb}: any symbol of type Ord, Bin, Rel
            \item \a{above}, \a{below}: anything
            \item Places \a{above} above, \a{below} below \a{symb} in smaller size
            \item Automatically determines correct type for decorated symbol
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ a \overset{(*)}{=} b \]
\[ \overset{*}{X} > \underset{*}{X} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Roots}
    \begin{codeexp}
        \lstinline|\sqrt[§exp§]{§radix§}|: \a{exp}th root of \a{radix}
    \end{codeexp}

    \begin{code}[lefthand width=.5\textwidth]
\[
    \frac{1}{
      \sqrt[p+q]{2}
    },
    \sqrt{\sqrt{\sqrt{
      \sqrt{\sqrt{\sqrt{3}}}
    }}}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Modular relations}
    \begin{codeexp}
        \lstinline|\bmod|, \lstinline|\pmod|, \lstinline|\mod|, \lstinline|\pod|
        \begin{itemize}
            \item \lstinline|\bmod|: mod as binary operator
            \item \lstinline|\pmod|: mod with parentheses
            \item \lstinline|\mod|: mod only, no parentheses
            \item \lstinline|\pod|: parentheses only, no mod
            \item<2-> \lstinline|\pod| reduces spacing if in inline math mode 
        \end{itemize}
    \end{codeexp}

    \begin{code}<3->
\begin{align*}
    a &\equiv b \pmod n \\
    a &\equiv b \mod n \\
    a &\equiv b \pod n
\end{align*}

\(a \bmod \mathfrak{p}\), 
\(a \equiv b \pod n\)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Text and boxes}
    \begin{codeexp}<+->
        \lstinline|\text{§text§}|
        \begin{itemize}
            \item Typeset \a{text} as if in text mode; scales \a{text} correctly
            \item Recall that math mode \alert{ignores} spaces
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\( \{p^{-1} \mid
    p \text{ prime}\} \)

\( A_{\text{up}},
    \hom_{ \text{\(k\)-alg} }\)
    \end{code}

    \begin{codeexp}<+->
        \lstinline|\boxed{§math§}|
        \begin{itemize}
            \item Surround \a{math} inside a box
        \end{itemize}
    \end{codeexp}

    \begin{code}<.->
\[\boxed{ \sum_{k=1}^n
    = \frac{n(n+1)}{2} }\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Ellipsis (dots)}
    \begin{codeexp}<+->
        \lstinline|\dots|\\
        \begin{itemize}[<+->]
            \item Produces \emph{ellipsis} \(\dots\) in math mode
            \item Tries to automatically determine correct vertical height and surrounding spacing based on \alert{following} symbol type
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\dotsc|, \lstinline|\dotsb|, \lstinline|\dotsm|, \lstinline|\dotsi|, \lstinline|\dotso|
        \begin{itemize}[<+->]
            \item Use if \lstinline|\dots| doesn't work, e.\,g.\ if \lstinline|\dots| is followed by~\lstinline|\)|
                \begin{itemize}
                    \item \lstinline|\dotsc|: \enquote{dots with commas}
                    \item \lstinline|\dotsb|: \enquote{dots with Bin/Rel symbol}
                    \item \lstinline|\dotsm|: \enquote{dots for multiplication dots}
                    \item \lstinline|\dotsi|: \enquote{dots with integrals}
                    \item \lstinline|\dotso|: \enquote{dots for others}
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\(a_1 + \dots + a_n\),
\(a_1 + a_2 + \dotsb\)

\(a_1, \dots, a_n\),
\(a_1 \dotsm a_n\)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable arrows}
    \begin{codeexp}<+->
        \lstinline|\x§type§[§below§]{§above§}|
        \begin{itemize}[<+->]
            \item Extendable arrows (should be only used in \alert{display math})
            \item \a{type}: arrow type
            \item \a{above}/\a{below}: expression above/below in small size
            \item Available arrow types \a{type} (selection):\\
                \lstinline|\xleftarrow|, \lstinline|\xrightarrow|, \lstinline|\xleftrightarrow|\alert{\dag}\\
                \lstinline|\xLeftarrow|\alert{\dag}, \lstinline|\xRightarrow|\alert{\dag}, \lstinline|\xLeftrightarrow|\alert{\dag}\\
                \lstinline|\xhookleftarrow|\alert{\dag}, \lstinline|\xhookrightarrow|\alert{\dag}, \lstinline|\xmapsto|\alert{\dag}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\[ \frac{1}{n}
  \xrightarrow{n \to \infty}
  0 \]
\[ A
  \xRightarrow[\text{implies}]{}
  B \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable arrows}
    \begin{exampleblock}{Note}
        Zooming out in the PDF, extendable arrows might look a bit \enquote{jagged}.
        These artefacts are due to rendering issues from the PDF viewer, a problem \LaTeX{} can't solve.
        This doesn't impair the quality of physical prints.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Extendable decorations}
    \begin{codeexp}<+->
        \lstinline|\widehat{§math§}|\\
        \lstinline|\widetilde{§math§}|
        \begin{itemize}[<+->]
            \item Stretched versions of \lstinline|\hat| and \lstinline|\tilde|
            \item Stretchability of \lstinline|\widehat| and \lstinline|\widetilde| is \alert{limited}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\(\widehat{xyz}\),
\(\widetilde{xyz}\)

\(\widetilde{abcdefgh}\),
\(\widehat{abcdefgh}\)
    \end{code}

    \begin{alertblock}{Tip}<+->
        \begin{itemize}
            \item Avoid this in inline math mode, as it increases the space between lines
            \item<4-> For accenting very long expressions, try (\lstinline|\;| is for space):
                \begin{code}
\((abcdefgh)\hat{\;} a\)
                \end{code}
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Extendable decorations}
    \begin{codeexp}<+->
        \lstinline|\overline{§math§}|, \lstinline|\underline{§math§}|\\
        \lstinline|\overrightarrow{§math§}|, \lstinline|\underrightarrow{§math§}|\\
        \lstinline|\overleftarrow{§math§}|, \lstinline|\underleftarrow{§math§}|\\
        \lstinline|\overleftrightarrow{§math§}|, \lstinline|\underleftrightarrow{§math§}|\\
        \begin{itemize}[<+->]
            \item Extendable decorations over\slash under \a{math}
            \item<.-> Can be combined\slash stacked
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[
    \overline{abc},
    \underline{abc},
    \overrightarrow{abc},
    \underrightarrow{abc}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable decorations}
    \begin{codeexp}<+->
        \lstinline|\overbrace{§math§}|, \lstinline|\underbrace{§math§}|\\
        \lstinline|\overbracket{§math§}|\alert{\dag}, \lstinline|\underbracket{§math§}|\alert{\dag}
        \begin{itemize}[<+->]
            \item Possible to put super- or subscripts after them
            \item \lstinline|\overbracket|, \lstinline|\underbracket| accept optional arguments
                \textrightarrow~see \lstinline|mathtools| docs
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\[
  \underbrace{abc}_{> 0},
  \overbrace{abc}^{\eqcolon z},
  \underbracket{abc}_{\text{hey}}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Limits for math operators}
    \begin{itemize}[<+->]
        \item All symbols of type Op have special mechanism for specifying super- and subscripts
        \item If symbol places super- and subscript above\slash below the symbol, it is said to \emph{take limits}
    \end{itemize}

    \begin{alertblock}{Warning}<+->
        Operators in inline math mode should \alert{never} take limits as it increases interline space (some authors still don't care).
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Limits for Op}
    \begin{codeexp}<+->
        \lstinline|\limits|, \lstinline|\nolimits|, \lstinline|\displaylimits|
        \begin{itemize}[<+->]
            \item Overwrites default placement of limits
            \item<.-> To be issued directly after Op type symbol
            \item \lstinline|\limits|: takes limits
            \item<.-> \lstinline|\nolimits|: does not take limits
            \item<.-> \lstinline|\displaylimits|: only takes limits if in \emph{display style}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.55\textwidth]<+->
\(\sum\limits_{i=1}^n i;
  \sum\nolimits_{i=1}^n i;
  \sum\displaylimits_{i=1}^n i\)
\[\sum\limits_{i=1}^n i;
  \sum\nolimits_{i=1}^n i;
  \sum\displaylimits_{i=1}^n i\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{itemize}[<+->]
        \item Text acts as an operator\\
            E.\,g.\ \(\hom(A,B), \ker f, \lim_{n \to \infty} \frac{1}{n}\)
        \item Named operators have proper spacing on both sides\\
            E.\,g.\ \(A \hom B\) vs.\ \(A \mathrm{hom} B\)
        \item Spacing after named operator is killed if delimiter (Open/Close) follows\\
            E.\,g.\ \(\ker(f)\)
        \item Some named operators take limits like \lstinline|\displaylimits|
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{block}{Predefined named operators (selection)}
        \begin{tabular}{@{}*4{r@{\hspace{\tabcolsep}}l}@{}}
            \(\arcsin\) & \lstinline|\arcsin|
            & \(\arccos\) & \lstinline|\arccos|
            & \(\arctan\) & \lstinline|\arctan| \\
            \(\sin\) & \lstinline|\sin|
            & \(\cos\) & \lstinline|\cos|
            & \(\tan\) & \lstinline|\tan| \\
            \(\sinh\) & \lstinline|\sinh|
            & \(\cosh\) & \lstinline|\cosh|
            & \(\tanh\) & \lstinline|\tanh| \\
            \(\cot\) & \lstinline|\cot|
            & \(\coth\) & \lstinline|\coth|
            & \(\exp\) & \lstinline|\exp| \\
            \(\log\) & \lstinline|\log|
            & \(\lg\) & \lstinline|\lg|
            & \(\ln\) & \lstinline|\ln| \\
            \(\deg\) & \lstinline|\deg|
            & \(\dim\) & \lstinline|\dim|
            & \(\ker\) & \lstinline|\ker| \\
            \(\hom\) & \lstinline|\hom| \\ \hline
            \(\det\) & \lstinline|\det|
            & \(\gcd\) & \lstinline|\gcd|
            & \(\Pr\) & \lstinline|\Pr| \\
            \(\lim\) & \lstinline|\lim|
            & \(\liminf\) & \lstinline|\liminf|
            & \(\limsup\) & \lstinline|\limsup| \\
            \(\varliminf\) & \lstinline|\varliminf|
            & \(\injlim\) & \lstinline|\injlim|
            & \(\varinjlim\) & \lstinline|\varinjlim| \\
            \(\varlimsup\) & \lstinline|\varlimsup|
            & \(\projlim\) & \lstinline|\projlim|
            & \(\varprojlim\) & \lstinline|\varprojlim| \\
            \(\min\) & \lstinline|\min|
            & \(\max\) & \lstinline|\max|
            & \(\inf\) & \lstinline|\inf| \\
            \(\sup\) & \lstinline|\sup|
        \end{tabular}

        Above the line: don't take limits\\
        Below the line: take limits in display style
    \end{block}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{codeexp}<+->
        \lstinline|\DelcareMathOperator{\§cmd§}{§text§}|\\
        \lstinline|\DelcareMathOperator*{\§cmd§}{§text§}|
        \begin{itemize}[<+->]
            \item Defines \lstinline|\§cmd§| with \a{text} as named operator
            \item Must be issued in preamble, \lstinline|\§cmd§| must be undefined
            \item \a{text} is in a \enquote{special text mode}:
                \begin{itemize}[<1->]
                    \item \lstinline|-| (hyphen) is replaced by text hyphen, not minus sign
                    \item \lstinline|*| (asterisk) is replaced by raised text asterisk, not centred math asterisk
                    \item Otherwise behaves like math mode with text in \lstinline|\mathrm|
                \end{itemize}
            \item Starred version defines named operator which takes limits in display style
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{pdflist}[lefthand width=.6\textwidth, listing options={firstline=3}, sidebyside gap=0pt, comment style={scale=2}]
\documentclass{article}
\usepackage[papersize={3cm,5cm}, scale=.9]{geometry}
\usepackage{mathtools}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator
    {\Hom}{\mathcal{H}om}
\DeclareMathOperator
    {\projdim}{proj.dim}
\DeclareMathOperator*
    {\esssup}{ess\,sup}
\DeclareMathOperator*
    {\plim}{p-lim}
\begin{document}
\[ \im f \]
\[ \Hom(F,G) \]
\[ \projdim(M) \]
\[ \esssup_{x \in X} f(x) \]
\[ \plim_{n \to \infty} X_n \]
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{block}{Redefining named operators}<+->
        Sadly, there is \alert{no} \lstinline|\RedeclareMathOperator|.
        You \alert{have to} use low-level \TeX{} in order to clear a command.
    \end{block}
    
    \begin{actionenv}<+->
        \begin{lstlisting}
            \let\hom\relax
            \DeclareMathOperator{\hom}{Hom}
        \end{lstlisting}
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Named operators}
    \begin{codeexp}<+->
        \lstinline|\operatorname{§text§}|\\
        \lstinline|\operatorname*{§text§}|
        \begin{itemize}[<+->]
            \item Used for one-off named operators 
            \item<.-> Starred version for named operators which takes limits in display style
        \end{itemize}
    \end{codeexp}

    \begin{alertblock}{When should I define named operators?}<+->
        Difficult question.
        The answer is essentially: do you need the \alert{spacing} features of named operators?
        If not, don't use it; e.\,g.:
        \begin{code}<+->
% First option is better
\(\mathrm{id}_K \circ h\),
\(\operatorname{id}_K \circ h\)
        \end{code}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Global settings for taking limits}
    \a{options} for \lstinline|\usepackage[§options§]{mathtools}|

    \begin{codeexp}<+->
        \lstinline|intlimits|, \lstinline|nointlimits| (default)
        \begin{itemize}
            \item \alert{In display style}, whether to use \lstinline|\limits|\slash \lstinline|\nolimits| for all integrals (\lstinline|\int|, \lstinline|\iint|, etc.)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|sumlimits| (default), \lstinline|nosumlimits|
        \begin{itemize}
            \item \alert{In display style}, whether to use \lstinline|\limits|\slash \lstinline|\nolimits| for all sum-like operators (\lstinline|\sum|, \lstinline|\prod|, \lstinline|\bigcup|, etc.)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|namelimits| (default), \lstinline|nonamelimits|
        \begin{itemize}
            \item \alert{In display style}, whether to use \lstinline|\limits|\slash \lstinline|\nolimits| for named operators that take limits (the ones below the line in the table, or the ones defined by \lstinline|\DeclareMathOperator*|)
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Stacking in limits of operators}
    \begin{codeexp}<+->
        \lstinline|\substack{§line1§ \\ §line2§ \\ §\dots§}|
        \begin{itemize}[<+->]
            \item Stack \a{line1}, \a{line2}, \a{\dots} vertically
            \item Should be used in super- or subscripts
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[
    \sum_{\substack{
        0 \le i \le n\\
        0 \le j \le m}}
    P(i,j)
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{itemize}[<+->]
        \item All delimiters are vertically extendable
        \item Some relations are vertically extendable (selection):
            \begin{itemize}[<1->]
                \item \lstinline+|+ or \lstinline|\vert|, \(|\)
                \item \lstinline+\|+ or \lstinline|\Vert|, \(\|\)
                \item \lstinline|/|, \(/\)
                \item \lstinline|\backslash|, \(\backslash\)
            \end{itemize}
    \end{itemize}

    \begin{codeexp}<+->
        \lstinline|\big§delim§|, \lstinline|\Big§delim§|, \lstinline|\bigg§delim§|, \lstinline|\Bigg§delim§|
        \begin{itemize}[<+->]
            \item \a{delim}: extendable delimiter or relation symbol
            \item Increasing extra sizes for \a{delim}
            \item<.-> Creates symbols of type Ord
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ b ( \big( \Big( \bigg( \Bigg( \frac{b}{d}\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{codeexp}<+->
        \lstinline|\bigl§delim§|, \lstinline|\Bigl§delim§|, \lstinline|\biggl§delim§|, \lstinline|\Biggl§delim§|
        \lstinline|\bigm§delim§|, \lstinline|\Bigm§delim§|, \lstinline|\biggm§delim§|, \lstinline|\Biggm§delim§|
        \lstinline|\bigr§delim§|, \lstinline|\Bigr§delim§|, \lstinline|\biggr§delim§|, \lstinline|\Biggr§delim§|
        \begin{itemize}[<+->]
            \item Same as \lstinline|\big§delim§|, etc.\ from before, but creates different symbol types
            \item \lstinline|\bigl§delim§|, etc.\ create Open symbols
            \item<.-> \lstinline|\bigm§delim§|, etc.\ create Rel symbols
            \item<.-> \lstinline|\bigr§delim§|, etc.\ create Close symbols
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ \Bigl\{ \frac{a}{b}
   \Bigm| a,b > 0
   \Bigr\} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{codeexp}<+->
        \lstinline|\left§ldelim§ §\dots§ \right§rdelim§|\\
        \lstinline|\left§ldelim§ §\dots§ \middle§mdelim§ §\dots§ \right§rdelim§|
        \begin{itemize}[<+->]
            \item \a{ldelim}, \a{mdelim}, \a{rdelim}: extendable delimiters or relations
            \item \a{ldelim}, \a{rdelim} can be \emph{null delimiter}~\lstinline|.| (full stop), which acts as a \enquote{placeholder}
            \item Auto-scale \a{ldelim}, \a{mdelim}, \a{rdelim} to same size based on largest subformula in \a{\dots}
            \item Resulting \a{mdelim} is of type Ord \textrightarrow~add extra space manually
            \item Delimiters can extend larger than \lstinline|\Bigg|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{code}<+->
\[ \left\{ \frac{a}{b}
   \; \middle| \; a,b > 0
   \right\} \]

\[ \left. \begin{aligned}
    a = b \\
    c = d
\end{aligned} \right\} \]
    \end{code}

    \begin{alertblock}{Warning}<+->
        \lstinline|\left \right| has bad spacing before and after it (\TeX{} creates a type \emph{Inner}):
        \begin{code}
\[ f \left( x^2 \right),
    \sin \left( \frac{a}{b} \right) \]
        \end{code}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{codeexp}<+->
        \lstinline|\DeclarePairedDelimiter{\§cmd§}{§ldelim§}{§rdelim§}|\alert{\dag}
        \begin{itemize}[<+->]
            \item Defines \lstinline|\§cmd§| which uses \a{ldelim} and \a{rdelim} as delimiters
            \item \lstinline|\§cmd§[§size§]{§\dots§}|
                \begin{itemize}[<1->]
                    \item \a{size}: any of \lstinline|\big|, \lstinline|\Big|, \lstinline|\bigg|, \lstinline|\Bigg| (default is normal size)
                    \item Wraps \a{\dots} inside delimiters
                \end{itemize}
            \item \lstinline|\§cmd§*{§\dots§}|
                \begin{itemize}[<1->]
                    \item Wraps \a{\dots} inside auto-scaled delimiters \alert{without spacing issues}
                \end{itemize}
            \item For more advanced variants, see \lstinline|mathtools| docs
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\DeclarePairedDelimiter
    {\Set}{ \{ }{ \} }
\[ \Set*{ \frac{a}{b}
    \; \middle| \;
    a,b > 0} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Extendable delimiters and relations}
    \begin{exampleblock}{Typographical remarks}<+->
        I~recommend to \alert{not} always use auto-scaled delimiters.
        It is okay if the tips of the delimiters are on the same height as super-\slash subscripts.
    \end{exampleblock}

    \begin{code}<+->
\DeclarePairedDelimiter
    {\pn}{ ( }{ ) }
\[ f \pn*{x^2}, f(x^2) \]
\[ 2 \pn*{\sum_{j=1}^n j},
    2 \pn[\bigg]{
    \sum_{j=1}^n j} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Matrices}
    \begin{codeexp}<+->
        \lstinline|\begin{matrix}  §\dots§ \end{matrix}|\\
        \lstinline|\begin{pmatrix} §\dots§ \end{pmatrix}|\\
        \lstinline|\begin{bmatrix} §\dots§ \end{bmatrix}|\\
        \lstinline|\begin{Bmatrix} §\dots§ \end{Bmatrix}|\\
        \lstinline|\begin{vmatrix} §\dots§ \end{vmatrix}|\\
        \lstinline|\begin{Vmatrix} §\dots§ \end{Vmatrix}|
        \begin{itemize}[<+->]
            \item Creates a matrix
            \item \a{\dots} follows same syntax as \lstinline|tabular|
                \begin{itemize}[<1->]
                    \item \lstinline|&|~separates columns
                    \item \lstinline|\\|~separates rows
                \end{itemize}
            \item \lstinline|matrix| environment has no delimiters
                \begin{itemize}[<1->]
                    \item \lstinline|p|~uses parentheses
                    \item \lstinline|b|~uses brackets
                    \item \lstinline|B|~uses braces
                    \item \lstinline|v|~uses \lstinline|\vert|, i.\,e.\ pipes~\(|\)
                    \item \lstinline|V|~uses \lstinline|\Vert|, i.\,e.\ double pipes~\(\|\)
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Matrices}
    \begin{code}<+->
\[ \begin{matrix}
        0 & 1 \\
        1 & 0
\end{matrix} \]
\[ \begin{vmatrix}
        0 & 1 \\
        1 & 0
    \end{vmatrix} \]
    \end{code}

    \begin{alertblock}{Warning}<+->
        By default, \lstinline|amsmath| only allows up to ten columns.
        Redefine \lstinline|MaxMatrixCols| for more columns:
        \begin{lstlisting}
            \setcounter{MaxMatrixCols}{20}
        \end{lstlisting}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Matrices}
    \begin{codeexp}<+->
        \lstinline|\begin{matrix*}[§colalign§]  §\dots§ \end{matrix*}|\alert{\dag}\\
        \tabstrut\vdots\\
        \lstinline|\begin{Vmatrix*}[§colalign§] §\dots§ \end{Vmatrix*}|\alert{\dag}
        \begin{itemize}[<+->]
            \item \a{colalign}: one of \lstinline|l|~(left), \lstinline|r|~(right), \lstinline|c|~(centre)
            \item Same as unstarred versions, but all columns are aligned according to \a{colalign}
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[ \begin{pmatrix*}[r]
    2.2 & 24.1 \\
    3.0 & -22.5
\end{pmatrix*} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Matrices}
    \begin{codeexp}<+->
        \lstinline|\begin{smallmatrix}  §\dots§ \end{smallmatrix}|\\
        \lstinline|\begin{psmallmatrix} §\dots§ \end{psmallmatrix}|\alert{\dag}\\
        \tabstrut\vdots\\
        \lstinline|\begin{Vsmallmatrix} §\dots§ \end{Vsmallmatrix}|\alert{\dag}\\
        \lstinline|\begin{smallmatrix*}[§colalign§]  §\dots§ \end{smallmatrix*}|\alert{\dag}\\
        \tabstrut\vdots\\
        \lstinline|\begin{Vsmallmatrix*}[§colalign§] §\dots§ \end{Vsmallmatrix*}|\alert{\dag}
        \begin{itemize}[<+->]
            \item Same as the previous matrix environments, but smaller
            \item<.-> Ideal for \((2 \times 2)\)-matrices in inline math mode
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
text
\( \begin{psmallmatrix}
    0 & 1 \\ 1 & 0
    \end{psmallmatrix}\)
text
    \end{code}
\end{frame}

\begin{frame}[fragile]{Ellipses in matrices}
    \begin{codeexp}<+->
        \lstinline|\cdots|, \lstinline|\vdots|, \lstinline|\ddots|
        \begin{itemize}
            \item Create centred, vertical, diagonal dots resp.
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[
\begin{pmatrix}
    a_{11} & \cdots & a_{1n} \\
    \vdots & \ddots & \vdots \\
    a_{m1} & \cdots & a_{mn}
\end{pmatrix}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Cases}
    \begin{codeexp}<+->
        \lstinline|\begin{cases}|\\
        \lstinline|    §expr1§ & §cond1§ \\|\\
        \lstinline|    §expr2§ & §cond2§ \\|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{cases}|
        \begin{itemize}
            \item Case distinction into multiple \a{expr1}, \a{expr2}, \dots{} with conditions \a{cond1}, \a{cond2}, \dots{} resp.
            \item Lines are enclosed by opening brace to the left
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.55\textwidth, righthand width=.4\textwidth]<+->
\[ f(x) =
\begin{cases}
x,
    & \text{if \(x\) odd}, \\
\frac{x}{2},
    & \text{if \(x\) even}.
\end{cases}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Cases}
    \begin{codeexp}<+->
        \lstinline|\begin{dcases}  §\dots§ \end{dcases}|\alert{\dag}\\
        \lstinline|\begin{rcases}  §\dots§ \end{rcases}|\alert{\dag}\\
        \lstinline|\begin{drcases} §\dots§ \end{drcases}|\alert{\dag}
        \begin{itemize}
            \item Work like previous \lstinline|cases| environment
            \item \lstinline|rcases|: lines are enclosed by closing brace to the right
            \item \lstinline|d|~environments typeset \a{expr1}, \a{expr2}, \dots{} in \emph{display style}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.55\textwidth, righthand width=.4\textwidth]<+->
\[ f(x) =
\begin{dcases}
x,
    & \text{if \(x\) odd}, \\
\frac{x}{2},
    & \text{if \(x\) even}.
\end{dcases}
\]
    \end{code}
\end{frame}

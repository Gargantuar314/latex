\section{Theorem-like environments}

\begin{frame}[fragile]{Theorem-like environments}
    \begin{codeexp}<+->
        \lstinline|\usepackage{amsthm}|
        \begin{itemize}[<+->]
            \item Extends the default \LaTeX{} \lstinline|\newtheorem| syntax
            \item \emph{De facto} standard; other packages available, e.\,g.\ \lstinline|ntheorem|
            \item Has to be loaded \alert{after} \lstinline|amsmath| or \lstinline|mathtools|
        \end{itemize}
    \end{codeexp}

    \begin{block}{Anatomy of theorem-like environments}<+->
        \begingroup\rmfamily
        \textbf{Theorem~1} (note)\textbf{.}\hspace{5pt}\itshape
        Text text text.
        \endgroup
        \begin{itemize}[<+->]
            \item \textbf{\rmfamily Theorem~1.}: \emph{theorem heading}, consisting of \emph{theorem name} and \emph{theorem number}
            \item \textrm{(note)}: \emph{theorem note}
            \item \textit{\rmfamily Text text text.}: \emph{theorem body}
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Predefined theorem styles}
    \begin{codeexp}<+->
        \lstinline|\theoremstyle{plain}| (default)\\
        \lstinline|\theoremstyle{definition}|\\
        \lstinline|\theoremstyle{remark}|
        \begin{center}
            \begin{tabular}{@{}llll@{}} \toprule
                Style & Thm.\ heading & Thm.\ body & Spacing above\slash below \\ \midrule
                \lstinline|plain| & bold & italic & extra spacing \\
                \lstinline|definition| & bold & upright & extra spacing \\
                \lstinline|remark| & italic & upright & less spacing \\ \bottomrule
            \end{tabular}
        \end{center}
    \end{codeexp}

    \begin{block}{Intended usage}<+->
        \begin{itemize}[<+->]
            \item \lstinline|plain|: theorem, lemma, corollary, proposition, conjecture, criterion
            \item \lstinline|definition|: definition, problem, example, exercise, algorithm, question, axiom, property
            \item \lstinline|remark|: remark, note, notation, claim, summary, case
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Interlude: counters in \LaTeX}
    \begin{itemize}[<+->]
        \item \LaTeX{} uses \alert{counters} to automatically number anything (page number, enumerations, section numbers, etc.)
        \item Each counter is increased when invoking certain commands
        \item Counters can depend on a \enquote{parent counter}: counter is reset to zero whenever parent counter is increased
        \item Common parent counter names for theorems
            \begin{itemize}[<1->]
                \item \lstinline|section|
                \item \lstinline|subsection|
                \item \lstinline|subsubsection|
                \item \lstinline|equation|
            \end{itemize}
        \item Counter names have \alert{no backslash}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Defining theorem-like environments}
    \begin{codeexp}<+->
        \lstinline|\newtheorem{§thmenv§}{§thmname§}[§parentcntr§]|\\
        \lstinline|\newtheorem{§thmenv§}[§sharedcntr§]{§thmname§}|\\
        \lstinline|\newtheorem*{§thmenv§}{§thmname§}|
        \begin{itemize}[<+->]
            \item Defines theorem-like environment \a{thmenv} and counter \a{thmenv}; \a{thmenv} must be undefined
            \item \a{thmname}: theorem name, should be capitalised
            \item \a{parentcntr}: counter name; if provided, theorem is numbered as \enquote{\a{parentcntr}.\a{thmcntr}}
            \item \a{sharedcntr}: counter name; if provided, theorem uses \a{sharedcntr} as theorem counter and doesn't create new counter
            \item Versions with optional arguments are mutually exclusive
            \item Starred version suppresses theorem number and doesn't create new counter
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Defining theorem-like environments}
    \begin{codeexp}<+->
        \lstinline|\begin{§thmenv§}[§note§]|\\
        \tabstrut\a{text}\\
        \lstinline|\end{§thmenv§}|
        \begin{itemize}[<+->]
            \item Theorem-like environment \a{thmenv}
            \item \a{note}: upright and wrapped in parentheses 
            \item \a{text}: any text
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Example: preamble}<+->
        \begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
            \usepackage{amsthm}
            \newtheorem{theorem}{Theorem}
            \newtheorem{lemma}{Lemma}[section]
            \theoremstyle{definition}
            \newtheorem{definition}[theorem]{Definition}
            \theoremstyle{remark}
            \newtheorem{remark}[theorem]{Remark}
            \newtheorem*{note}{Note}
        \end{lstlisting}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Defining theorem-like environments}
    \begin{exampleblock}{Example (continued): body}
        Select \lstinline|\theoremstyle| before defining \lstinline|\newtheorem|
        \begin{pdflist}[listing options={basicstyle=\footnotesize\ttfamily, firstline=15, lastline=24}, comment style={scale=2}]
\documentclass{article}
\usepackage[papersize={6cm,7cm},
    scale=.9]{geometry}
\usepackage{amsthm}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{definition}
    [theorem]{Definition}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}
\newtheorem*{note}{Note}
\begin{document}

\begin{theorem}[hello] Text.
\end{theorem}
\begin{lemma} Text.
\end{lemma}
\begin{definition} Text.
\end{definition}
\begin{remark} Text.
\end{remark}
\begin{note} Text.
\end{note}

\section{Section}
\begin{theorem} Text.
\end{theorem}
\begin{lemma} Text.
\end{lemma}

\end{document}
        \end{pdflist}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Proof environment}
    \begin{codeexp}<+->
        \lstinline|\begin{proof}[§altproof§]|\\
        \tabstrut\a{text}\\
        \lstinline|\end{proof}|
        \begin{itemize}[<+->]
            \item Pre-defined proof environment
            \item \enquote{Proof} name is localised according to \lstinline|babel|
            \item \a{altproof} replaces \enquote{Proof} if specified
            \item Automatically places a QED symbol (square~\(\square\)) at the end of each proof
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\qedhere|
        \begin{itemize}[<+->]
            \item In certain cases, \lstinline|amsthm| fails to place the QED symbol correctly, e.\,g.\ when the proof finishes with an equation or with a list
            \item Place \lstinline|\qedhere| as last symbol on this line where it should appear
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Proof environment}
    \begin{pdflist}[listing options={basicstyle=\small\ttfamily, firstline=7, lastline=23}, comment style={scale=2}]
\documentclass{article}
\usepackage[papersize={6cm, 4cm},
    scale=.9]{geometry}
\usepackage{mathtools}
\usepackage{amsthm}
\begin{document}
\begin{proof} \end{proof}
\begin{proof}
    [Proof of Theorem~2]
\end{proof}

\begin{proof}
    \begin{equation*}
        a = b. \qedhere
    \end{equation*}
\end{proof}

\begin{proof} Text
    \begin{enumerate}
        \item Text.
            \qedhere
    \end{enumerate}
\end{proof}

\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Custom theorem style}
    \begin{codeexp}<+->
        \lstinline|\newtheoremstyle{§stylename§}|\\
        \tabstrut\lstinline|{§space-above§}{§space-below§}|\\
        \tabstrut\lstinline|{§body-font§}{§head-indent§}|\\
        \tabstrut\lstinline|{§head-font§}{§head-punct§}{§head-space§}|\\
        \tabstrut\lstinline|{§head-spec§}|
        \begin{itemize}[<+->]
            \item Custom theorem style; can be selected with \lstinline|\theoremstyle|
            \item \a{stylename}: name of theorem style
            \item \a{space-above}, \a{space-below}: length, default \lstinline|\topsep|\\
                Space above\slash below the theorem-like environment
            \item \a{body-font}, \a{head-font}: font commands, default empty\\
                Body\slash heading font specification, e.\,g.\ \lstinline|\sffamily\bfseries|
            \item \a{head-indent}: length, default empty\\
                Heading indent, e.\,g.\ \lstinline|\parindent|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Custom theorem style}
    \begin{itemize}[<+->]
        \item \a{head-space}: length, default \lstinline|5pt plus 1pt minus 1pt|\\
            Space after heading, can be \lstinline[showspaces]| | (space) for word space
        \item \a{head-punct}: string, default \lstinline|.|\\
            Punctuation after header
        \item \a{head-spec}: heading specification
            \begin{itemize}
                \item Default looks like the predefined theorem styles
                \item Can (and should) contain following
                    \begin{itemize}
                        \item \lstinline|\thmname{#1}|: theorem name
                        \item \lstinline|\thmnumber{#2}|: theorem number
                        \item \lstinline|\thmnote{#3}|: theorem note
                    \end{itemize}
                \item \lstinline|amsthm| drops certain parts depending on which \lstinline|\newtheorem| command is used
                \item E.\,g.\ the default for \a{head-spec}:
            \end{itemize}
    \end{itemize}

    \begin{actionenv}<+->
        \begin{lstlisting}
            \thmname{#1}\thmnumber{ #2}\thmnote{\textnormal{ (#3)}}
        \end{lstlisting}
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Bugs and strategies}
    \begin{block}{Brackets in theorem note}<+->
        \emph{Solution:} wrap optional argument in braces to hide it.
        \begin{lstlisting}
            \begin{theorem}[{ \cite[p.~87]{bib1} }] §\dots§
        \end{lstlisting}
    \end{block}

    \begin{block}{Named theorem like \emph{Krull's theorem}}<+->
        \emph{Solution:} Either give the name in the theorem note:
        \begin{lstlisting}
            \begin{theorem}[Krull's theorem]
                §\dots§
        \end{lstlisting}
        or define a new theorem:
        \begin{lstlisting}
            \theoremstyle{plain}
            \newtheorem*{eisenstein}{Eisenstein's criterion}
        \end{lstlisting}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Bugs and strategies}
    \begin{block}{Theorems starting with lists}<+->
        Two problems with theorem-like environments starting with lists:
        \begin{enumerate}
            \item Too much horizontal space between theorem heading and first item of list
            \item Vertical space after list is killed (for some reason I don't know)
        \end{enumerate}
        \begin{actionenv}<+->
            \emph{Solution:} avoid this by preceding the list with text, e.\,g.:
            \begin{itemize}
                \item \enquote{We have the following:}
                \item \enquote{The following hold true:}
                \item \enquote{\dots{} has the following properties:}
                \item \enquote{The following are equivalent:}
            \end{itemize}
        \end{actionenv}
    \end{block}

\end{frame}

\begin{frame}[fragile]{Bugs and strategies}
    \begin{exampleblock}{But what if I can't/don't want to precede by text?}<+->
        There is no real solution (since 2017).
        The best is probably to force the list on a new line by \lstinline|\leavevmode|:
        \begin{lstlisting}
            \begin{theorem} \leavevmode
                \begin{enumerate}
                    \item §\dots§
        \end{lstlisting}
        \begin{actionenv}<+->
            However, the theorem heading can be \emph{orphaned}, i.\,e.\ a page break could appear between heading and list \textrightarrow~place a \alert{manual} \lstinline|\newpage| before the heading (when finalising the document).
        \end{actionenv}
    \end{exampleblock}

    \begin{alertblock}{Tip}<+->
        Check out the \lstinline|thmtools| package for an easier and more powerful interface to defining theorem-like environments.
    \end{alertblock}
\end{frame}

\section{Typing derivatives}

\begin{frame}[fragile]{Loading \texttt{diffcoeff}}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{diffcoeff}|: \emph{differential coefficients}
        \begin{itemize}[<+->]
            \item Convenient interface to typeset derivatives in Leibniz notation
            \item Terminology: in \(\diff{f}{x}\), we call \(\diff{}{x}\) the \emph{differential operator}, \(f\)~the \emph{differentiand} and \(\dl{x}\) the \emph{differential}
            \item By default, typesets derivatives according to ISO standard
        \end{itemize}
    \end{codeexp}

    \begin{block}{\a{options}}<+->
        \begin{itemize}[<+->]
            \item \lstinline|spaced=§num§|: spacing before the differentiand
                \begin{itemize}[<1->]
                    \item \lstinline|1|: insert small space
                    \item \lstinline|0| (default): insert no space
                    \item \lstinline|-1|: insert small space only if differentianed has multiple tokens
                \end{itemize}
            \item \lstinline|mleftright|: load the \lstinline|mleftright| package to avoid spacing issues with \lstinline|\left \right| (cf.~\lstinline|\DeclarePairedDelimiter|)\\
                \alert{\textrightarrow~recommended}
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Simple derivatives and partial derivatives}
    \begin{codeexp}<+->
        \lstinline|\diff{§diff§}{§vars§}|, \lstinline|\diffp{§diff§}{§vars§}|\\
        \lstinline|\difs{§diff§}{§vars§}|, \lstinline|\difsp{§diff§}{§vars§}|\\
        \lstinline|\difc{§diff§}{§vars§}|, \lstinline|\difcp{§diff§}{§vars§}|
        \begin{itemize}[<+->]
            \item \a{diff}: differentiand;
            \item<.-> \a{vars}: (comma-separated list of) variable(s) of differentiation
            \item \lstinline|f|~for \enquote{fraction}, \lstinline|s|~for \enquote{slash}, \lstinline|c|~for \enquote{compact} form
            \item \lstinline|p|~for \enquote{partial} derivative
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{gather*}
\diff{f(x)}{x}, \diffp{f}{x} \\
\difs{f(x)}{x}, \difsp{f}{x} \\
\difc{f(x)}{x}, \difcp{f}{x} \\
\diff{}{x}, \difc{f}{}
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Higher order derivatives and partial derivatives}
    \begin{exampleblock}{Notation}<+->
        From now on, we write \lstinline|\difx| for the collective \lstinline|\diff|, \lstinline|\difs|, \lstinline|\difc|.
        Similarly for \lstinline|\difxp|.
    \end{exampleblock}

    \begin{codeexp}<+->
        \lstinline|\difx[§ord§]{§diff§}{§var§}|\\
        \lstinline|\difxp[§ord§]{§diff§}{§var§}|
        \begin{itemize}[<+->]
            \item \a{ord}: order of differentiation, can be any math expression
            \item \lstinline|1| for \a{ord} has no effect
            \item Automatically wraps \a{var} in parentheses if it has multiple tokens
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.5\textwidth, sidebyside gap=0pt]<+->
\begin{gather*}
\diff[n+1]{f}{x},
\difs[n+1]{f}{x},
\difcp[n+1]{f}{x} \\
\diff[2]{\ln \sin x}{\sin x}
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Starred variants}
    \begin{codeexp}<+->
        \lstinline|\difx*{§diff§}{§var§}|\\
        \lstinline|\difxp*{§diff§}{§var§}|
        \begin{itemize}[<+->]
            \item Appends \a{diff} to the differential operator
            \item Makes it easy to compare both versions
            \item Has no effect for compact form
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{gather*}
\diff*{(ax^2 + bx + c)}{x} \\
\diffp*{f(x,y,z)}{x} \\
\difs*{(ax^2 + bx + c)}{x}
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Starred variants}
    \begin{codeexp}<+->
        \lstinline|\difx**{§var§}{§diff§}|\\
        \lstinline|\difxp**{§var§}{§diff§}|
        \begin{itemize}[<+->]
            \item Appends \a{diff} \emph{and} reverses the order of arguments \a{var} and \a{diff}
            \item Makes it easier to write for complicated differentiands
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{gather*}
\diff**{x}{ (ax^2 + bx + c) } \\
\diffp**{x}{ \frac{1}{\sqrt{x^2 + y^2}} }
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Point of evaluation, variables held constant}
    \begin{codeexp}<+->
        \lstinline|\difx{§diff§}{§var§}[§eval§]|\\
        \lstinline|\difxp{§diff§}{§var§}[§eval§]|
        \begin{itemize}[<+->]
            \item Wraps the derivative in parentheses (in default settings) and places \a{eval} as a subscript
            \item \alert{No space} is allowed between derivative and \lstinline|[§eval§]|
            \item Superscripts can be appended, but change parentheses to brackets or a vertical bar for better results
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]<+->
\begin{gather*}
\diff{f(x)}{x}[x=0],
\diffp*{\frac{P}{T}}{U}[V] \\
\difs*{f}{x}[x=0] \\
\diff{\sin x}{x}[0]^{\pi/2}
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Mixed partial derivatives}
    \begin{codeexp}<+->
        \lstinline|\difxp{§diff§}{§var1§, §var2§, §\dots§}|
        \begin{itemize}[<+->]
            \item Comma-separated list of variables \a{vari}
            \item Total order of differentiation is automatically calculated
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\[
\diffp{f}{x,y,z},
\difcp{f}{x,y,z}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Mixed partial derivatives of higher order}
    \begin{codeexp}<+->
        \lstinline|\difxp[§ord1§, §\dots§]<§totord§>{§diff§}{§var1§, §\dots§}|\\
        \lstinline|\difxp<§totord§>{§diff§}{§var1§:§ord1§, §var2§:§ord2§, §\dots§}|
        \begin{itemize}[<+->]
            \item Specify for each \a{vari} its order of differentiation \a{ordi}
            \item Both versions are synonymous
                \begin{itemize}
                    \item First command: missing orders at the end of \lstinline|[§ord1§, §\dots§]| are interpreted as~\lstinline|1|
                    \item Second command: missing \lstinline|:§ordi§| is interpreted as~\lstinline|1|
                \end{itemize}
            \item Specifying total order \a{totord} is \alert{recommended}
                \begin{itemize}
                    \item \lstinline|diffcoeff|'s algorithm for calculating the total order can lead to unexpected results
                    \item Sometimes even throws an \alert{error}
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Mixed partial derivatives of higher order}
    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]<+->
\begin{gather*}
\diffp[2,3]{F}{x,y,z},
\diffp{F}{x:2,y:3,z}
\end{gather*}
    \end{code}

    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]<+->
\begin{gather*}
\diffp{F}{x, y:km+1, z:m+k-1} \\
\diffp<(k+1)(m+1)>{F}
    {x,y:km+1,z:m+k-1}
\end{gather*}
    \end{code}

    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]<+->
\[
% \diffp{F}{x:2^k, y} error!
\diffp<2^k+1>{F}{x:2^k, y}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Full specification}
    \begin{lstlisting}
        \difx*[§ord1§, §\dots§]<§totord§>{§diff§}{§var1§, §\dots§}[§eval§]
        \difx*<§totord§>{§diff§}{§var1§:§ord1§, §\dots§}[§eval§]
    \end{lstlisting}
    \begin{itemize}
        \item Same for \lstinline|\difxp|
        \item Second star swaps \lstinline|{§diff§}| and \lstinline|{§var1§, §\dots§}|
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Differentials and Jacobians}
    \begin{codeexp}<+->
        \lstinline|\dl[§ord1§, §\dots§]{§var1§, §\dots§}^{§exp§}|
        \lstinline|\dl{§var1§:§ord1§, §\dots§}^{§exp§}|
        \begin{itemize}[<+->]
            \item Creates differential
            \item Alternative: \lstinline|\difl| instead of \lstinline|\dl|
            \item \a{exp}: raises all variables to the \a{exp}, overwrites the \a{ordi}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\jacob{§f1§, §f2§, §\dots§}{§x1§, §x2§, §\dots§}|
        \begin{itemize}[<+->]
            \item Creates Jacobian with~\(\partial\)
            \item Always in fraction style
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]<+->
\begin{gather*}
\iint\limits_V f(x,y) \dl{x,y},
\iint\limits_V f \dl[2]{x} \\
\jacob{f_1,f_2,f_3}{x_1,x_2,x_3}
\end{gather*}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Customising differentials}
    \begin{codeexp}<+->
        \lstinline|\difdef{§cmds§}{§name§}{§settings§}|
        \begin{itemize}[<+->]
            \item General command to change default settings and create variant forms
            \item \a{cmds}: a~comma-separated list of the following
                \begin{itemize}[<1->]
                    \item \lstinline|f|, \lstinline|fp|: \enquote{fraction} style
                    \item \lstinline|s|, \lstinline|sp|: \enquote{slash} style
                    \item \lstinline|c|, \lstinline|cp|: \enquote{compact} style
                    \item \lstinline|l|: differential
                    \item \lstinline|j|: Jacobians
                \end{itemize}
            \item \a{name}: a~name for a variant
                \begin{itemize}
                    \item Can consist of letters, numbers, mathematical symbols like \lstinline|+|~and~\lstinline|=|
                    \item Variant can be accessed by \lstinline|\difx.§name§.| instead of \lstinline|\difx|, similar for \lstinline|\difxp|
                    \item If left \alert{empty}, settings change the default
                \end{itemize}
            \item \a{settings}: a~myriad of settings, see \lstinline|diffcoeff| docs,~§3.2
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Example: tensor calculus}
    \begin{code}[lefthand width=.6\textwidth]
\difdef{ f, s }{ n }{
  op-symbol = \nabla,
  op-symbol-alt = \mathrm{d}
}
\[ \diff.n.{v^i}{t} \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Example: wrapping multi-token variables}
    \begin{code}
\difdef{ f, fp }{ (dv) }{
  long-var-wrap = (dv)
}
\difdef{ f, fp }{ dv }{
  long-var-wrap = dv
}
\[
  \diff      [2]{f}{x_i},
  \diff.(dv).[2]{f}{x_i},
  \diff.dv.  [2]{f}{x_i}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Example: point of evaluation}
    \begin{code}<+->
\difdef{ f, fp, s, sp }{}{
  outer-Ldelim = \left.,
% Maybe explicit '\Big|'
  outer-Rdelim = \right|,
  sub-nudge = 0mu
}
\[ \diff{f}{x}[x=0],
   \difs{f}{x}[x=0] \]
    \end{code}

    \begin{code}<+->
\difdef{ f, fp, s, sp }{}{
  outer-Ldelim = \left[,
  outer-Rdelim = \right],
  sub-nudge = 0mu
}
\[ \diff{f}{x}[x=0],
   \difs{f}{x}[x=0] \]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Example: italic~d}
    \begin{code}
\difdef{ f, s, c, l }{}{
  op-symbol = d,
% Tiny space between 'd'
% and order of diff.
  op-order-nudge = 1mu
}
\[
  \diff[2]{f}{x},
  \difs[2]{f}{x},
  \dl[2]{x}
\]
    \end{code}
\end{frame}

\begin{frame}[fragile]{Example: higher order differentials}
    \begin{code}[lefthand width=.6\textwidth]
\difdef{ l }{}{
  style = d^,
% Why do I need to set this
% option? A bug?
  op-symbol = \mathrm{d}
}
\[
  \iiint\limits_{V} f(x) \dl[3]x
\]
    \end{code}
\end{frame}

\section{Symbols}

\begin{frame}[fragile]{Symbol types}
    \begin{itemize}[<+->]
        \item \LaTeX{} employs a very complicated system for finding correct spacing between two math symbols
        \item 8~types, e.\,g.\
            \begin{itemize}
                \item<.-> Ordinary (Ord), \lstinline|\mathord{§\dots§}|, e.\,g.\ \(a\),~\(0\),~\(\emptyset\)
                \item Operator (Op), \lstinline|\mathop{§\dots§}|, e.\,g.\ \(\sum\),~\(\prod\)
                \item Binary operator (Bin), \lstinline|\mathbin{§\dots§}|, e.\,g.\ \(+\),~\(-\),~\(\otimes\)
                \item Relation (Rel), \lstinline|\mathrel{§\dots§}|, e.\,g.\ \(=\),~\(\le\),~\(\subseteq\)
                \item Opening/closing delimiter (Open/Close), \lstinline|\mathopen{§\dots§}|\slash \lstinline|\mathclose{§\dots§}|, e.\,g.\ \((\;)\), \(\langle \; \rangle\)
            \end{itemize}
    \end{itemize}

    \begin{block}{Looking up symbols}<+->
        \begin{itemize}
            \item \href{https://detexify.kirelabs.org/classify.html}{Detexify} (AI-powered)
            \item \href{https://ctan.org/pkg/comprehensive}{The Comprhenesive \LaTeX{} Symbol List}
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Symbol types}
    \begin{exampleblock}{Some notes on \LaTeX{}'s system}
        \begin{itemize}[<+->]
            \item \LaTeX{} converts Bin to Ord depending on context\\
                E.\,g.\ \(+x\) (no space) vs.\ \(y+x\) (thin spaces)
            \item Bin can be forced to be Ord by inserting an \alert{empty Ord} \lstinline|{}|\\
                E.\,g.\ \lstinline|\({} + x\)| gives \({}+x\) (thin space)
            \item Compared to Bin, Rel has more space surrounding it
                \begin{code}[listing options={lastline=3}]
\(a + b\)

\(a \mathrel{+} b\)
\hspace{-1.92ex}\smash{\rule{.4pt}{2\baselineskip}}\hspace{.15ex}\smash{\rule{.4pt}{2\baselineskip}}
                \end{code}
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Extra packages}
    \begin{itemize}[<+->]
        \item Many symbols are already available with standard \LaTeX{}
        \item<.-> Many more symbols available through extra packages
        \item \lstinline|amssymb|: strongly recommended
            \begin{itemize}[<1->]
                \item Provides widely used extra symbols
                \item Provides \emph{Fraktur} and \emph{Blackboard math} fonts
            \end{itemize}
        \item \lstinline|stmaryrd|: also very famous
            \begin{itemize}[<1->]
                \item Even more symbols for theoretical computer science (process algebra, functional programming, linear logic, etc.)
                \item Has to be loaded \emph{after} \lstinline|amssymb|
            \end{itemize}
        \item Others: \lstinline|euscript|, \lstinline|mathrsfs|, \lstinline|mathabx|, \lstinline|stix|, \lstinline|yhmath|, etc.
    \end{itemize}

    \begin{exampleblock}{Note}<+->
        We'll focus on \lstinline|amssymb| only.
        Symbols/features only available with \lstinline|amssymb| are marked with a \alert{\emph{double dagger}~\ddag}.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Ord type (selection)}
    \begin{block}{Typical symbols}
        \begin{tabular}{@{}*6{r@{\hspace{\tabcolsep}}l@{\hspace{.75cm}}}@{}}
            \(a\) & \lstinline|a|
            & \(A\) & \lstinline|A|
            & \(0\) & \lstinline|0|
            & \(?\) & \lstinline|?|
            & \(!\) & \lstinline|!|
            & \(.\) & \lstinline|.| \\
            \(/\) & \lstinline|/|
            & \(\#\) & \lstinline|\#|
            & \(\%\) & \lstinline|\%|
            & \('\) & \lstinline|'|
        \end{tabular}
    \end{block}

    \begin{block}{Greek letters}<2->
        \begin{tabular}{@{}*4{r@{\hspace{\tabcolsep}}l}@{}}
            \(\alpha\) & \lstinline|\alpha|
            & \(\beta\) & \lstinline|\beta|
            & \(\gamma\) & \lstinline|\gamma|
            & \(\delta\) & \lstinline|\delta| \\ \hline
            \(\Gamma\) & \lstinline|\Gamma|
            & \(\Delta\) & \lstinline|\Delta|
            & \(\Theta\) & \lstinline|\Theta|
            & \(\Lambda\) & \lstinline|\Lambda| \\
            \(\Xi\) & \lstinline|\Xi|
            & \(\Pi\) & \lstinline|\Pi|
            & \(\Sigma\) & \lstinline|\Sigma|
            & \(\Phi\) & \lstinline|\Phi| \\
            \(\Psi\) & \lstinline|\Psi|
            & \(\Omega\) & \lstinline|\Omega|
            & \(\varGamma\) & \lstinline|\varGamma|
            & \(\varDelta\) & \lstinline|\varDelta| \\ \hline
            \(\epsilon\) & \lstinline|\epsilon|
            & \(\varepsilon\) & \lstinline|\varepsilon|
            & \(\theta\) & \lstinline|\theta|
            & \(\vartheta\) & \lstinline|\vartheta| \\
            \(\kappa\) & \lstinline|\kappa|
            & \(\varkappa\) & \lstinline|\varkappa|\alert{\ddag}
            & \(\pi\) & \lstinline|\pi|
            & \(\varpi\) & \lstinline|\varpi| \\
            \(\rho\) & \lstinline|\rho|
            & \(\varrho\) & \lstinline|\varrho|
            & \(\phi\) & \lstinline|\phi|
            & \(\varphi\) & \lstinline|\varphi|
        \end{tabular}
    \end{block}

    \begin{exampleblock}{Quick question}<3->
        Why is there no \lstinline|\Alpha|, \lstinline|\Beta|, etc.?
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Ord type (selection)}
    \begin{block}{Other symbols}
        \begin{tabular}{@{}*3{r@{\hspace{\tabcolsep}}l}@{}}
            \(\aleph\) & \lstinline|\aleph|
            & \(\beth\) & \lstinline|\beth|\alert{\ddag}
            & \(\ell\) & \lstinline|\ell| \\
            \(\Im\) & \lstinline|\Im|
            & \(\Re\) & \lstinline|\Re|
            & \(\hbar\) & \lstinline|\hbar| \\
            \(\imath\) & \lstinline|\imath|
            & \(\jmath\) & \lstinline|\jmath|
            & \(\partial\) & \lstinline|\partial| \\
            \(\wp\) & \lstinline|\wp|
            & \(\angle\) & \lstinline|\angle|\alert{\ddag}
            & \(\measuredangle\) & \lstinline|\measuredangle|\alert{\ddag} \\
            \(\flat\) & \lstinline|\flat|
            & \(\natural\) & \lstinline|\natural|
            & \(\sharp\) & \lstinline|\sharp| \\
            \(\infty\) & \lstinline|\infty|
            & \(\nabla\) & \lstinline|\nabla|
            & \(\backslash\) & \lstinline|\backslash| \\
            \(\heartsuit\) & \lstinline|\heartsuit|
            & \(\diamondsuit\) & \lstinline|\diamondsuit|
            & \(\clubsuit\) & \lstinline|\clubsuit| \\
            \(\spadesuit\) & \lstinline|\spadesuit|
            & \(\emptyset\) & \lstinline|\emptyset|
            & \(\varnothing\) & \lstinline|\varnothing|\alert{\ddag} \\
            \(\bot\) & \lstinline|\bot|
            & \(\top\) & \lstinline|\top|
            & \(\neg\) & \lstinline|\neg| \\
            \(\exists\) & \lstinline|\exists|
            & \(\nexists\) & \lstinline|\nexists|\alert{\ddag}
            & \(\forall\) & \lstinline|\forall| \\
            \(\square\) & \lstinline|\square|\alert{\ddag}
            & \(\blacksquare\) & \lstinline|\blacksquare|\alert{\ddag}
            & \(\triangle\) & \lstinline|\triangle| \\
            \(|\) & \lstinline+|+
            & \(\|\) & \lstinline+\|+
        \end{tabular}
    \end{block}
    
    \begin{block}{Synonyms}
        \lstinline|\vert| for \lstinline+|+, \lstinline|\Vert| for \lstinline+\|+, \lstinline|\lnot| for \lstinline|\neg|
    \end{block}
\end{frame}

\begin{frame}[fragile]{Ord type (selection)}
    \begin{block}{Accents}
        \begin{tabular}{@{}*3{r@{\hspace{\tabcolsep}}l}}
            \(\dot{a}\) & \lstinline|\dot{a}|
            & \(\ddot{a}\) & \lstinline|\ddot{a}|
            & \(\dddot{a}\) & \lstinline|\dddot{a}|\alert{\ddag} \\
            \(\bar{a}\) & \lstinline|\bar{a}|
            & \(\hat{a}\) & \lstinline|\hat{a}|
            & \(\tilde{a}\) & \lstinline|\tilde{a}| \\
            \(\mathring{a}\) & \lstinline|\mathring{a}|
            & \(\vec{a}\) & \lstinline|\vec{a}|
        \end{tabular}
    \end{block}

    \begin{exampleblock}{Note}<2->
        \begin{itemize}
            \item Accents turn any symbol into Ord; redeclare symbol type if necessary
            \item Accents can be stacked
        \end{itemize}
    \end{exampleblock}

    \begin{code}<3->
\(a \hat{=} b\),
\(a \mathrel{\hat{=}} b\),
\(\hat{\bar{a}}\)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Bin type (selection)}
    \begin{tabular}{@{}*4{r@{\hspace{\tabcolsep}}l}@{}}
        \(+\) & \lstinline|+|
        & \(-\) & \lstinline|-|
        & \(\pm\) & \lstinline|\pm|
        & \(\mp\) & \lstinline|\mp| \\
        \(*\) & \lstinline|*|
        & \(\cdot\) & \lstinline|\cdot|
        & \(\times\) & \lstinline|\times|
        & \(\rtimes\) & \lstinline|\rtimes|\alert{\ddag} \\
        \(\oplus\) & \lstinline|\oplus|
        & \(\ominus\) & \lstinline|\ominus|
        & \(\otimes\) & \lstinline|\otimes|
        & \(\odot\) & \lstinline|\odot| \\
        \(\boxplus\) & \lstinline|\boxplus|\alert{\ddag}
        & \(\boxminus\) & \lstinline|\boxminus|\alert{\ddag}
        & \(\boxtimes\) & \lstinline|\boxtimes|\alert{\ddag}
        & \(\boxdot\) & \lstinline|\boxdot|\alert{\ddag} \\
        \(\bullet\) & \lstinline|\bullet|
        & \(\star\) & \lstinline|\star|
        & \(\circ\) & \lstinline|\circ|
        & \(\amalg\) & \lstinline|\amalg| \\
        \(\cup\) & \lstinline|\cup|
        & \(\sqcup\) & \lstinline|\sqcup|
        & \(\cap\) & \lstinline|\cap|
        & \(\setminus\) & \lstinline|\setminus| \\
        \(\vee\) & \lstinline|\vee|
        & \(\wedge\) & \lstinline|\wedge|
        & \(\dag\) & \lstinline|\dag|
        & \(\ddag\) & \lstinline|\ddag|
    \end{tabular}

    \begin{block}{Synonyms}
        \lstinline|\ast| for \lstinline|*|, \lstinline|\land| for \lstinline|\wedge|, \lstinline|\lor| for \lstinline|\vee|, \lstinline|\dagger| for \lstinline|\dag|, \lstinline|\ddagger| for \lstinline|\ddag|
    \end{block}
\end{frame}

\begin{frame}[fragile]{Rel type (selection)}
    \begin{block}{Order and set symbols}
        \begin{tabular}{@{}*4{r@{\hspace{\tabcolsep}}l}@{}}
            \(<\) & \lstinline|<|
            & \(\nless\) & \lstinline|\nless|\alert{\ddag}
            & \(>\) & \lstinline|>|
            & \(\ngtr\) & \lstinline|\ngtr|\alert{\ddag} \\
            \(\le\) & \lstinline|\le|
            & \(\ll\) & \lstinline|\ll|
            & \(\leqq\) & \lstinline|\leqq|\alert{\ddag}
            & \(\leqslant\) & \lstinline|\leqslant|\alert{\ddag} \\
            \(\ge\) & \lstinline|\ge|
            & \(\gg\) & \lstinline|\gg|
            & \(\geqq\) & \lstinline|\geqq|\alert{\ddag}
            & \(\geqslant\) & \lstinline|\geqslant|\alert{\ddag} \\
            \(\lneq\) & \lstinline|\lneq|\alert{\ddag}
            & \(\nleq\) & \lstinline|\nleq|\alert{\ddag}
            & \(\gneq\) & \lstinline|\gneq|\alert{\ddag}
            & \(\ngeq\) & \lstinline|\ngeq|\alert{\ddag} \\
            \(\prec\) & \lstinline|\prec|
            & \(\preceq\) & \lstinline|\preceq|
            & \(\succ\) & \lstinline|\succ|
            & \(\succeq\) & \lstinline|\succeq| \\
            \(=\) & \lstinline|=|
            & \(\ne\) & \lstinline|\ne|
            & \(\sim\) & \lstinline|\sim|
            & \(\nsim\) & \lstinline|\nsim|\alert{\ddag} \\
            \(\approx\) & \lstinline|\approx|
            & \(\equiv\) & \lstinline|\equiv|
            & \(\cong\) & \lstinline|\cong|
            & \(\ncong\) & \lstinline|\ncong|\alert{\ddag} \\
            \(\simeq\) & \lstinline|\simeq|
            & \(\in\) & \lstinline|\in|
            & \(\ni\) & \lstinline|\ni|
            & \(\notin\) & \lstinline|\notin| \\
            \(\subset\) & \lstinline|\subset|
            & \(\subseteq\) & \lstinline|\subseteq|
            & \(\subsetneq\) & \lstinline|\subsetneq|\alert{\ddag}
            & \(\nsubseteq\) & \lstinline|\nsubseteq|\alert{\ddag} \\
            \(\supset\) & \lstinline|\supset|
            & \(\supseteq\) & \lstinline|\supseteq|
            & \(\supsetneq\) & \lstinline|\supsetneq|\alert{\ddag}
            & \(\nsupseteq\) & \lstinline|\nsupseteq|\alert{\ddag}
        \end{tabular}

        \(\vartriangleleft\) \lstinline|\vartriangleleft|\alert{\ddag};
        \(\trianglelefteq\) \lstinline|\trianglelefteq|\alert{\ddag};\\
        \(\coloneq\) \lstinline|\coloneq|\alert{\dag};
        \(\eqcolon\) \lstinline|\eqcolon|\alert{\dag}
    \end{block}

    \begin{block}{Synonyms}
        \lstinline|\geq| for \lstinline|\ge|, \lstinline|\leq| for \lstinline|\le|, \lstinline|\neq| for \lstinline|\ne|
    \end{block}
\end{frame}

\begin{frame}[fragile]{Rel type (selection)}
    \begin{block}{Arrows}
        \begin{tabular}{@{}*2{r@{\hspace{\tabcolsep}}l}@{}}
            \(\to\) & \lstinline|\to|
            & \(\longrightarrow\) & \lstinline|\longrightarrow| \\
            \(\gets\) & \lstinline|\gets|
            & \(\longleftarrow\) & \lstinline|\longleftarrow| \\
            \(\leftrightarrow\) & \lstinline|\leftrightarrow|
            & \(\longleftrightarrow\) & \lstinline|\longleftrightarrow| \\
            \(\mapsto\) & \lstinline|\mapsto|
            & \(\longmapsto\) & \lstinline|\longmapsto| \\
            \(\Rightarrow\) & \lstinline|\Rightarrow|
            & \(\Longrightarrow\) & \lstinline|\Longrightarrow| \\
            \(\Leftrightarrow\) & \lstinline|\Leftrightarrow|
            & \(\Longleftrightarrow\) & \lstinline|\Longleftrightarrow| \\
            \(\dasharrow\) & \lstinline|\dasharrow|\alert{\ddag}
            & \(\Leftarrow\) & \lstinline|\Leftarrow| \\
            \(\hookrightarrow\) & \lstinline|\hookrightarrow|
            & \(\twoheadrightarrow\) & \lstinline|\twoheadrightarrow|\alert{\ddag} \\
            \(\rightrightarrows\) & \lstinline|\rightrightarrows|\alert{\ddag}
            & \(\rightleftarrows\) & \lstinline|\rightleftarrows|\alert{\ddag} \\
            \(\nearrow\) & \lstinline|\nearrow|
            & \(\searrow\) & \lstinline|\searrow| \\
            \(\nrightarrow\) & \lstinline|\nrightarrow|\alert{\ddag}
            & \(\nleftarrow\) & \lstinline|\nleftarrow|\alert{\ddag}
        \end{tabular}
    \end{block}

    \begin{block}{Synonyms}
        \lstinline|\rightarrow| for \lstinline|\to|, \lstinline|\leftarrow| for \lstinline|\gets|, \lstinline|\dashrightarrow| for \lstinline|\dasharrow|
    \end{block}
\end{frame}

\begin{frame}[fragile]{Rel type (selection)}
    \begin{block}{Special arrows}
        \begin{itemize}
            \item \(\implies\) \lstinline|\implies| has more spacing than \lstinline|\Longrightarrow|
            \item \(\iff\) \lstinline|\iff| has more spacing than \lstinline|\Longleftrightarrow|
        \end{itemize}
    \end{block}

    \begin{block}{Other symbols}<2->
        \begin{tabular}{@{}*4{r@{\hspace{\tabcolsep}}l}@{}}
             \(:\) & \lstinline|:|
            & \(\parallel\) & \lstinline|\parallel|
            & \(\nparallel\) & \lstinline|\nparallel|\alert{\ddag}
            & \(\perp\) & \lstinline|\perp| \\
            \(\mid\) & \lstinline|\mid|
            & \(\nmid\) & \lstinline|\nmid|\alert{\ddag}
            & \(\smile\) & \lstinline|\smile|
            & \(\frown\) & \lstinline|\frown| \\
            \(\vdash\) & \lstinline|\vdash|
            & \(\dashv\) & \lstinline|\dashv|
            & \(\models\) & \lstinline|\models|
            & \(\vDash\) & \lstinline|\vDash|\alert{\ddag}
        \end{tabular}
    \end{block}

    \begin{block}{Negation of symbols}<3->
        \begin{itemize}
            \item If \lstinline|amssymb| or \LaTeX{} provide negated symbols, use them
            \item If one symbols does not exist in negated form, write \lstinline|\not|, \(\not\) before it, e.\,g.\ \(\not \simeq\)\\
                \textrightarrow{} can lead to suboptimal results
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Op type (selection)}
    \begingroup
    \renewcommand{\arraystretch}{1.7}
    \begin{tabular}{@{}*2{r@{\hspace{\tabcolsep}}r@{\hspace{\tabcolsep}}l}@{}}
        \(\int\) & \(\displaystyle \int\) & \lstinline|\int|
        & \(\iint\) & \(\displaystyle \iint\) & \lstinline|\iint| \\
        \(\iiint\) & \(\displaystyle \iiint\) & \lstinline|\iiint|
        & \(\iiiint\) & \(\displaystyle \iiiint\) & \lstinline|\iiiint|\\
        \(\idotsint\) & \(\displaystyle \idotsint\) & \lstinline|\idotsint|
        & \(\oint\) & \(\displaystyle \oint\) & \lstinline|\oint|\\
        \(\bigcup\) & \(\displaystyle \bigcup\) & \lstinline|\bigcup|
        & \(\bigcap\) & \(\displaystyle \bigcap\) & \lstinline|\bigcap|\\
        \(\bigvee\) & \(\displaystyle \bigvee\) & \lstinline|\bigvee|
        & \(\bigwedge\) & \(\displaystyle \bigwedge\) & \lstinline|\bigwedge|\\
        \(\sum\) & \(\displaystyle \sum\) & \lstinline|\sum|
        & \(\bigtimes\) & \(\displaystyle \bigtimes\) & \lstinline|\bigtimes|\alert{\dag}\\
        \(\prod\) & \(\displaystyle \prod\) & \lstinline|\prod|
        & \(\coprod\) & \(\displaystyle \coprod\) & \lstinline|\coprod|\\
        \(\bigoplus\) & \(\displaystyle \bigoplus\) & \lstinline|\bigoplus|
        & \(\bigotimes\) & \(\displaystyle \bigotimes\) & \lstinline|\bigotimes|\\
        \(\bigodot\) & \(\displaystyle \bigodot\) & \lstinline|\bigodot|
        & \(\bigsqcup\) & \(\displaystyle \bigsqcup\) & \lstinline|\bigsqcup|
    \end{tabular}
    \endgroup
\end{frame}

\begin{frame}[fragile]{Delimiter type (selection)}
    \begingroup
    \renewcommand{\arraystretch}{2}
    \begin{tabular}{@{}*2{r@{\hspace{\tabcolsep}}r@{\hspace{\tabcolsep}}l}@{}}
        \((\;)\) & \(\biggl(\;\biggr)\) & \lstinline|( )|
        & \([\;]\) & \(\biggl[\;\biggr]\) & \lstinline|[ ]|\\
        \(\{\;\}\) & \(\biggl\{\;\biggr\}\) & \lstinline|\{ \}|
        & \(\langle \; \rangle\) & \(\biggl\langle\;\biggr\rangle\) & \lstinline|\langle \rangle|\\
        \(\lceil \; \rceil\) & \(\biggl\lceil\;\biggr\rceil\) & \lstinline|\lceil \rceil|
        & \(\lfloor \; \rfloor\) & \(\biggl\lfloor\;\biggr\rfloor\) & \lstinline|\lfloor \rfloor|\\
        \(\lvert \; \rvert\) & \(\biggl\lvert\;\biggr\rvert\) & \lstinline|\lvert \rvert|
        & \(\lVert \; \rVert\) & \(\biggl\lVert\;\biggr\rVert\) & \lstinline|\lVert \rVert|
    \end{tabular}
    \endgroup
    \begin{block}{Synonyms}
        \lstinline|\lparen \rparen|\alert{\dag} for \lstinline|( )|, \lstinline|\lbrack \rbrack| for \lstinline|[ ]|, \lstinline|\lbrace \rbrace| for \lstinline|\{ \}|
    \end{block}
\end{frame}

\begin{frame}{Common pitfalls}
    \begin{exampleblock}{Exercise*}
        \(|\)~is one of the most overused symbols in Mathematics.
        What pipe command has to be used?
        \begin{enumerate}
            \item \(a \mid b\) (divisibility)
            \item \(\{n \in \mathbb{N} \mid n \text{ odd}\}\) (set notation)
            \item \(f|_U\) (restriction)
            \item \(\frac{\partial f}{\partial x}\big|_{(x,y) = (a,b)}\) (evaluation of partial derivative)
            \item \(D(\mathfrak{P} | \mathfrak{p})\) (decomposition group)
            \item \(\langle \phi | \psi \rangle\) (inner product)
            \item \(\lvert x \rvert\) (absolute value)
            \item \(\langle \phi \rvert\), \(\lvert \phi \rangle\) (bra-ket notation)
            \item \(\ell \parallel k\) (parallel)
            \item \(\rVert x \lVert\) (norm)
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Common pitfalls}
    \begin{exampleblock}{\emph{\normalcolor{Solution}}}
        \begin{enumerate}
            \item \lstinline|a \mid b|
            \item \lstinline|\{n \in \mathbb{N} \mid n \text{ odd}}|
            \item \lstinline+f|_U+
            \item \lstinline+\frac{\partial f}{\partial x} \big|_{(x,y) = (a,b)}+
            \item \lstinline+D(\mathfrak{P} | \mathfrak{p})+
            \item \lstinline+\langle \phi | \psi \rangle+
            \item \lstinline|\lvert x \rvert|
            \item \lstinline|\langle \phi \rvert|, \lstinline|\lvert \phi \rangle|
            \item \lstinline|\ell \parallel k|
            \item \lstinline|\rVert x \lVert|
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Common pitfalls}
    \begin{itemize}[<+->]
        \item Accents on~\(i\): \(\hat{\imath}\) (\lstinline|\imath|), not \(\hat{i}\) (\lstinline|i|);
            similar for~\(\hat{\jmath}\) (\lstinline|\jmath|)
        \item Pushforward measure: \(f_{\sharp} \mu\) (\lstinline|\sharp|), not \(f_{\#} \mu\) (\lstinline|\#|)
        \item Cardinality: \(\# S\), not \(\sharp S\)
        \item Complement: \(A \setminus B\) (\lstinline|\setminus|), not \(A \backslash B\) (\lstinline|\backslash|)
        \item Empty set: \(\emptyset\)~(\lstinline|\emptyset|) or \(\varnothing\)~(\lstinline|\varnothing|), not \O~(\lstinline|\O|)
        \item Much less: \(\ll\) (\lstinline|\ll|), not \(<<\)~(\lstinline|<<|);\\
            similar for \(\gg\) (\lstinline|\gg|)
        \item Implies (not in logic): \(\implies\) (\lstinline|\implies|), not \(\Rightarrow\) (\lstinline|\Rightarrow|);\\
            similar for \(\iff\) (\lstinline|\iff|)
        \item Functions: \(f \colon A \to B\) (\lstinline|colon|), not \(f : A \to B\) (\lstinline|:|)
        \item Set notation: \(\{x \mid f(x)\}\) (\lstinline|\mid|) or \(\{x : f(x)\}\) (\lstinline|:|), not \(\{x | f(x)\}\) (\lstinline+|+)
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Common pitfalls}
    \begin{itemize}[<+->]
        \item Divides: \(a \mid b\) (\lstinline|\mid|), not \(a|b\) (\lstinline+|+)
        \item Parallel: \(\ell \parallel k\) (\lstinline|\parallel|), not \(\ell || k\) (\lstinline+||+) or \(\ell \| k\) (\lstinline+\|+)
        \item Sum: \(\sum_{k=1}^n k\) (\lstinline|\sum|), not \(\Sigma_{k=1}^n k\) (\lstinline|\Sigma|);\\
            similar for \(\prod\) (\lstinline|\prod|)
        \item Big operators: \(\bigoplus_{i \in I} M_i\) (\lstinline|\bigoplus|), not \(\oplus_{i \in I} M_i\) (\lstinline|\oplus|);
            similar for \(\bigotimes\) (\lstinline|\bigotimes|), \(\bigcup\) (\lstinline|\cup|), \(\bigcap\) (\lstinline|\biccap|), etc.\
        \item Multi-integral: \(\iint_A\) (\lstinline|\iint|), not \(\int \int_A\) (\lstinline|\int \int|)
        \item Absolute value: \(\lvert x \rvert\) (\lstinline|\lvert \rvert|), not \(|x|\) (\lstinline+| |+)
        \item Norm: \(\lVert x \rVert\) (\lstinline|\lVert \rVert|), not \(\|x\|\) (\lstinline+\| \|+)
        \item Dot product: \(\langle v,w \rangle\) (\lstinline|\langle \rangle|), not \(<v,w>\) (\lstinline|< >|)
        \item Half-open interval: \(\mathopen{]} a,b]\) (\lstinline|\mathopen{]} ]|), not \(]a,b]\) (\lstinline|] ]|)
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Math fonts}
    \begin{itemize}[<+->]
        \item Analogous to text fonts:
            \begin{itemize}[<1->]
                \item Roman \lstinline|\mathrm|
                \item Sans serif \lstinline|\mathsf|
                \item Typewriter \lstinline|\mathtt|
                \item Boldface \lstinline|\mathbf|
                \item Italics \lstinline|\mathit|
                \item Normal \lstinline|\mathnormal|
            \end{itemize}
        \item Math font commands are \alert{not} cumulative
    \end{itemize}

    \begin{code}[lefthand width=.6\textwidth]<+->
\(\mathrm{3x}, \mathsf{3x},
  \mathtt{3x}, \mathbf{3x},
  \mathit{3x}\)

\(\mathbf{\mathit{3x}}\)
    \end{code}
\end{frame}

\begin{frame}[fragile]{Math fonts}
    \begin{block}{Special math fonts}<+->
        \begin{itemize}
            \item Calligraphic \lstinline|\mathcal{§symbols§}|: \(\mathcal{A}, \mathcal{B}, \mathcal{C}\)
            \item Blackboard bold \lstinline|\mathbb{§symbols§}|\alert{\ddag}: \(\mathbb{A}, \mathbb{B}, \mathbb{C}\)
            \item Fraktur \lstinline|\mathfrak{§symbols§}|\alert{\ddag}: \(\mathfrak{A}, \mathfrak{B}, \mathfrak{C}, \mathfrak{a}, \mathfrak{b}, \mathfrak{c}\)
        \end{itemize}
    \end{block}

    \begin{alertblock}{Warning}<+->
        \lstinline|\mathcal| and \lstinline|\mathbb| are only defined for capital letters; \lstinline|\mathfrak| only for capital and small letters.
        Using other characters for \a{symbols} gives strange results!
        Use other packages if needed.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Bold math}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§option§]{bm}|
        \begin{itemize}[<+->]
            \item Better bold math; tries really hard to keep correct spacing
            \item \a{option} \lstinline|nopmb| disables \enquote{poor man's bold} (see below)
            \item Should be loaded \alert{after} packages that change the math font
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\bm{§math§}|
        \begin{itemize}[<+->]
            \item Typesets \a{math} in bold while keeping italics
            \item Expects that commands in \a{math} are also included with arguments;
                exceptions are accents, e.\,g.\ \lstinline|\bm{\hat}{a}|
            \item How does it work?
                \begin{enumerate}
                    \item Compares normal version with \LaTeX's \lstinline|\boldmath| version
                    \item If \LaTeX{} would provide a bold symbol, then \lstinline|bm| uses that
                    \item Otherwise \lstinline|bm| uses \lstinline|amsmath|'s \enquote{poor man's bold}: typeset the symbol thrice with slight offsets (e.\,g.\ \(\bm{\sum}\))
                \end{enumerate}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Bold math}
    \begin{code}[lefthand width=.5\textwidth]<+->
\(\bm{\sum_{k=1}^n (3k+x)}\)

\(\mathbf{\sum_{k=1}^n (3k+x)}\)
    \end{code}

    \begin{alertblock}{Tip}<+->
        Sometimes, \lstinline|\bm{§math§}| doesn't work as expected when using \lstinline|\math...| commands.
        Try protecting \a{math} with extra braces.
    \end{alertblock}

    \begin{code}<.->
\(\mathrm{ \bm{g}   },
  \mathrm{ \bm{{g}} }\)
    \end{code}

    \begin{alertblock}{Warning}<+->
        Don't use \lstinline|amsmath|'s \lstinline|\boldsymbol| and \lstinline|\pmb|.
        \LaTeX's \lstinline|\boldmath| becomes unnecessary.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Bold math}
    \begin{codeexp}<+->
        \lstinline|\bmdefine{\§cmd§}{§symb§}|
        \begin{itemize}[<+->]
            \item Defines a new command \lstinline|\§cmd§| which typesets \a{symb} as if it were \lstinline|\bm{§symb§}|
            \item \a{symb} can be an accent
            \item Speeds up compilation time by creating a \enquote{new glyph} \lstinline|\§cmd§|
        \end{itemize}

        \begin{code}<+->
\bmdefine{\balpha}{\alpha}
\bmdefine{\bhat}{\hat}
\(\balpha, \bhat{a}\)
        \end{code}
    \end{codeexp}
\end{frame}

\section{Common errors}

\begin{frame}[fragile]{Remarks on warnings and errors}
    \begin{itemize}[<+->]
        \item \LaTeX{} differentiates between \alert{warnings} (still compiles) and \alert{errors} (aborts)
        \item Errors always contain:
            \begin{itemize}[<1->]
                \item First line: reason for the error
                \item Next two lines: line number in source and exact character where \LaTeX{} found the error
            \end{itemize}
        \item \LaTeX{} errors are incredibly hard to debug:
            \begin{itemize}
                \item Multiple reasons for the same error message
                \item Error message may happen much later than the actual problem
                \item Sometimes, a~low-level \TeX{} error is raised by some higher-level \LaTeX{} error
                \item Often times, multiple errors appear for the same problem
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Some common warnings}
    \begin{codeexp}<+->
        \verb|Reference '<key>' on page <page> undefined|\\
        \verb|Citation '<key>' on page <page> undefined|
        \begin{itemize}
            \item Maybe \a{key} has a typo
            \item Maybe provide a \verb|.bib| entry or a \lstinline|\label{§key§}|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Label '<key>' multiply defined|
        \begin{itemize}
            \item To many \lstinline|\label|'s with the same \a{key} in the document
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Some common warnings}
    \begin{codeexp}<+->
        \verb|Underfull \hbox (badness <num>)|\\
        \verb|Overfull \hbox (badness <num>)|
        \begin{itemize}[<+->]
            \item Line has either too few or too much content
            \item Maybe to long inline math expression on this line or the next\\
                \textrightarrow~put this in display style or rewrite text
            \item Maybe to long non-hyphenated word on this line or the next
                \textrightarrow~provide explicit hyphenation points as in \lstinline|hy\-phe\-na\-tion| or \lstinline|\hyphenation{hy-phen-ation}| or rewrite text
            \item Maybe load \lstinline|microtype|
            \item \lstinline|Underful \hbox| can also be caused by \lstinline|\\| after paragraph text
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Some common errors}
    \begin{codeexp}<+->
        \verb|There's no line here to end|
        \begin{itemize}
            \item \lstinline|\\| has not been used after text in text mode, e.\,g.\ after an empty line or with \lstinline|\\\\|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Something's wrong---perhaps a missing \item|
        \begin{itemize}
            \item There is text without \lstinline|\item| in a list
            \item Sometimes a spurious \lstinline|\addvspace| in \emph{horizontal mode}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Undefined control sequence|
        \begin{itemize}
            \item Maybe a typo
            \item Either define the command, or load the package which defines the command
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Brackets}
    \begin{exampleblock}{Exercise}<+->
        The code
        \begin{lstlisting}
            \begin{theorem}
                [t] a
            \end{theorem}
        \end{lstlisting}
        types \enquote{\textbf{Theorem~1} (t)\textbf{.} a}.
        How can we fix that?
    \end{exampleblock}

    \begin{actionenv}<+->
        \emph{Solution:} Environments accepting optional arguments gobble \lstinline|[t]|.
        Either put empty braces after \lstinline|\begin{theorem}|, or wrap \lstinline|{[t]}|.
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Some common errors}
    \begin{codeexp}<+->
        \verb|Runaway argument?|
        \begin{itemize}
            \item Most of the time, a closing~\lstinline|}| while using or defining a command is missing
            \item Can happen when using URLs inside \lstinline|\footnote|
            \item \LaTeX{} only shows the line \emph{following} the error, not the error itself
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Missing } inserted|\\
        \verb|Too many }'s|
        \begin{itemize}
            \item Maybe an unmatched \lstinline|{| or \lstinline|\begin{§env§}|
            \item Maybe an unmatched \lstinline|}| or \lstinline|\end{§env§}|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Extra alignment tab has been changed to \cr|
        \begin{itemize}
            \item Fix the column specification in \lstinline|tabular|, it hasn't enough columns
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Some common errors}
    \begin{codeexp}<+->
        \verb|Missing $ inserted|
        \begin{itemize}
            \item Wrongly placed math mode command in text mode
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|Missing \right|\\
        \verb|Extra \right|
        \begin{itemize}
            \item Either an unmatching \lstinline|\left| or \lstinline|\right|
            \item Use \lstinline|\DeclarePairedDelimiter| anyway
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Some more difficult warnings and errors}
    \begin{codeexp}<+->
        \verb|hyperref Warning: Token are not allowed in a PDF|\\
        \tabstrut\verb|string (Unicode): removing <something>|
        \begin{itemize}[<+->]
            \item In general, \LaTeX{} commands can't appear in the PDF table of contents (in the PDF viewer)
            \item Most of the time, this happens with math in sectioning commands
            \item Use \lstinline|\texorpdfstring{§tex§}{§unicode§}|
                \begin{itemize}
                    \item \a{tex} is normal \LaTeX{} command
                    \item Google a \a{unicode} replacement for \a{tex} and copy \&~paste it
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|pgf Error: Single ampersand used with wrong catcode|
        \begin{itemize}
            \item Maybe an~\lstinline|&| inside a \lstinline|tikzcd| environment inside a \lstinline|\footnote|\\
                \textrightarrow~Use the option \lstinline|ampersand replacement|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Some more difficult errors}
    \begin{codeexp}<+->
        \verb|File ended while scanning use of \@writefile|
        \begin{itemize}[<+->]
            \item The previous compilation finished prematurely
            \item Recompile the document
            \item \alert{Note:} latexmk only recompiles a document if it has changed\\
                \textrightarrow~add some gibberish to trick latexmk into recompiling
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \verb|TeX capacity exceeded, sorry [<explanation>]|
        \begin{itemize}
            \item Some evil recursion is going on
            \item Good luck fixing that!
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Practical tips for debugging}
    \begin{itemize}
        \item Sometimes, deleting all files created during the previous compilation and recompiling can help
        \item For locating the error in the source code, comment out the whole body and\slash or preamble with \lstinline|\iffalse \fi| and then binary search through the code
    \end{itemize}
\end{frame}

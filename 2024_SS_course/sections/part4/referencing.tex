\section{Referencing in \texorpdfstring{\LaTeX}{LaTeX}}

\begin{frame}[fragile]{Counters}
    \begin{itemize}[<+->]
        \item Recall from the section on \lstinline|amsthm|: \LaTeX{} uses \alert{counters} for automatic numbering
        \item Predefined counters (selection)
            \begin{itemize}[<1->]
                \item \lstinline|part|, \lstinline|chapter|, \dots, \lstinline|subsection|
                \item \lstinline|enumi|, \lstinline|enumii|, \lstinline|enumiii|, \lstinline|enumiv| (for \lstinline|enumerate|)
                \item \lstinline|figure|, \lstinline|table|, \lstinline|footnote|
                \item \lstinline|equation| (for all math environments)
                \item \lstinline|page|, \lstinline|totalpages|
            \end{itemize}
        \item Possible to define new counters, e.\,g.\ \lstinline|\newtheorem|
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Cross-references}
        \begin{itemize}[<+->]
            \item Used for e.\,g.\ \lstinline|\tableofcontents|, \lstinline|\listoffigures|, the bibliography, indices, etc.
            \item Statically writing \enquote{Theorem~2.1 on page~14} is bad
            \item Using automatic \alert{cross-references} is better: references adapt themselves if the numbers should change
        \end{itemize}

    \begin{block}{Current reference string}<+->
        \begin{itemize}[<+->]
            \item \LaTeX{} maintains a variable, the \alert{current reference string}
            \item (Almost) always a number from a counter
            \item Certain commands set this string; is only set in \emph{current scope}
                \begin{itemize}
                    \item I.\,e.\ during the currently closest surrounding environment\slash group
                    \item Is reset to the previous value after scope ends
                \end{itemize}
            \item Using the reference strings saves it to the \verb|.aux| file for later compilations
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Creating references}
    \begin{quote}<+->
        Without tricks, \alert{only counters} can be manually referenced.
    \end{quote}

    \begin{codeexp}<+->
        \lstinline|\label{§key§}|
        \begin{itemize}[<+->]
            \item Associates \a{key} to the current reference string, writes both to \verb|.aux| file
            \item \a{key}: string of characters, preferably only digits, ASCII letters and common punctuation
            \item \a{key} should be unique
            \item By convention, \a{key} is e.\,g.\ \lstinline|fig:monalisa| for figures to avoid pollution of namespace\\
                Other prefixes: \lstinline|ch:|, \lstinline|sec:|, \lstinline|subsec:|, \lstinline|tab:|, \lstinline|eq:|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile, t]{Creating references}
    \begin{exampleblock}{Exercise}<+->
        Suppose that the following is placed in an empty document body.
        What does \lstinline|\label| refer to?
        \emph{Note:} This is not good code!

        \begin{enumerate}
            \item<+-> \leavevmode
                \begin{lstlisting}
                    \label{1}
                    \section{Section~1}
                    \label{2}
                    Text text.
                    \label{3}
                \end{lstlisting}

                \begin{actionenv}<+->
                    \emph{Solution:} \lstinline|1|~refers to nothing (empty string), \lstinline|2|~and~\lstinline|3| refer to \enquote{Section~1}.
                \end{actionenv}
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile, t]{Creating references}
    \begin{exampleblock}{Exercise}<+->
        Suppose that the following is placed in an empty document body.
        What does \lstinline|\label| refer to?
        \emph{Note:} This is not good code!

        \begin{enumerate}
            \stepcounter{enumi}
            \item \leavevmode
                \begin{lstlisting}
                    \begin{figure}
                        \begin{center}
                            \caption{Caption~1}
                            \label{1}
                        \end{center}
                        \label{2}
                    \end{figure}
                \end{lstlisting}

                \begin{actionenv}<+->
                    \emph{Solution:} \lstinline|1|~refers to \enquote{Caption~1}, \lstinline|2|~refers to nothing (empty string).
                \end{actionenv}
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile, t]{Creating references}
    \begin{exampleblock}{Exercise}<+->
        Suppose that the following is placed in an empty document body.
        What does \lstinline|\label| refer to?
        \emph{Note:} This is not good code!

        \begin{enumerate}
            \stepcounter{enumi}
            \stepcounter{enumi}
            \item \leavevmode
                \begin{lstlisting}
                    \section{Section~1}
                    \begin{theorem} % Theorem 1.1
                        \label{1}
                        \begin{enumerate}
                            \item Item~1
                                \label{2}
                        \end{enumerate}
                    \end{theorem}
                    \label{3}
                \end{lstlisting}

                \begin{actionenv}<+->
                    \emph{Solution:} \lstinline|1|~refers to \enquote{Theorem~1.1}, \lstinline|2|~refers to \enquote{Item~1}, \lstinline|3|~refers to \enquote{Section~1}.
                \end{actionenv}
        \end{enumerate}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Using references}
    \begin{codeexp}<+->
        \lstinline|\ref{§key§}|
        \begin{itemize}[<+->]
            \item Returns the reference string associated to \a{key} in running text
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\pageref{§key§}|
        \begin{itemize}[<+->]
            \item Returns the page of the object which set the reference string of \a{key}
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Note}<+->
        \textbf{??} in your document means that \LaTeX{} doesn't know what to replace \lstinline|\ref| or \lstinline|\pageref| with.
        Recompile (or fix the undefined \a{key} if the problem prevails)!
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Using reference strings}
    \begin{alertblock}{Tips}
        \begin{itemize}[<+->]
            \item Issue \lstinline|\label| \alert{directly after} the counter to be referenced
            \item Use a tilde~\lstinline|~| to set a \emph{non-breaking space}, i.\,e.\ line break at~\lstinline|~| is prohibited
        \end{itemize}
    \end{alertblock}

    \begin{pdflist}[listing options={firstline=6, lastline=15}, comment style={scale=2}]<+->
\documentclass{article}
\usepackage[papersize={4cm,3cm}, scale=.9]{geometry}
\usepackage{amsthm}
\newtheorem{theorem}{Theorem}[section]
\begin{document}
\begin{theorem}\leavevmode
    \label{thm}
    \begin{enumerate}
        \item \label{item}
            Text.
    \end{enumerate}
\end{theorem}
Item~\ref{item}.
Theorem~\ref{thm} on
page~\pageref{thm}.
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Using reference strings}
    \begin{pdflist}[listing options={firstline=5, lastline=13}, comment style={scale=2}]
\documentclass{article}
\usepackage[papersize={4cm,3cm}, scale=.9]{geometry}
\usepackage{mathtools}
\begin{document}
\begin{subequations}
    \label{eq}
    \begin{gather}
        a \label{seq1} \\
        b \label{seq2}
    \end{gather}
\end{subequations}

Equation~\ref{eq}, subequations \ref{seq1}~and~\ref{seq2}.
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Hyperlinks in PDFs}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{hyperref}|\\
        \lstinline|\hypersetup{§options§}|
        \begin{itemize}[<+->]
            \item Creates internal and external hyperlinks in documents; interacts with the PDF specification
            \item Upon loading:
                \begin{itemize}[<1->]
                    \item Makes all cross-references clickable with coloured boxes (each \lstinline|\ref|, \lstinline|\cite| and each item in the \lstinline|\tableofcontents|)
                    \item Creates a table of contents in the PDF viewer
                \end{itemize}
            \item \a{options}: comma-separated key-value list
        \end{itemize}
    \end{codeexp}

    \begin{center}
        \begin{actionenv}<.->
            \includegraphics[scale=.3]{hyperref}
        \end{actionenv}
    \end{center}
    \footnotetext{Picture: own work.
    Program is \emph{GNOME Evince}.}
\end{frame}

\begin{frame}[fragile]{Hyperlinks in PDFs}
    \begin{alertblock}{Warning}<+->
        Although one of the most important packages in \LaTeX:
        \begin{itemize}[<+->]
            \item \lstinline|hyperref| is very frail and is sometimes incompatible with other packages
            \item Rule of thumb: \lstinline|hyperref| should be pretty much the \alert{last} loaded package
            \item Read the docs of \lstinline|hyperref| and other packages
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{\a{options} for \texttt{hyperref}}
    More than 100 options.
    Here a selection.

    \begin{block}{Document properties}<+->
        \a{\dots} should be Unicode; braces can be dropped if \a{\dots} contains no commas.
        \begin{itemize}[<+->]
            \item \lstinline|pdfauthor={§\dots§}|: author
            \item<.-> \lstinline|pdfkeywords={§\dots§}|: comma-separated list of keywords
            \item<.-> \lstinline|pdfsubject={§\dots§}|: subject
            \item<.-> \lstinline|pdftitle={§\dots§}|: title
            \item \lstinline|bookmarks| (default \lstinline|true|): create PDF bookmarks
            \item<.-> \lstinline|bookmarksnumbered| (default \lstinline|false|): put section numbers before PDF bookmarks
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{\a{options} for \texttt{hyperref}}
    \begin{block}{Hyperlink colours}<+->
        \begin{itemize}[<+->]
            \item By default, links are marked by coloured boxes in the PDF viewer; these don't appear in print
            \item \lstinline|colorlinks|: remove coloured boxes and colour text instead\\
                \textrightarrow~this \alert{appears} in print!
            \item \lstinline|hidelinks|: don't highlight hyperlinks
            \item \lstinline|linkcolor=§colour§| (default \lstinline|red|): for internal links in PDF
            \item \lstinline|citecolor=§colour§| (default \lstinline|green|): for citations
            \item \lstinline|urlcolor=§colour§| (default \lstinline|magenta|): for URLs
            \item \lstinline|filecolor=§colour§| (default \lstinline|cyan|): for opening local files
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{\a{options} for \texttt{hyperref}}
    \begin{exampleblock}{Example settings}<+->
        When loading \lstinline|xcolor| (e.\,g.\ already done when loading \TikZ):
        \begin{lstlisting}
            \hypersetup{
                bookmarksnumbered,
                pdfauthor=Me,
                pdftitle=Awesome title,
                colorlinks, % choose dark colours for print
                linkcolor=red!70!black, % red 70%, black 30%
                urlcolor=blue!80!black,
                citecolor=green!50!black
            }
        \end{lstlisting}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\texttt{hyperref} commands}
    \begin{codeexp}<+->
        \lstinline|\ref*{§key§}|\\
        \lstinline|\pageref*{§key§}|
        \begin{itemize}
            \item Versions of \lstinline|\ref|, \lstinline|\pageref| that don't create hyperlinks
        \end{itemize}
    \end{codeexp}


    \begin{codeexp}<+->
        \lstinline|\href{§URL§}{§text§}|
        \begin{itemize}[<+->]
            \item Turn \a{text}in to a hyperlink that points to \a{URL}
            \item \a{text}: any \LaTeX{} is legal
            \item \a{URL}: URL, e.\,g.\
                \begin{itemize}[<1->]
                    \item \verb|https://gitlab.com/Gargantuar314/latex|\\
                        Opens URL in default browser
                    \item \verb|mailto:tien.nguyen@uni-bonn.de|\\
                        Opens default email program with correct recipient
                    \item \verb|run:../kirby.png|\\
                        Opens the file relative to current directory
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\texttt{hyperref} commands}
    \begin{codeexp}<+->
        \lstinline|\url{§URL§}|\\
        \lstinline|\nolinkurl{§URL§}|
        \begin{itemize}
            \item \a{URL}: same as for \lstinline|\href|
            \item Typesets \a{URL} directly and turns it into a hyperlink
            \item \lstinline|\nolinkurl| doesn't create a hyperlink
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Note}<+->
        If \a{URL} (both for \lstinline|\href|, \lstinline|\url|) contains non-ASCII characters (especially web URLs), they must be converted in percent-encoded form, e.\,g.\
        \begin{lstlisting}[language=bash, identifierstyle=\color{black}]
            https://de.wikipedia.org/wiki/K%C3%B6ln
        \end{lstlisting}
        This is done automatically upon copy \&~paste from the browser.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\texttt{hyperref} caveats}
    \begin{exampleblock}{Note}<+->
        \lstinline|hyperref| loads the \lstinline|url| package, which forbids line breaks at explicit hyphens in \a{URL}.
        If needed, put
        \begin{lstlisting}
            \PassOptionsToPackage{hyphens}{url}
        \end{lstlisting}
        \alert{before} loading \lstinline|hyperref| to avoid this.
    \end{exampleblock}

    \begin{alertblock}{Warning}<+->
        Avoid \lstinline|\href|, \lstinline|\url| inside \alert{any} commands, especially \lstinline|\section|, etc.
        For \lstinline|\footnote|, escape at least every special character, i.\,e.\ replace \verb|# % &| by \verb|\# \% \&|.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{\enquote{Clever} referencing}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{cleveref}|
        \begin{itemize}[<+->]
            \item Automates some of the typesetting involved with cross-references
            \item<.-> Main feature: typesets the \enquote{Theorem} of \enquote{Theorem~1.1} as well
            \item Support for \lstinline|babel| and \lstinline|amsthm|
            \item Has to be loaded \alert{after} \lstinline|hyperref|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{New \enquote{clever} cross-referencing command}
    \begin{codeexp}<+->
        \lstinline|\cref{§keys§}|, \lstinline|\cref*{§keys§}|\\
        \lstinline|\Cref{§keys§}|, \lstinline|\Cref*{§keys§}|
        \begin{itemize}[<+->]
            \item \a{keys}: comma-separated list of keys
                \begin{itemize}[<1->]
                    \item Must not contain spaces
                    \item Keys must not contain commas
                \end{itemize}
            \item Alternative to \lstinline|\ref|; original \lstinline|\ref| stays untouched
            \item Automatically adds the \enquote{type} of reference to each key, sorts and compresses them; predefined types:
                \begin{itemize}
                    \item \lstinline|equation|, \lstinline|figure|, \lstinline|table|, \lstinline|page|, \lstinline|part|, \lstinline|chapter|, \dots, \lstinline|subsubsection|, \lstinline|enumi|, \dots, \lstinline|enumiv| (giving \enquote{item})
                    \item \lstinline|theorem|, \lstinline|lemma|, \lstinline|corollary|, \lstinline|proposition|, \lstinline|definition|, \lstinline|result|, \lstinline|example|, \lstinline|remark|, \lstinline|note|
                    \item \lstinline|algorithm|, \lstinline|listing|
                \end{itemize}
            \item \lstinline|\Cref| capitalises the type; to be used at the beginning of sentences
            \item With \lstinline|hyperref|, starred versions don't create hyperlinks
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{New \enquote{clever} cross-referencing command}
    \begin{exampleblock}{Note}<+->
        \lstinline|\newtheorem| should be used \alert{after} loading \lstinline|cleveref|.
    \end{exampleblock}

    \begin{pdflist}[listing options={firstline=3}, lefthand width=.6\textwidth, comment style={scale=2}, sidebyside gap=0pt]<+->
\documentclass{article}
\usepackage[papersize={5cm,6cm}, scale=.9]{geometry}
\usepackage{amsthm}
\usepackage{cleveref}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\begin{document}
\begin{theorem}\label{t1}
\end{theorem}
\begin{lemma}\label{l1}\end{lemma}
\begin{lemma}\label{l2}\end{lemma}
\begin{lemma}\label{l3}\end{lemma}
\Cref{l1,l2}; \cref{l1,t1};
\cref{l1,t1,l2,l3}.
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{New referencing commands}
    \begin{codeexp}<+->
        \lstinline|\crefrange{§key1§}{§key2§}|, \lstinline|\crefrange*{§key1§}{§key2§}|\\
        \lstinline|\Crefrange{§key1§}{§key2§}|, \lstinline|\Crefrange*{§key1§}{§key2§}|
        \begin{itemize}[<+->]
            \item \a{key1}, \a{key2}: two keys of the same type
            \item Convenience command for ranges like \enquote{Eqs.~\a{key1} to~\a{key2}}
            \item Does not check whether all keys from \a{key1} to \a{key2} are indeed of the same type
            \item Starred and capitalised versions work like \lstinline|\cref|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\cpageref{§keys§}|, \lstinline|\Cpageref{§keys§}|\\
        \lstinline|\cpagerefrange{§key1§}{§key2§}|, \lstinline|\Cpagerefrange{§key1§}{§key2§}|
        \begin{itemize}[<+->]
            \item Alternative to \lstinline|\pageref|; original \lstinline|\pageref| stays untouched
            \item Add the \enquote{page} key, e.\,g.\ \enquote{pages 1,~2, and 24}
            \item Work like \lstinline|\cref|, \lstinline|\crefrange|, etc.\
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{\texttt{cleveref} \a{options}}
    \begin{itemize}[<+->]
        \item Some \lstinline|babel| languages, e.\,g.\ \lstinline|english| (default), \lstinline|ngerman|, \lstinline|french|
        \item \lstinline|sort|: only sort cross-references in \a{keys} of \lstinline|\cref{§keys§}|
        \item \lstinline|compress|: only compress cross-references in \a{keys} into reference ranges
        \item \lstinline|sort&compress| (default)
        \item<.-> \lstinline|nosort|: neither \lstinline|sort|, nor \lstinline|compress|
        \item \lstinline|capitalize| or \lstinline|capitalise|: always capitalise type names\\
            \textrightarrow~should still use \lstinline|\cref| and \lstinline|\Cref|
        \item \lstinline|namelink|: with \lstinline|hyperref|, include type name into hyperlink
        \item \lstinline|noabbrev|: avoid abbreviations like \enquote{eq.}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Using parts of cross-references}
    \begin{codeexp}<+->
        \lstinline|\namecref{§key§}|, \lstinline|\nameCref{§key§}|, \lstinline|\lcnamecref{§key§}|\\
        \lstinline|\namecrefs{§key§}|, \lstinline|\nameCrefs{§key§}|, \lstinline|\lcnamecrefs{§key§}|
        \begin{itemize}[<+->]
            \item \a{key}: a~\alert{single} key
            \item Produce the type name of the label associated to \a{key}
            \item \lstinline|C|~is for capitalised versions for the beginning of sentences
            \item \lstinline|lc|~is for lowercase versions, regardless of option \lstinline|capitalize|
            \item \lstinline|s|~at the end is for plural versions
            \item E.\,g.\ \enquote{Something will be discussed in section 2~and~3.
                These \emph{sections} \dots}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\labelcref{§keys§}|\\
        \lstinline|\labelcpageref{§keys§}|
        \begin{itemize}[<+->]
            \item \a{keys}: list of keys
            \item Remove all type names from \lstinline|\cref| or \lstinline|\cpageref|
            \item Sometimes necessary for languages which decline nouns like German
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Customising cross-references}
    \begin{codeexp}<+->
        \lstinline|\crefname{§type§}{§singular§}{§plural§}|\\
        \lstinline|\Crefname{§type§}{§singular§}{§plural§}|
        \begin{itemize}[<+->]
            \item \a{type}: usually counter name or name of theorem-like environment
            \item (Re)define \a{singular} and \a{plural} names for \a{type}
            \item \lstinline|\Crefname| is used with \lstinline|\Cref|, etc.
            \item Necessary for counters with no predefined type
            \item If \lstinline|\Crefname| not given, it is inferred from \lstinline|\crefname|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\crefalias{§counter§}{§type§}|
        \begin{itemize}
            \item When referencing \a{counter}, use the same format as that of \a{type}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\label[§type§]{§key§}|
        \begin{itemize}
            \item Overrides the type of \a{key} with \a{type}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Customising cross-references}
    \begin{pdflist}[listing options={firstline=5}, lefthand width=.6\textwidth, comment style={scale=1.8}, sidebyside gap=0pt]<+->
\documentclass{article}
\usepackage[papersize={5.5cm,6cm}, scale=.9]{geometry}
\usepackage{amsthm}
\usepackage{cleveref}
\newtheorem{theorem}{Theorem}
\newtheorem{theorem*}[theorem]
    {Theorem*}
\newtheorem{krull}[theorem]
    {Krull's theorem}
\crefalias{theorem*}{theorem}
\crefname{krull}{Krull's theorem}{}
\begin{document}
\begin{theorem*}\label{t1}
\end{theorem*}
\begin{theorem}[Krull's theorem]
    \label[krull]{t2}
\end{theorem}
\begin{krull}\label{krull}
\end{krull}
\Cref{t1,t2}; \cref{krull}.
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Customising cross-references}
    \begin{codeexp}<+->
        \lstinline|\creflabelformat{§type§}{§format§}|
        \begin{itemize}[<+->]
            \item Customise the reference numbers of \a{type} when using \lstinline|\cref| etc.\
            \item \a{format} must contain \lstinline|#1|, \lstinline|#2|, \lstinline|#3|
                \begin{itemize}
                    \item \lstinline|#1| is the reference number
                    \item \lstinline|#2 #3| mark beginning\slash end of the hyperlink from \lstinline|hyperref|
                    \item Order should be \lstinline|#2 #1 #3|
                    \item E.\,g.\ \lstinline|#2(#1)#3| makes \enquote{(\a{counter})} into hyperlink for \a{type}
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

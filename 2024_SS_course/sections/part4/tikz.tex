\section{Drawing diagrams with \texorpdfstring{\TikZ}{TikZ}}

\begin{frame}[fragile]{Length units in \LaTeX}
    \begin{block}{Font independent units}<+->
        \begin{tabular}{@{}lll@{}} \toprule
            Unit & Name & Note \\ \midrule
            \lstinline|pt| & point & 1/72.72 in \\
            \lstinline|mm| & millimetre & \\
            \lstinline|cm| & centimetre & \\ \bottomrule
        \end{tabular}
    \end{block}
    
    \begin{block}{Font dependent units}<+->
        \begin{tabular}{@{}lll@{}} \toprule
            Unit & Name & Note \\ \midrule
            \lstinline|ex| & ex & height of a small \enquote{x} \\
            \lstinline|em| & em & width of a capital \enquote{M} \\
            \lstinline|mu| & math unit & 1/18 em \\ \bottomrule
        \end{tabular}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Loading \TikZ}
    \begin{codeexp}<+->
        \lstinline|\usepackage{tikz}|
        \begin{itemize}
            \item Very powerful tool for drawing all kinds of pictures
            \item Has its own syntax; based on the lower-level language PGF
        \end{itemize}
    \end{codeexp}

    \begin{block}{Documentation}<+->
        \begin{itemize}
            \item \TikZ{} docs has over 1300 pages (\href{https://ctan.org/pkg/pgf}{CTAN})
            \item Online version at \href{https://tikz.dev/}{tikz.dev}
            \item \alert{We only present a small fraction}
        \end{itemize}
    \end{block}

    \begin{codeexp}<+->
        \lstinline|\usetikzlibrary{§libraries§}|
        \begin{itemize}
            \item \a{libraries}: comma-separated list of \TikZ{} libraries
            \item Over 40 libraries, see part~V in the \TikZ{} docs
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\tikzset{§\dots§}|
        \begin{itemize}
            \item Defines keys and styles globally
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{The main environment}
    \begin{codeexp}<+->
        \lstinline|\begin{tikzpicture}[§key-val§]|\\
        \tabstrut\a{\dots}\\
        \lstinline|\end{tikzpicture}|
        \begin{itemize}[<+->]
            \item Creates a \TikZ{} picture
            \item \a{key-val}: comma-separated key-value list\\
                Options for the whole \lstinline|tikzpicture|
            \item \a{\dots}: \TikZ{} instructions\\
                Each instruction almost always starts with a command and ends in a \alert{semicolon}~\lstinline|;|
            \item Should be usually placed in a \lstinline|figure| float
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\tikz[§key-value§]{§\dots§}|
        \begin{itemize}[<+->]
            \item Command version of \lstinline|tikzpicture|
            \item Can be used for small in-text pictures
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{The main environment}
    \begin{block}{\a{key-val} (useful selection)}<+->
        \begin{itemize}[<+->]
            \item \lstinline|scale=§frac§| (default~\lstinline|1|): scales the whole picture
            \item \lstinline|baseline=§coord§| (default lowest point): vertically puts \a{coord} on baseline of surrounding text
        \end{itemize}
    \end{block}

    \begin{code}[lefthand width=.6\textwidth]<+->
text \tikz
    {\draw (0,0) -- (1,-1);}

text \tikz[baseline={(0,0)}]
    {\draw (0,0) -- (1,-1);}

text \tikz[baseline=(current bounding box.center)]
    {\draw (0,0) -- (1,-1);}
    \end{code}
\end{frame}

\begin{frame}[fragile]{PGF mathematical expressions}
    \begin{block}{Typical math expressions}
        \begin{itemize}[<+->]
            \item \lstinline|x+y|, \lstinline|x-y|, \lstinline|x*y|, \lstinline|x/y|, \lstinline|sqrt(x)|, \lstinline|pi|
            \item \lstinline|div(x,y)|, \lstinline|mod(x,y)|: floor division, modulo (with sign)
            \item \lstinline|pow(x,y)|: power
                \begin{itemize}
                    \item For greatest accuracy, \lstinline|y|~should be an integer
                    \item Otherwise, calculates \(\mathrm{e}^{y \ln x}\)
                \end{itemize}
            \item \lstinline|exp(x)|, \lstinline|ln(x)|, \lstinline|log10(x)|
            \item \lstinline|deg(x)|, \lstinline|rad(x)|: convert to degrees\slash radians
            \item \lstinline|sin(x)|, \lstinline|cos(x)|, \lstinline|tan(x)|: \lstinline|x|~in degrees\\
                If e.\,g.\ \lstinline|sin(x r)|, \lstinline|x|~is in radians
        \end{itemize}
    \end{block}

    \begin{alertblock}{Warning}<+->
        If the expression contains parentheses or commas, it probably needs to be protected with braces, e.\,g.\ \lstinline|(2, {2*(3+1)} )| for coordinates.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Specifying coordinates}
    \begin{exampleblock}{Note}<+->
        \TikZ{} measures lengths in cm by default.
    \end{exampleblock}

    \begin{codeexp}<+->
        \lstinline|(§x§,§y§)|\\
        \lstinline|(§x§,§y§,§z§)|\\
        \begin{itemize}[<+->]
            \item<.-> Cartesian coordinates
            \item \a{x}, \a{y}, \a{z}: either a number, or a length like \lstinline|-1mm|, \lstinline|.363pt|
            \item Coordinates can also be math expressions, e.\,g.\ \lstinline|(2+3,2)|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|(§deg§:§rad§)|
        \begin{itemize}[<+->]
            \item<.-> Polar coordinates
            \item \a{deg}: a~number, measured in degrees
            \item<.-> \a{rad}: a~positive number or a positive length
            \item Coordinates can also be math expressions, e.\,g.\ \lstinline|(360/5:2)|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Path specification}
    \begin{codeexp}<+->
        \lstinline|\path[§key-value§] §\dots§;|
        \begin{itemize}[<+->]
            \item \alert{Path} is the fundamental building block in \TikZ
            \item \a{key-value}: comma-separated key-value list with options for the path
            \item \a{\dots}: consists of coordinates and operations how to connect coordinates
            \item Abbreviations exist \textrightarrow~\alert{almost never used}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{codeexp}<+->
        \lstinline|\draw[§key-val§] §\dots§;|
        \begin{itemize}
            \item Draws something
        \end{itemize}
    \end{codeexp}

    \begin{block}{Operation \enquote{move-to}}<+->
        \begin{itemize}
            \item Moves the current drawing point to the next
            \item No special command
        \end{itemize}
    \end{block}

    \begin{block}{Operation \enquote{line-to}}<+->
        \lstinline|--|, \lstinline+-|+, \lstinline+|-+
        \begin{itemize}[<+->]
            \item \lstinline|--|: straight line
            \item \lstinline+|-+: draw a vertical line, then a horizontal line
            \item \lstinline+-|+: draw a horizontal line, then a vertical line
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{code}[righthand width=.6\textwidth, sidebyside gap=0pt]
\begin{tikzpicture}
\draw (0,0) -- (1,1)
    (1,0) -- (2,1);
\draw (2,0) -| (3,2)
    -- (4,1) |- (5,0);
\end{tikzpicture}
    \end{code}

    \begin{exampleblock}{Note}<+->
        (Sometimes), it makes a difference whether two lines are joined.
        \begin{code}
\begin{tikzpicture}
    [line width=10pt]
\draw (0,0) -- (1,1)
(1,1) -- (2,0);
\draw (0,-1) -- (1,0) -- (2,-1);
\end{tikzpicture}
        \end{code}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{block}{Operation \enquote{curve-to}}<+->
        \lstinline|.. controls (§coord1§) ..|
        \lstinline|.. controls (§coord1§) and (§coord2§) ..|
        \begin{itemize}[<+->]
            \item Coordinates \a{coord1}, \a{coord2} influence the path in a \enquote{smooth} way
        \end{itemize}
    \end{block}

    \begin{alertblock}{Warning}<+->
        \a{coord1}, \a{coord2} \alert{don't} lie on the curve.
        See \href{https://en.wikipedia.org/wiki/B%C3%A9zier_curve}{Wikipedia}.
    \end{alertblock}

    \begin{code}<+->
\begin{tikzpicture}
\draw (0,0) .. controls (30:1) and (-30:2) .. (2,0);
\node at (30:1) {A};
\node at (-30:2) {B};
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{block}{Operation \enquote{rectangle}}
        \lstinline|rectangle|
        \begin{itemize}
            \item Rectangle from current point to next point
        \end{itemize}
    \end{block}

    \begin{code}<+->
\begin{tikzpicture}
\draw (0,0) rectangle (-2,-1)
    -- (0,0);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{block}{Operation \enquote{circle}}<+->
        \lstinline|circle[§options§]|
        \begin{itemize}[<+->]
            \item<.-> Circle or ellipse at current point
            \item \a{options} (actually necessary):
                \begin{itemize}
                    \item \lstinline|x radius=§length§|: radius in x-direction
                    \item \lstinline|y radius=§length§|: radius in y-direction
                    \item \lstinline|radius=§length§|: radius in x- and y-direction
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{code}<+->
\begin{tikzpicture}
\draw (0,0) circle[
    radius=1] -- (1,0);
\draw (2,0) circle[
    x radius=.2,
    y radius=1.3];
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{block}{Operation \enquote{arc}}<+->
        \lstinline|arc[§options§]|
        \begin{itemize}
            \item<.-> Arc as part of a circle or an ellipse; \alert{starts} at the current point
            \item \a{options} (actually necessary):
                \begin{itemize}
                    \item \lstinline|x radius|, \lstinline|y radius|, \lstinline|radius|: same as for \lstinline|circle|
                    \item \lstinline|start angle=§angle§|: start angle in degrees relative to horizontal line
                    \item \lstinline|end angle=§angle§|: like \lstinline|start angle|
                    \item \lstinline|delta angle=§angle§|: replaces \lstinline|end angle|, specifies difference of \lstinline|start angle| and \lstinline|end angle|
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{code}<+->
\begin{tikzpicture}
\draw (0,0) arc[
    start angle=150,
    end angle=-90,
    radius=.5] -- (1,0);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Drawing}
    \begin{block}{Operation \enquote{grid}}<+->
        \lstinline|grid[§options§]|
        \begin{itemize}[<+->]
            \item<.-> Grid between two coordinates
            \item<.-> Grid lines are placed at integer coordinates by default
            \item \a{options}:
                \begin{itemize}
                    \item \lstinline|xstep=§length§|: change coordinates for x-direction grid line placement starting at \lstinline|(0,0)|
                    \item \lstinline|ystep=§length§|: same as \lstinline|xstep|
                    \item \lstinline|step=§length§|: sets both \lstinline|xstep|, \lstinline|ystep|
                    \item \lstinline|help lines|: makes grid lines grey
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{tikzpicture}
\draw (0,0) grid[help lines,
    step=.5] (2,2);
\draw (0,0) grid (1.5,1.5);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Special coordinates}
    \begin{codeexp}<+->
        \lstinline|cycle|
        \begin{itemize}
            \item Is replaced with the first coordinate in a path
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzpicture}
\draw (0,0) -- (1,1)
    -- (2,0) -- cycle;
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Special coordinates}
    \begin{codeexp}<+->
        \lstinline|+(§coord§)|\\
        \lstinline|++(§coord§)|
        \begin{itemize}[<+->]
            \item \lstinline|+|: \a{coord} relative to latest current coordinate, doesn't update current point
            \item \lstinline|++|: \a{coord} relative to latest current coordinate, updates current point to \a{coord}
            \item Can be useful for shifting polar coordinates
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{tikzpicture}
\draw (0,0) -- (1,1) -- +(0,-.5)
    -- +(.5,0) -- +(0,.5);
\end{tikzpicture}

\begin{tikzpicture}
\draw (0,0) -- (1,1) -- ++(0,-.5)
    -- +(.5,0) -- +(0,.5);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Drawing options (\a{key-val})}
    \begin{codeexp}<+->
        \lstinline|->|, \lstinline|<-|, \lstinline|-<|, \lstinline|->>|, etc.
        \begin{itemize}
            \item Arrow tips
            \item For more arrow tips, see the \TikZ{} library \lstinline|arrows.meta|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|draw=§colour§|
        \begin{itemize}
            \item \a{colour} for path; all colours of \lstinline|xcolor| are supported
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|fill=§colour§|
        \begin{itemize}
            \item Fill area with \a{colour}, then draw the lines
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|line width=§width§|\\
        \lstinline|ultra thin|, \lstinline|very thin|, \lstinline|thin|, \lstinline|semithick|, \lstinline|ultra thick|, \lstinline|very thick|, \lstinline|thick|
        \begin{itemize}
            \item Thickness of lines
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|dotted|, \lstinline|densely dotted|, \lstinline|loosely dotted|\\
        \lstinline|dashed|, \dots, \lstinline|dash dot|, \dots, \lstinline|dash dot dot|, \dots
        \begin{itemize}
            \item Line style
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Drawing options (\a{key-val})}
    \begin{code}[lefthand width=.6\textwidth, sidebyside gap=0pt]
\begin{tikzpicture}
\draw[->, purple, very thick]
    (0,0) -- (2,0);
\draw[->, red!30!cyan,
      thick] (0,0) -- (0,2);
\draw[olive, densely dash dot dot]
    (0,0) circle[radius=1];
\draw[fill=gray, draw=red] (0,0)
    arc[start angle=180,
    end angle=0, radius=.5];
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Nodes}
    \begin{codeexp}<+->
        \lstinline|\node[§key-val§] at (§coord§) {§label§};|
        \lstinline|\node[§key-val§] (§name§) at (§coord§) {§label§};|
        \begin{itemize}[<+->]
            \item Defines node \a{name} at \a{coord}, \a{label} is placed at \a{coord}
            \item \a{name}: any string of letters or digits
            \item \a{label}: anything in normal text mode, can contain \lstinline|\\|
        \end{itemize}
    \end{codeexp}

    \begin{code}<+->
\begin{tikzpicture}
\node (A) at (0,0) {\textbf{A}};
\node (B) at (1,0) {\(\pi\)};
\node (C) at (1,1) {};
\draw (A) -- (B) -- (C);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Node options (\a{key-val})}
    \begin{itemize}[<+->]
        \item \lstinline|shape=§shape§|: border of node; one of \lstinline|rectangle| (default), \lstinline|circle|
        \item \lstinline|draw={§options§}|: whether to draw the border with \a{options}
        \item \lstinline|inner sep=§length§| (default \lstinline|.3333em|): separation between \a{label} and boundary
        \item \lstinline|outer sep=§length§| (default 1/2 line width of border): separation between border and \lstinline|\draw| lines
        \item \lstinline|above|, \lstinline|below|, \lstinline|left|, \lstinline|right|, \lstinline|above right|, \dots: \a{label} placement relative to \a{coord}
    \end{itemize}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{tikzpicture}
\node[draw=pink, inner sep=0pt]
    (0) at (0,0) {A};
\node[draw, circle, outer sep=2mm]
    (1) at (1,1) {B};
\draw (0) -- (1);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Placing nodes on a line or curve}
    \begin{block}{Operation \enquote{node}}<+->
        \lstinline|node[§key-val§] {§label§}|
        \begin{itemize}[<+->]
            \item Places node on a \lstinline|\draw| \enquote{line to} or \enquote{curve to} operation
            \item Extra option for \a{key-val}:
                \begin{itemize}
                    \item \lstinline|pos=§frac§|: position on line, \a{frac} between \lstinline|0|~and~\lstinline|1|
                    \item \lstinline|at start|, \lstinline|very near start|, \lstinline|near start|, \lstinline|midway| (default), \lstinline|near start|, \lstinline|very near start|, \lstinline|at start|
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{tikzpicture}
\draw (0,0) --
    node[near start] {A}
    node[pos=.6] {B}
    (1,1) node[right] {C};
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Coordinates}
    \begin{codeexp}<+->
        \lstinline|\coordinate[§key-val§] (§name§) at (§coord§);|
        \begin{itemize}[<+->]
            \item Place coordinate at \a{coord} with name \a{name}
            \item Basically a \lstinline|\node| of dimension zero
            \item \alert{Note}: no \lstinline|inner sep| or \lstinline|outer sep|
            \item \a{key-val} (among others):
                \begin{itemize}
                    \item \lstinline|label=§place§:§label§|: place \a{label} at \a{place} (\lstinline|above|, \dots)
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.6\textwidth]<+->
\begin{tikzpicture}
\coordinate[label=left:\(A\)]
    (A) at (0,0);
\coordinate[label=right:\(B\)]
    (B) at (1,1);
\draw (A) circle[radius=2pt]
    -- (B) circle[radius=2pt];
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Transformations}
    All paths accept the following keys in \a{key-val}:
    \begin{itemize}[<+->]
        \item \lstinline|xshift=§length§|, \lstinline|yshift=§length§|: shift all coordinates in x-/y-direction
        \item \lstinline|shift=(§coord§)|: shift all coordinates by adding \a{coord}
        \item \lstinline|scale=§frac§|: multiply all coordinates by \a{frac}
        \item \lstinline|xscale=§frac§|, \lstinline|yscale=§frac§|: scale in x-/y-direction
        \item \lstinline|scale around={§frac§:(§coord§)}|: origin of scaling is \a{coord}
        \item \lstinline|rotate=§angle§|: rotate all coordinates by \a{angle} degrees
        \item \lstinline|rotate around={§angle§:(§coord§)}|: origin of rotation is \a{coord}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Plotting functions}
    \begin{codeexp}<+->
        \lstinline|\draw[§key-val§] plot (§coord§);|
        \begin{itemize}[<+->]
            \item Plots a function specified by \a{coord}
            \item \a{coord} should contain the running variable \lstinline|\x|
            \item \a{key-val}:
                \begin{itemize}
                    \item \lstinline|sample=§num§| (default \lstinline|25|): number of points to approximate the function
                    \item \lstinline|domain=§start§:§end§| (default \lstinline|-5:5|): range of values for \lstinline|\x|
                    \item \lstinline|smooth|: make approximation smooth
                \end{itemize}
            \item See the package \lstinline|pgfplots| for more advanced plots
        \end{itemize}
    \end{codeexp}

    \begin{code}[lefthand width=.7\textwidth, sidebyside gap=0pt]<+->
\begin{tikzpicture}
    [every plot/.style={domain=-1:1}]
\draw plot ({exp(\x)}, \x+.5);
\draw[samples=100] plot ({exp(\x)}, \x);
\draw[smooth] plot ({exp(\x)}, \x-.5);
\end{tikzpicture}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Loops}
    \begin{codeexp}<+->
        \lstinline|\foreach §vars§ [§options§] in {§list§} {§\dots§}|
        \begin{itemize}[<+->]
            \item Iterates with \a{vars} over \a{list} executing \a{\dots}
            \item \a{vars}: one (e.\,g.\ \lstinline|\i|), or multiple (e.\,g.\ three \lstinline|\xcoord/\ycoord|)
            \item \a{list}: specify values for each variable in the same syntax, e.\,g.\ \lstinline|{0/0, 1/-1, .5/.3}|
            \item \a{list} can contain \lstinline|...| notation, e.\,g.\
                \begin{itemize}
                    \item \lstinline|{0,...,5}| is \lstinline|{0,1,2,3,4,5}|
                    \item \lstinline|{a_1,...,d_1}| is \lstinline|{a_1,b_1,c_1,d_1}|
                    \item \lstinline|{8,...,3.3}| is \lstinline|{8,7,6,5,4}| (stopping before \lstinline|3.3|)
                    \item \lstinline|{0,.1,...,.4}| is \lstinline|{0,.1,.2,.3,.4}| (difference calculated from first two values)
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Loops}
    \begin{block}{\a{options}}<+->
        \begin{itemize}[<+->]
            \item \lstinline|evaluate=§var§|\\
                \lstinline|evaluate=§var§ as §newvar§|\\
                \lstinline|evaluate=§var§ as §newvar§ using §formula§|\\
                Evaluates \a{var} as a math expression
            \item \lstinline|remember=§var§ as §newvar§|\\
                \lstinline|remember=§var§ as §newvar§ (initially §value§)|\\
                Remember \a{var} from previous iteration
            \item \lstinline|count=§newvar§|\\
                \lstinline|count=§newvar§ from §vaue§|\\
                Define counter
        \end{itemize}
    \end{block}

    \begin{exampleblock}{Extra}<+->
        For even more programming, see the \TikZ{} library \lstinline|math|
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Loops}
    \begin{codealt}[listing above text, listing options={basicstyle=\tiny\ttfamily}]
\begin{tikzpicture}
    \foreach \i [remember=\i as \j (initally 0)] in {1,...,5}
        \node[draw, circle, label=90-360/5*\j:\(g_\i H g_\i^{-1}\)]
            (\i) at (90-360/5*\j:1.5) {\(h^*\)};
    \foreach \i [evaluate=\i as \j using {int(mod(\i,5)+1)}] in {1,...,5}
        \draw[->] (\i) edge [bend left=30, looseness=.75] (\j);
\end{tikzpicture}
    \end{codealt}
\end{frame}

\begin{frame}[fragile]{Tips on creating diagrams}
    \begin{itemize}[<+->]
        \item Plan the diagram beforehand, maybe with pencil and paper
        \item (If necessary) calculate crucial points geometrically
        \item Use the same font size, same (default) line width, same colours as the text in the document
        \item Keep in mind or exploit the fact that \TikZ{} draws \alert{sequentially}, i.\,e.\ later stuff overdraws previous stuff
        \item Define \lstinline|\node| or \lstinline|\coordinate| if points are used repeatedly
        \item Use the \lstinline|/.style| syntax for repeated options (can also be used in \lstinline|\tikzset|)
            \begin{lstlisting}
                \begin{tikzpicture}[
                        mystyle/.style={§\dots§},
                        every node/.style={§\dots§}
                    ]
                    \draw[mystyle, red] §\dots§;
                \end{tikzpicture}
            \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Homework}
    \begin{exampleblock}{\home}
        Recreate the following diagram.

        \begin{tikzpicture}
            \draw[help lines] (0,0) grid (4,4);
            \draw[->] (0,0) -- (4,0) node[below] {\(x\)};
            \draw[->] (0,0) -- (0,4) node[left] {\(y\)};
            \draw[domain=0:4, samples=100, smooth] plot (\x, {exp(\x)/20}) node[right] {\(f(x) = \exp(x) / 20\)};
        \end{tikzpicture}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Homework}
    \begin{exampleblock}{\home*}
        Draw the unit circle for the \(L^p\)-norm with \(p = 6\) (see \href{https://en.wikipedia.org/wiki/Lp_space#Definition}{Wikipedia}).
        \emph{Tip:} use polar coordinates as in \lstinline|({cos(\x)}, {sin(\x)})| for plotting.

        \begin{tikzpicture}
            \draw[->] (-2,0) -- (2,0);
            \draw[->] (0,-2) -- (0,2);
            \foreach \i/\j in {-1/-1, -1/1, 1/-1, 1/1}
                \draw[domain=0:90, samples=100] plot ({\i*pow(cos(\x), 1/3)}, {\j*pow(sin(\x), 1/3)});
        \end{tikzpicture}
    \end{exampleblock}
\end{frame}

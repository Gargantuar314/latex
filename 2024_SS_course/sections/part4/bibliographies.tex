\section{Bibliographies}

\begin{frame}[fragile]{Simple bibliographies by hand}
    \begin{codeexp}<+->
        \lstinline|\begin{thebibliography}{§widest-label§}|\\
        \tabstrut\lstinline|\bibitem[§label§]{§key§} §\dots§|\\
        \lstinline|\end{thebibliography}|
        \begin{itemize}[<+->]
            \item Is typed like a list
            \item For \a{key}, same rules as for keys in cross-references apply (I~use the prefix \lstinline|bib:|)
            \item Enumerates like [1],~[2],~\dots{} by default; can be overwritten with \a{label}
            \item \a{widest-label} determines list indentation
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\cite[§note§]{§key1§, §key2§, §\dots§}|
        \begin{itemize}[<+->]
            \item Prints \enquote{[\a{label1}, \a{label2}, \a{\dots}, \a{note}]} where \a{labeli} is the label associated to \a{keyi}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Simple bibliographies by hand}
    \begin{pdflist}[listing options={firstline=4, lastline=13}, lefthand width=.6\textwidth, comment style={scale=1.7}, sidebyside gap=0pt]
\documentclass{article}
\usepackage[papersize={5.5cm,8cm}, scale=.9]{geometry}
\begin{document}
\cite[ch.~2, \S3]{bib1},
\cite[pp.~35--49]{bib2}.
\begin{thebibliography}{Gro1}
  \bibitem[Gro1]{bib1}
  \textsc{Grothendieck, A.} (1971):
  \textit{Revêtements étales et groupe fondamental (SGA~1).}
  In: Lecture notes in mathematics \textbf{224}.
  Berlin, New York: Springer.
  \bibitem{bib2} blob
\end{thebibliography}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Automatic bibliographies with Bib\LaTeX}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{biblatex}|
        \begin{itemize}[<+->]
            \item Most modern and most powerful bibliography manager to date
            \item Used in tandem with the external program \lstinline|biber|
            \item Based on the \emph{fossil} Bib\TeX, which is (sadly) still very widespread
            \item Integration with \lstinline|babel| and \lstinline|csquotes|
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Remark}<+->
        AMS will \emph{never} support Bib\LaTeX, but rather endorse their own \lstinline|amsrefs| package based on Bib\TeX.
        However, I've found (but never tested) a \href{https://github.com/konn/biblatex-math}{project by Hiromi Ishii} which emulates the AMS style.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Bibliography format}
    \begin{itemize}[<+->]
        \item Bibliographical data is usually written in a \verb|.bib| file
        \item \alert{Tip:} specify as much bibliographical data as you can
    \end{itemize}

    \begin{codeexp}<+->
        \lstinline|@§type§{§key§,|\\
        \tabstrut\lstinline|§field1§ = {§data1§},| (more common brace syntax)\\
        \tabstrut\lstinline|§field2§ = "§data2§",| (less common quotes syntax)\\
        \tabstrut\a{\dots}\\
        \lstinline|}|
        \begin{itemize}[<+->]
            \item \a{type}: type of reference (see later)
            \item \a{key}: key to be used as \lstinline|\cite{§key§}|\\
                Must not contain spaces or any of \verb|"#%,={}'()-\|
            \item \a{fieldi}: fields specific to \a{type}\\
                Some are mandatory, some are optional
            \item \a{datai}: anything goes, including commas
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Bibliography format: field types}
    \begin{block}{Name lists}<+->
        \begin{itemize}[<+->]
            \item E.\,g.\ fields \lstinline|author|, \lstinline|editor|
            \item Multiple names are split with \lstinline|and|
            \item \alert{Recommendation:} split last and first name with comma
            \item List can be finished with \lstinline|and others|
            \item Protect splitting of any kind with braces \lstinline|{ }|
        \end{itemize}
    \end{block}

    \begin{actionenv}<+->
        \begin{lstlisting}
            author = {Knuth, D. E. and von Neumann, John and others},
            editor = {{Lion and Noble, Ltd.}}
        \end{lstlisting}
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Bibliography format: field types}
    \begin{block}{Literal lists}<+->
        \begin{itemize}[<+->]
            \item E.\,g.\ fields \lstinline|publisher|, \lstinline|location|
            \item Almost identical to name lists, but no split at commas or last\slash first names
            \item I.\,e.: split multiple names with \lstinline|and|, optionally finish list with \lstinline|and others|
            \item Protect splitting of any kind with braces \lstinline|{ }|
        \end{itemize}
    \end{block}

    \begin{block}{Fields}<+->
        \begin{itemize}[<+->]
            \item E.\,g.\ almost all other fields
            \item Data is taken as is
            \item Dates should be given as \lstinline|yyyy-mm-dd|
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Standard types (selection)}
    \begin{exampleblock}{Remark}<+->
        \begin{itemize}[<+->]
            \item See the Bib\LaTeX{} docs, §2.1.1 for more types
            \item All types here accept \lstinline|title|, \lstinline|date|
            \item All types here accept \lstinline|language|, \lstinline|doi|, \lstinline|url|, \lstinline|urldate|
        \end{itemize}
    \end{exampleblock}

    \begin{itemize}[<+->]
        \item \lstinline|article|: article in journal or magazine
            \begin{itemize}[<1->]
                \item Additionally required: \lstinline|author|, \lstinline|journaltitle|
                \item Additionally optional: \lstinline|volume|, \lstinline|number|, \lstinline|pages|
            \end{itemize}
        \item \lstinline|book|: single-volume book
            \begin{itemize}[<1->]
                \item Additionally required: \lstinline|author|
                \item Additionally optional: \lstinline|editor|, \lstinline|subtitle|, \lstinline|series|, \lstinline|number|, \lstinline|publisher|, \lstinline|location|, \lstinline|isbn|
            \end{itemize}
        \item \lstinline|online|: online resource
            \begin{itemize}[<1->]
                \item Additionally required: \lstinline|author|, \lstinline|url|
                \item Additionally optional: \lstinline|subtitle|, \lstinline|version|
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Standard types (selection)}
    \begin{itemize}[<+->]
        \item \lstinline|proceedings|: single-volume conference proceedings
            \begin{itemize}[<1->]
                \item Additionally optional: \lstinline|editor|, \lstinline|eventtitle|, \lstinline|eventdate|, \lstinline|series|, \lstinline|number|, \lstinline|publisher|, \lstinline|location|, \lstinline|isbn|
            \end{itemize}
        \item \lstinline|thesis|: thesis for requirements for a degree
            \begin{itemize}[<1->]
                \item Additionally required: \lstinline|author|, \lstinline|type|, \lstinline|institution|
                \item Additionally optional: \lstinline|subtitle|
            \end{itemize}
        \item \lstinline|unpublished|: work not formally published
            \begin{itemize}[<1->]
                \item Additionally required: \lstinline|author|
                \item Additionally optional: \lstinline|subtitle|, \lstinline|howpublished|
            \end{itemize}
        \item \lstinline|mvbook|, \lstinline|mvproceedings|: multi-volume versions (\alert{use these})
            \begin{itemize}[<1->]
                \item Additionally optional: \lstinline|volume|, \lstinline|volumes|
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Standard fields (selection)}
    \begin{exampleblock}{Remark}<+->
        See the Bib\LaTeX{} docs, §2.2.2 for field descriptions
    \end{exampleblock}

    \begin{block}{Some explanations for certain fields}<+->
        \begin{itemize}[<+->]
            \item \lstinline|number| (literal): number in series
            \item \lstinline|series| (literal): name of series like \enquote{Studies in \dots}
            \item \lstinline|volume| (integer): volume number of the work
            \item \lstinline|volumes| (integer): total number of volumes
            \item \lstinline|institution|: name of university or similar
            \item \lstinline|urldate|: URL access date
        \end{itemize}
    \end{block}

    \begin{alertblock}{Pro tip for math citations}<+->
        AMS' free \href{https://mathscinet.ams.org/mathscinet/freetools/mref}{MRef} provides high quality Bib\TeX{} format information (compatible with Bib\LaTeX).
        Alternatively, use AMS' \href{https://mathscinet.ams.org/mathscinet/publications-search}{MathSciNet} (subscription based).
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Citing arXiv papers}
    \begin{alertblock}{Tip}<+->
        Use the \enquote{Export Bib\TeX{} Citation} functionality on arXiv.
    \end{alertblock}

    \begin{exampleblock}{Note}<+->
        There are some extra fields \lstinline|eprint|, \lstinline|archivePrefix|, \lstinline|primaryClass|.
        Leave them there!
        (Bib\LaTeX{} supports some arXiv-specific fields\@.)
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Bib\LaTeX{} \a{options}}
    More than 100 option, see the Bib\LaTeX{} docs.

    \begin{codeexp}<+->
        \lstinline|sorting=§sort§|
        \begin{itemize}[<+->]
            \item Sorting order of the bibliography
            \item 
                \begin{itemize}[<1->]
                    \item \lstinline|n|~for \enquote{author name}
                    \item \lstinline|t|~for \enquote{title}
                    \item \lstinline|y|~for \enquote{publication year}
                    \item \lstinline|a|~for \enquote{alphabetic label} (requires an \enquote{alphabetic} style)
                \end{itemize}
            \item \a{sort} (selection): one of \lstinline|nty| (default), \lstinline|nyt|, \lstinline|ynt|, \lstinline|ydnt| (descending year), \lstinline|anyt|, \lstinline|none| (sort by appearance in text)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|style=§style§|
        \begin{itemize}[<+->]
            \item Citation style used in text
            \item Can be altered with other Bib\LaTeX{} commands
            \item Custom styles via \verb|.bbx| or \verb|.cbx| files
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Bib\LaTeX{} \a{options}}
    \begin{block}{Standard citation styles (selection)}
        \begin{itemize}[<+->]
            \item Some styles have alterations:
                \begin{itemize}
                    \item \lstinline|..-comp|: compress, i.\,e.\ sort and compress multiple keys in \lstinline|\cite|; e.\,g.\ \enquote{[8,~3, 1, 7,~2]} vs.\ \enquote{[1--3, 7,~8]}
                    \item \lstinline|..-verb|: verbose, i.\,e.\ split multiple keys in \lstinline|\cite|; e.\,g.\ \enquote{[2,~5,~6]} vs.\ \enquote{[2];~[5];~[6]}
                \end{itemize}
            \item \lstinline|numeric|, \lstinline|numeric-comp|, \lstinline|numeric-verb| (\enquote{\LaTeX/IEEE style}): labels are numbers [1],~[2],~\dots
            \item \lstinline|alphabetic|, \lstinline|alphabetic-verb| (like \enquote{AMS style}): auto-generated labels based on author name and year, e.\,g.\ [Jon86], [JW86]
            \item \lstinline|authoryear|, \lstinline|authoryear-comp| (like \enquote{Harvard style}):\\
                labels are last name, year and letter if ambiguous, e.\,g.\ Doe 1995a, Doe 1995b, Jones 1998
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Citing in text}
    \begin{codeexp}<+->
        \lstinline|\cite[§pre§][§post§]{§key§}|
        \begin{itemize}[<+->]
            \item Cite command like in standard \LaTeX
            \item \a{pre}: note before citation, usually \enquote{see}, \enquote{see also}, \enquote{cf.}
            \item \a{post}: note after citation, usually chapter\slash section or page numbers
                \begin{itemize}
                    \item If \a{post} is single Roman or Arabic number, prints \enquote{p.\ \a{post}}
                    \item If \a{post} is range of numbers like \lstinline|27--35|, prints \enquote{pp.\ \a{post}}
                \end{itemize}
            \item \a{key}: single key associated to reference
            \item Only one optional argument is interpreted as \a{post}; use empty brackets if only \a{pre} is desired, e.\,g.\ \lstinline|\cite[see][]{§key§}|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\cites[§pre1§][§post1§]{§key1§}[§pre2§][§post2§]{§key2§}§\dots§|
        \begin{itemize}
            \item Multi-citation version of \lstinline|\cite|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Citing in text}
    \begin{codeexp}<+->
        \lstinline|\parencite[§pre§][§post§]{§key§}|\\
        \lstinline|\footcite[§pre§][§post§]{§key§}|
        \begin{itemize}[<+->]
            \item Wrap \lstinline|\cite| in parentheses or place \lstinline|\cite| in footnote
            \item \lstinline|\parencite| has no effect with \lstinline|numeric| or \lstinline|alphabetic| styles
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\supercite{§key§}|
        \begin{itemize}[<+->]
            \item Only with \lstinline|numeric| styles
            \item Places number of reference as superscript without brackets (e.\,g.\ in \emph{Nature} Journal)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\nocite{§key§}|
        \begin{itemize}[<+->]
            \item By default, only cited references are included in the bibliography
            \item \lstinline|\nocite| adds reference associated to \a{key} to the bibliography
            \item If \a{key} is \lstinline|*|, then it adds \emph{all} references to the bibliography
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Including the bibliography}
    \begin{codeexp}<+->
        \lstinline|\addbibresource[§options§]{§resource§}|
        \begin{itemize}
            \item Adds \a{resource}, usually a \verb|.bib| file, to the available resources
            \item Should be issued in the preamble
            \item \a{resource}: full path name (with extension), relative to current directory
            \item \a{options}: e.\,g.\ \lstinline|label=§label§| to label \a{resource}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\printbibliography[§key-value§]|
        \begin{itemize}
            \item Prints the bibliography, usually at the end of the document
            \item Possible \a{key-value} pairs:
                \begin{itemize}
                    \item \lstinline|title=§title§|: alternative title to \emph{Bibliography}
                    \item \lstinline|prenote=§name§|: text between heading and actual bibliography\\
                        \a{name} needs to be defined by \lstinline|\defbibnote{§name§}{§text§}|
                    \item \lstinline|postnote=§name§|: text after bibliography, similar to \lstinline|prenote|
                    \item \lstinline|type=§type§|: only include entries of \a{type}
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\section{Algorithms in pseudocode}

\begin{frame}[fragile]{Algorithm packages}
    \begin{codeexp}<+->
        \lstinline|\usepackage{algorithm}|
        \begin{itemize}
            \item Part of the \lstinline|algorithmic| package
            \item Provides the \lstinline|algorithm| float
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\usepackage[§option§]{algpseudocode}|
        \begin{itemize}[<+->]
            \item Part of the \lstinline|algorithmicx| package
            \item Mimics the style from \lstinline|algorithmic|
            \item Enables definitions for new pseudocode keywords
            \item \a{option}: \lstinline|noend| will remove all lines like \textbf{end if} from the PDF
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Creating an algorithm}
    \begin{block}{Typical algorithm float}<+->
        \begin{lstlisting}
            \begin{algorithm}
                \caption{§\dots§}
                \begin{algorithmic}[§num§]
                    §algorithm§
                \end{algorithmic}
            \end{algorithm}
        \end{lstlisting}
    \end{block}

    \begin{itemize}[<+->]
        \item \a{num}: numbers every \a{num}th line
            \begin{itemize}
                \item \lstinline|1|~numbers every line
                \item \lstinline|0|~numbers no line (default)
            \end{itemize}
        \item Lines can be cross-references with \lstinline|\label|
        \item Indentation in \a{algorithm} is done automatically
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{Statements and comments}
    \begin{code}
\begin{algorithmic}[1]
    \State \(w \gets 0\)
    \Comment{comment}
    \Statex New part
    \State \(v \gets 2+w\)
\end{algorithmic}
    \end{code}

    \begin{itemize}[<2->]
        \item \lstinline|\State|: starts a new line with normal text
        \item Not needed for blocks defined by \lstinline|algpseudocode|
        \item<3-> \lstinline|\Statex|: same as \lstinline|\State|, but without line number
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{For blocks}
    \begin{code}
\begin{algorithmic}[1]
    \For{cond}
        \State body
    \EndFor
\end{algorithmic}
    \end{code}

    \begin{code}<2->
\begin{algorithmic}[1]
    \ForAll{cond}
        \State body
    \EndFor
\end{algorithmic}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{While and repeat blocks}
    \begin{code}
\begin{algorithmic}[1]
    \While{cond}
        \State body
    \EndWhile
\end{algorithmic}
    \end{code}

    \begin{code}<2->
\begin{algorithmic}[1]
    \Repeat
        \State body
    \Until{cond}
\end{algorithmic}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{If block}
    \begin{code}
\begin{algorithmic}[1]
    \If{cond}
        \State body
    \ElsIf{cond}
        \State body
    \ElsIf{cond}
        \State body
    \Else
        \State body
    \EndIf
\end{algorithmic}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{Procedure and function block}
    \begin{code}
\begin{algorithmic}[1]
    \Procedure{name}{args}
        \State body
    \EndProcedure
\end{algorithmic}
    \end{code}

    \begin{code}<2->
\begin{algorithmic}[1]
    \Function{name}{args}
        \State body
    \EndFunction
\end{algorithmic}
    \end{code}

    \uncover<3->{\structure{Function calls}}
    \begin{code}<3->
\begin{algorithmic}[1]
    \State \Call{name}{args}
\end{algorithmic}
    \end{code}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \begin{alertblock}{Warning}
        \lstinline|\( \)| cannot be used in \a{args} for all three commands on the previous slide; instead, \lstinline|$ $| is necessary.
        For a fix, see \href{https://tex.stackexchange.com/questions/669665}{\TeX{} SE}.
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Typing pseudocode}
    \structure{(Infinite) loop block}
    \begin{code}
\begin{algorithmic}[1]
    \Loop
        \State body
    \EndLoop
\end{algorithmic}
    \end{code}

    \structure<2->{Require/ensure conditions}\\
    \begin{code}<2->
\begin{algorithmic}[1]
    \Require cond
    \Ensure cond
    \State \(w \gets 0\)
\end{algorithmic}
    \end{code}
    \begin{itemize}[<2->]
        \item \lstinline|\Require|\slash\lstinline|\Ensure| should not have preceding \lstinline|\State|
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Pseudocode example}
    \begin{pdflist}[listing options={firstline=6, lastline=20, basicstyle=\footnotesize\ttfamily}, sidebyside gap=0pt, comment style={scale=2}, lefthand width=.55\textwidth]
\documentclass{article}
\usepackage[papersize={7cm,6cm}, scale=.9]{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\begin{document}
\begin{algorithm}
\caption{Euclid's algorithm}
\label{alg:euclid}
\begin{algorithmic}[1]
  \Procedure{Euclid}{$a,b$}
  \Comment{The gcd of \(a\) and \(b\)}
    \State \(r \gets a \bmod b\)
    \While{\(r \ne 0\)}
      \State \(a \gets b\)
      \State \(b \gets r\)
      \State \(r \gets a \bmod b\)
    \EndWhile
    \State \textbf{return} \(b\)
  \EndProcedure
\end{algorithmic}
\end{algorithm}
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Defining new blocks}
    \begin{codeexp}<+->
        \lstinline|\algblock{§start§}{§end§}|
        \begin{itemize}[<+->]
            \item Defines a \lstinline|\§start§ \§end§| block
            \item<.-> Starting text is \lstinline|\textbf{§start§}|, similar for end text
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\algblockdefx{§start§}{§end§}|\\
        \tabstrut\lstinline|[§start-num-args§][§start-default-opt§]{§start-text§}|\\
        \tabstrut\lstinline|[§end-num-args§][§end-default-opt§]{§end-text§}|
        \begin{itemize}[<+->]
            \item<.-> Defines a \lstinline|\§start§ \§end§| block
            \item Second line works like \lstinline|\newcommand|; defines \lstinline|\§start§| which may accept arguments
            \item Same for the third line and \lstinline|\§end§|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Defining new blocks}
    \begin{codeexp}<+->
        \lstinline|\algcblock{§known-start§}{§mid§}{§end§}|
        \begin{itemize}[<+->]
            \item<.-> Defines continued blocks \lstinline|\§known-start§ \§mid§ \§end§| like \lstinline|\If \Else \EndIf|
            \item \a{known-start} has to be an already defined start of a block
            \item Middle text is \lstinline|\textbf{§mid§}|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\algcblockdefx{§known-start§}{§mid§}{§end§}|\\
        \tabstrut\lstinline|[§mid-num-args§][§mid-default-opt§]{§mid-text§}|\\
        \tabstrut\lstinline|[§end-num-args§][§end-default-opt§]{§end-text§}|
        \begin{itemize}
            \item Similar to \lstinline|\algblockdefx| for continued blocks
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Note}<+->
        For more advanced definitions, see the \lstinline|algorithmicx| docs
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Defining new blocks}
    \begin{exampleblock}{Example}
        The following reimplements the \lstinline|\If \ElsIf \Else \EndIf| block.
        \begin{lstlisting}
            \algblockdefx{If}{EndIf}
                [1]{\textbf{if} #1\ \textbf{then}}
                {\textbf{end if}}
            \algcblockdefx{If}{ElsIf}{EndIf}
                [1]{\textbf{else if} #1\ \textbf{then}}
                {\textbf{end if}}
            \algcblockdefx{ElsIf}{ElsIf}{EndIf}
                [1]{\textbf{else if} #1\ \textbf{then}}
                {\textbf{end if}}
            \algcblock{If}{Else}{EndIf}
            \algcblock{ElsIf}{Else}{EndIf}
        \end{lstlisting}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Redefining blocks}
    \begin{codeexp}<+->
        \lstinline|\algrenewtext{§cmd§}[§num-args§][§default-opt§]{§text§}|
        \begin{itemize}[<+->]
            \item Redefines \lstinline|\§cmd§| appearing in some block
            \item Works like \lstinline|\renewcommand|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Defining new commands}
    \begin{codeexp}<+->
        \lstinline|\algnewcommand{\§cmd§}{§text§}|
        \lstinline|\algrenewcommand{\§cmd§}{§text§}|
        \begin{itemize}[<+->]
            \item All pre-defined keywords are saved as \lstinline|\algorithmic§keyword§|
            \item Can (re)define keywords
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Example: require/ensure type commands}<+->
        \begin{lstlisting}
            % Defining "\Input"
            \algnewcommand{\algorithmicinput}{\textbf{Input:}}
            \algnewcommand{\Input}{\item[\algorithmicinput]}
        \end{lstlisting}
    \end{exampleblock}
\end{frame}



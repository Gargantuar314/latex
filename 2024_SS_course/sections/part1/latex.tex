\section{What is \LaTeX?}

\begin{frame}[fragile]{History}
    \begin{enumerate}[<+->]
        \item[1978] \textit{\alert{\TeX} -- Donald Knuth}\\
            Digital typesetting system
        \item[1979] \textit{\alert{METAFONT} -- Donald Knuth}\\
            Origin of \emph{Computer Modern} font family
        \item[1984] \textit{\alert{\LaTeX} -- Leslie Lamport}\\
            Typesetting and designing documents, \enquote{\TeX{} with macros}
        \item[1994]<.-> Release of \alert{\LaTeXe}
        \item[1996] \textit{\alert{pdf\TeX} -- \foreignlanguage{vietnamese}{Hàn Thế Thành}}\\
            \TeX{} with PDF output and microtypography
        \item[2004]<.-> \textit{\alert{\XeTeX} -- Jonathan Kew}\\
            \TeX{} with modern font support
        \item[2007]<.-> \textit{\alert{\LuaTeX} -- Taco Hekwater, Hartmut Henkel, Hans Hagen}\\
            \TeX{} extended with programming language Lua
        \item[2018] Cancellation of \alert{\LaTeX\,3}, updating \LaTeXe{} instead
    \end{enumerate}
\end{frame}

\begin{frame}{What is \LaTeX?}
    \structure{Typical process of writing a book}
    \begin{actionenv}<+->
        \begin{figure}
            \begin{tikzpicture}[box/.style={inner sep=6pt, text depth=0pt, thick}, >={Stealth[scale=1.5]}]
                \node[draw, box] (a) at (0,0) {author};
                \node[draw, box] (b) at (4,0) {book designer};
                \node[draw, box] (c) at (8,0) {typesetter};
                \draw[->, thick] (a) -- (b);
                \draw[->, thick] (b) -- (c);
                \uncover<+->{\node[box] at (0,-1) {you};}
                \uncover<.->{\node[box] at (4,-1) {\LaTeX{} (\&~you)};}
                \uncover<.->{\node[box] at (8,-1) {\TeX{} (\&~you)};}
            \end{tikzpicture}
        \end{figure}
    \end{actionenv}

    \begin{itemize}[<+->]
        \item \alert{WYSIWM}~-- specify content and logical structure of your text
        \item Let \LaTeX{} and \TeX{} handle the rest (and \alert{trust the defaults})
        \item Strongly related to \alert{typography} (legibility \&~aesthetics)
        \item Common text editing programs cannot reach same quality
    \end{itemize}
\end{frame}

\begin{frame}{Goals for these talks}
    \begin{block}{What am I assuming?}<+->
        You have heard of \LaTeX, know that it is cool and that everyone is using it, yet you have never typed one backslash in your life.
    \end{block}

    \begin{block}{Goals}<+->
        \begin{enumerate}[<+->]
            \item Explaining basic functioning of \LaTeX{} and its syntax\\
                \begin{itemize}[<1->]
                    \item Focusing on \alert{pdf\LaTeX}, not \XeLaTeX{} or \LuaLaTeX\\
                    \item Focusing on writing \alert{documents}, not letters, presentations, etc.
                \end{itemize}
            \item Basic typography along the way
        \end{enumerate}
    \end{block}

    \begin{block}{The idea of these talks}<+->
        \begin{itemize}
            \item Gallery of features
            \item Practice makes perfect
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Resources}
    \begin{block}{Asking questions}
        \begin{itemize}
            \item \href{https://tex.stackexchange.com/}{\textit{\TeX{} StackExchange}}\\[\itemsep]
                Search for questions and answers, and don't be afraid to ask for yourself.
        \end{itemize}
    \end{block}

    \begin{exampleblock}{Tips for asking questions on \TeX~SE}<2->
        \begin{itemize}
            \item (Re)search on related questions, ask only if you still cannot help yourself
            \item Descriptive title and detailed explanation of your problem
            \item What have you tried?
                Other helpful \TeX~SE questions?
            \item \alert{Provide a minimal working example \href{https://tex.meta.stackexchange.com/questions/228}{(MWE)}} (if possible)
            \item Use tags
            \item Be engaging and friendly in the comments
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}{Resources}
    \begin{block}{Textbook}
        \begin{itemize}
            \item Tobias Oetiker \emph{et~al.}: \href{https://tobi.oetiker.ch/lshort/lshort.pdf}{\textit{The Not So Short Introduction to \LaTeX}}\\
                My inspiration for these talks.
        \end{itemize}
    \end{block}

    \begin{block}{Looking up stuff}<2->
        \begin{itemize}
            \item Karl Berry \textit{et~al.}: \href{https://latexref.xyz/dev/latex2e.pdf}{\textit{\LaTeXe: An unofficial reference manual}}\\
                Concise yet thorough manual on all standard \LaTeX{} commands.
            \item Various authors: \href{https://en.wikibooks.org/wiki/LaTeX}{\textit{\LaTeX{} Wikibooks}}\\
                Good overview over various topics, but a bit dated.
            \item \href{https://ctan.org/}{\textit{The Comprehensive \TeX{} Archive Network (CTAN)}}\\
                Documentation on (virtually) any package.
            \item Daniel Kirsch: \href{https://detexify.kirelabs.org/classify.html}{\textit{Detexify}}\\
                Search for symbols by drawing them.
            \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Resources}
    \begin{block}{Going hardcore}
        \begin{itemize}
            \item Donald Knuth: \href{https://visualmatheditor.equatheque.net/doc/texbook.pdf}{\textit{The \TeX book}}\\
                \emph{The} book on \TeX{} with many exercises by the inventor.
            \item Victor Eijkhout: \href{https://github.com/VictorEijkhout/tex-by-topic/blob/main/TeXbyTopic.pdf}{\textit{\TeX{} by Topic}}\\
                Systematic in-depth treatment of all intricacies of \TeX.
            \item Leslie Lamport: \textit{\LaTeX: A Document Preparation System}\\
                Book on \LaTeX{} from the original creator.
                Very dated.
            \item Frank Mittelbach, Ulrike Fischer: \textit{The \LaTeX{} Companion I~\&~II}\\
                \emph{The} book on \LaTeX, explaining internal workings and many popular packages.
            \item Joseph Wright: \href{https://texfaq.org/}{\textit{\TeX{} FAQ}}\\
                Curated list of questions with more advanced answers.
            \item \LaTeX{} Project Team: \href{https://ctan.net/macros/latex/base/source2e.pdf}{\textit{The \LaTeXe{} Sources}}\\
                Official 1308 pages of madness on the source code of \LaTeXe.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Installation}
    \begin{structure}{What do we need to start \TeX-ing?}
        \begin{enumerate}[<+->]
            \item \LaTeX{} Distribution: computer needs to understand \LaTeX
            \item Text editor: we need to type some code
                \begin{itemize}[<1->]
                    \item[\textrightarrow] Ideally with good \LaTeX{} integration
                \end{itemize}
            \item \alert{Non-locking} PDF viewer
                \begin{itemize}[<1->]
                    \item[\textrightarrow] We don't want to close the PDF viewer for every compilation
                \end{itemize}
        \end{enumerate}
    \end{structure}
\end{frame}

\begin{frame}{Installation}
    \begin{block}{\LaTeX{} Distribution}<+->
        \href{https://www.tug.org/texlive/}{\TeX Live} (Mac\TeX{} for Mac)
        \begin{itemize}
            \item Groups packages into collections
            \item Cross-platform
        \end{itemize}
    \end{block}

    \begin{alertblock}{Tips on the installation}<+->
        \begin{itemize}
            \item Download for user only, not system-wide
            \item By default, \TeX Live downloads \emph{every package that exists} (>~5000 packages, >~7\,GB)
            \item Linux: download specific collections through package manager
            \item<.-> Windows: under \emph{Advanced} button, deselect all collections, and later download the needed collections in \emph{\TeX{} Live Shell}
            \item<.-> Mac~OS: download Basic\TeX~(?)
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}{Installation}
    \begin{exampleblock}{Recommended collections}
        \begin{itemize}
            \item Strongly recommended:
                \begin{itemize}
                    \item \emph{basic}
                    \item \emph{latex}
                    \item \emph{latexrecommended}
                    \item \emph{fontsrecommended} (for \lstinline|lmodern|)
                    \item \emph{binextra} (for \alert{\emph{latexmk}})
                \end{itemize}
            \item Recommended:
                \begin{itemize}
                    \item \emph{latexextra} (for \lstinline|enumitem|, \lstinline|csquotes|, \lstinline|cleveref|, \lstinline|imakeidx|)
                    \item \emph{bibtexextra} (for Bib\LaTeX)
                    \item \emph{mathscience} (for \lstinline|diffcoeff|, \lstinline|siunitx|, \lstinline|algorithmicx|)
                    \item \emph{pictures} (for \TikZ, \lstinline|tikz-cd|)
                \end{itemize}
            \item Optional:
                \begin{itemize}
                    \item \emph{langgerman}
                    \item \emph{langfrench}
                \end{itemize}
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}{Installation}
    \begin{block}{Code editor}<+->
        \href{https://code.visualstudio.com/}{Visual Studio Code} (VS~Code)
        \begin{itemize}[<+->]
            \item<.-> Most popular code editor, very versatile
            \item Plugins:
                \begin{itemize}[<1->]
                    \item \emph{\LaTeX{} Workshop} (provides integrated PDF viewer)
                    \item Recommended: \emph{Code Spell Checker} (\emph{British English}, \emph{German})
                \end{itemize}
        \end{itemize}
    \end{block}

    \begin{alertblock}{Pro Tips}<+->
        \begin{itemize}[<+->]
            \item<.-> Set \emph{Editor: Word Wrap} to \emph{bounded} in VS~Code
            \item Use keybindings provided by \href{https://code.visualstudio.com/docs/getstarted/keybindings}{VS~Code} and \href{https://github.com/James-Yu/LaTeX-Workshop/wiki/Snippets}{\LaTeX{} Workshop}
            \item Use the \emph{Sync\TeX} mechanism to jump between code and PDF
                (code to PDF: \texttt{ctrl}+\texttt{alt}+\texttt{j}, PDF to code: \texttt{ctrl}+\texttt{click})
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{What is \emph{latexmk}?}
    \begin{itemize}[<+->]
        \item \LaTeX{} must compile (at least) twice with table of contents, cross-references or hyperlinks
        \item Some packages need other programs to run, e.\,g.\ Bib\LaTeX{} (\lstinline|biber|),
            \lstinline|imakeidx| (\lstinline|makeindex| or \lstinline|xindy|),
            \lstinline|asymptote| (\lstinline|asy|)
        \item \href{https://mg.readthedocs.io/latexmk.html}{\emph{latexmk}} (read \enquote{\LaTeX{} make}) is Perl script that automates all compilations perfectly
    \end{itemize}

    \begin{exampleblock}{\home}<+->
        Install \TeX Live, a~good code editor that ideally has extensive \LaTeX{} support, and a \emph{non-locking} PDF viewer (not necessary if you use VS~Code).
    \end{exampleblock}
\end{frame}

\begin{frame}{Why \emph{not} Overleaf?}
    \begin{block}{Disadvantages}<+->
        \begin{itemize}[<+->]
            \item Proprietary garbage!
            \item Cannot work offline
            \item Editor is hard to work with, no keybindings (except \emph{vim mode})
            \item Limits compilation time and upload size (plot data, images)
            \item Not up-to-date \LaTeX{} kernel and packages
            \item Privacy concerns?
        \end{itemize}
    \end{block}

    \begin{block}{Advantages}<+->
        \begin{itemize}
            \item Works out of the box (for noobs)
            \item Easy co-editing
        \end{itemize}
    \end{block}
\end{frame}

\section{Document structure and \LaTeX{} syntax}

\begin{frame}[fragile]{Document structure}
    File name: \verb|hello_world.tex|
    \begin{pdflist}[lefthand width=.4\textwidth, comment style={scale=1.8}]
\documentclass{article}



\begin{document}

Hello world!

\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{Document structure}
    \begin{minipage}{.4\textwidth}
        \begin{lstlisting}
            \documentclass{article}



            \begin{document}

            Hello world!

            \end{document}
        \end{lstlisting}
    \end{minipage}%
    \begin{minipage}{.55\textwidth}
        \begin{itemize}
            \item[1] \alert{Document class}\\
                Type of document
            \item[2--4] \alert{Preamble}\\
                Packages, definitions, settings
            \item[6--8] \alert{Body}\\
                Printed content
        \end{itemize}
    \end{minipage}

    \begin{exampleblock}{What are all these files?}<2->
        \begin{itemize}
            \item \verb|.tex|: Do not delete!
            \item \verb|.pdf|: Final PDF
            \item \verb|.log|: Log file, useful for searching errors
            \item Other files are unimportant (\alert{keep them} for faster compilation)
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Parsing rules}
    \begin{block}{White space}
        Amount of \alert{white space} (i.\,e.\ spaces, empty lines) doesn't matter for \LaTeX.
        Multiple spaces are \emph{one} space, multiple empty lines are \emph{one} empty line.
    \end{block}

    \begin{code}<2->
ab, a b, a      b
    \end{code}

    \begin{code}<3->
ab,
ab,

ab,


ab
    \end{code}
\end{frame}

\begin{frame}[fragile]{Parsing rules}
    \begin{block}{\TeX{} parsing rules for commands (in one group)}
        \begin{enumerate}[<+->]
            \item Look for backslash character \lstinline|\|
            \item Read command name
                \begin{itemize}[<1->]
                    \item Either all following letters (case sensitive)
                    \item Or single character
                \end{itemize}
            \item Ignore following white space if any
            \item Read \(n\)~tokens (arguments, \(0 \le n \le 9\)) and ignore white space in between them; a~token can be
                \begin{itemize}[<1->]
                    \item Single letters, characters or digits
                    \item A~\alert{group}: Anything in braces (braces are not part of the argument) like \lstinline|{something}|
                    \item Other command names of the form \lstinline|\cmd| (without braces)
                \end{itemize}
        \end{enumerate}
    \end{block}

    \begin{quote}<+->\itshape
        If you encounter a command starting with~\lstinline|\|, read \(n\)~tokens while ignoring white space in between them.
    \end{quote}
\end{frame}

\begin{frame}[fragile]{Parsing rules}
    \begin{exampleblock}{Exercise}
        Let \lstinline|\x| be a command that reads \(n = 2\) arguments.
        Which of the following are equivalent?\\[\topsep]
        \begin{minipage}{.5\textwidth}
            \begin{enumerate}
                \item \lstinline|\x y z|
                \item \lstinline|\x {y} z|
                \item \lstinline|\x y{z}|
                \item \lstinline|\x {y} {z}|
                \item \lstinline|\x yz|
                \item \lstinline|\x{yz}|
                \item \lstinline|\x{yz}{}|
                \item \lstinline|\xy z|
                \item \lstinline|\xy \z|
            \end{enumerate}
        \end{minipage}%
        \begin{minipage}[b]{.5\textwidth}
            \only<2>{
                \emph{Solution:}
                \begin{itemize}
                    \item 1--5
                    \item 6--7
                    \item 8
                    \item 9
                \end{itemize}
            }
        \end{minipage}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Comments}
    \begin{block}{Comments}
        Start comments with percent sign \lstinline|%|
        \begin{lstlisting}
            %% A comment 1
            \foo % \a comment 2
        \end{lstlisting}
    \end{block}

    \begin{alertblock}{Pro tip}<2->
        For multi-line commands, enclose with
        \begin{lstlisting}
            \iffalse
            ...
            \fi
        \end{lstlisting}
    \end{alertblock}

    \begin{alertblock}{Warning}<3->
        In \LaTeX{} programming, use percent signs to suppress unwanted white space and line breaks
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{\LaTeX{} Command Syntax}
    \begin{codeexp}
        \begin{actionenv}<+->
            \lstinline|\§cmdname§[§optarg§]{§arg1§}§\dots§{§argn§}|
        \end{actionenv}

        \begin{itemize}[<+->]
            \item \a{cmdname}: command name
                \begin{itemize}[<1->]
                    \item Either string of letters (case sensitive) or a single character
                \end{itemize}
            \item \a{optarg}: optional argument, if any
                \begin{itemize}[<1->]
                    \item Sometimes multiple arguments separated by commas
                    \item Brackets can be dropped if \a{optarg} is empty
                \end{itemize}
            \item \a{arg1}, \dots, \a{argn} (\(0 \le n \le 9\)): mandatory arguments
                \begin{itemize}[<1->]
                    \item Braces should not be dropped if \a{argi} is empty
                    \item \(n \le 8\) if command accepts any \a{optarg}
                \end{itemize}
        \end{itemize}

        \begin{actionenv}<+->
            Some commands have starred variants:
            \lstinline|\§cmdname§*[§optarg§]{§ar1§}§\dots§{§argn§}|
        \end{actionenv}
    \end{codeexp}

    \begin{exampleblock}{Wait!}<+->
        Isn't \lstinline|[| (resp.\ \lstinline|*|) the first argument, according to \TeX{} parsing rules?\\
        Yes, \LaTeX{} does some magic.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{\LaTeX{} Environment Syntax}
    \begin{actionenv}<+->
        \lstinline|\begin{§envname§}[§optarg§]{§arg1§}§\dots§{§argn§}|\\
        \tabstrut\a{content}\\
        \lstinline|\end{§envname§}|
    \end{actionenv}
    \begin{itemize}[<+->]
        \item \a{envname}: environment name
            \begin{itemize}[<1->]
                \item Consists of letters (case sensitive) only
            \end{itemize}
        \item \a{optarg}: optional argument, if any
            \begin{itemize}[<1->]
                \item Sometimes multiple arguments separated by commas
                \item Brackets can be dropped if \a{optarg} is empty
            \end{itemize}
        \item \a{arg1}, \dots, \a{argn} (\(0 \le n \le 9\)): mandatory arguments
            \begin{itemize}[<1->]
                \item Braces should not be dropped if \a{argi} is empty
                \item \(n \le 8\) if command accepts any \a{optarg}
            \end{itemize}
        \item \a{content}: any content (can be empty)
    \end{itemize}

    \begin{actionenv}<+->
        Some environments have starred variants
        \lstinline|\begin{§envname§*} §\dots§ \end{§envname§*}|
    \end{actionenv}
\end{frame}

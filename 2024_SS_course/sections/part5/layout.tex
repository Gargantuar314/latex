\section{Page layout}

\begin{frame}[fragile]{Page size and margins}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§key-val§]{geometry}|\\
        \lstinline|\geometry{§key-val§}|
        \begin{itemize}[<+->]
            \item<.-> Sets many dimensions of the page layout
            \item \a{key-val}: comma-separated key-value list
            \item<.-> \lstinline|\geometry| enables late-selection of options
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Note}<+->
        The following options can be put in \a{key-val}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Paper size}
    \begin{codeexp}
        \lstinline|a0paper|, \dots, \lstinline|a6paper|,\\
        \lstinline|b0paper|, \dots, \lstinline|b6paper|,\\
        \lstinline|c0paper|, \dots, \lstinline|c6paper|, \lstinline|letterpaper|
        \begin{itemize}
            \item Paper size, default \lstinline|a4paper|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Text size}
    \begin{exampleblock}{Typographical remark}
        The average number of \alert{characters per line} should fall between 60~to~75.
        Depending on the font size, this gives the correct line width.
        Approximately based on the docs for the \lstinline|memoir| package (1~pica = 12~pt):\\
        \begin{center}
            \begin{tabular}{@{}ccc@{}} \toprule
                Font size & Line width in picas & Ratio to A4~paper width \\ \midrule
                10~pt & 24--28~pc & 0.48--0.56 \\
                11~pt & 25--31~pc & 0.50--0.62 \\
                12~pt & 26--33~pc & 0.52--0.66 \\ \bottomrule
            \end{tabular}
        \end{center}
        I~use 0.6 with 10~pt.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Text size}
    \begin{codeexp}<+->
        \lstinline|scale=§frac§|, \lstinline|vscale=§frac§|, \lstinline|hscale=§frac§|
        \begin{itemize}
            \item Set text\slash text height\slash text width as a scaled down version of the paper size
            \item \a{frac}: a~fraction between \lstinline|0| and~\lstinline|1|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|total={§width§, §height§}|, \lstinline|width=§width§|, \lstinline|height=§height§|
        \begin{itemize}
            \item Set width\slash height of text
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|heightrounded|
        \begin{itemize}
            \item Set text height to an integer multiple of \lstinline|\baselineskip|, i.\,e.\ the vertical distance between two text lines
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Margin size}
    \begin{codeexp}<+->
        \lstinline|left=§length§|, \lstinline|right=§length§|, \lstinline|top=§length§|, \lstinline|bottom=§length§|\\
        \lstinline|margin=§length§|, \lstinline|hmargin=§length§|, \lstinline|vmargin=§length§|
        \begin{itemize}
            \item Set margin
            \item For \lstinline|book|: use \lstinline|inner=§length§| (alias for \lstinline|left|), \lstinline|outer=§length§| (alias for \lstinline|right|)
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|hmarginratio=§num§:§num§|, \lstinline|vmarginratio=§num§:§num§|
        \begin{itemize}
            \item Set ratio \lstinline|left| : \lstinline|right| or \lstinline|top| : \lstinline|bottom| resp.
            \item Default \lstinline|hmarginratio|: \lstinline|1:1| for \lstinline|oneside|, \lstinline|2:3| for \lstinline|twoside|
            \item Default \lstinline|vmarginratio|: \lstinline|2:3|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Header and footer}
    \begin{itemize}
        \item Each page has a \alert{header} and a \a{footer}, containing meta-information
        \item Precise content and placement depends on the document class
    \end{itemize}

    \begin{codeexp}<+->
        \lstinline|\pagestyle{§style§}|
        \begin{itemize}[<+->]
            \item Selects a page \a{style}
            \item \lstinline|plain|: empty header, footer contains centred page number, e.\,g.\ default for chapter page
            \item \lstinline|empty|: empty header and footer, e.\,g.\ default for title page
            \item \lstinline|headings|
                \begin{itemize}
                    \item Header uses titles from \lstinline|\chapter| and \lstinline|\section| (\lstinline|book|, \lstinline|report|) or \lstinline|\section| and \lstinline|\subsection|
                    \item Footer contains centred page number
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Customising header and footer}
    \begin{codeexp}<+->
        \lstinline|\usepackage{fancyhdr}|
        \begin{itemize}
            \item Easy interface for customising header and footer
            \item Provides the new page style \lstinline|fancy|
            \item Define header and footer after setting text width
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Specifying header and footer}
    \begin{codeexp}<+->
        \lstinline|\fancyhead[§pos§]{§\dots§}|\\
        \lstinline|\fancyfoot[§pos§]{§\dots§}|
        \begin{itemize}[<+->]
            \item Place \a{\dots} at \a{pos}
            \item \a{pos}: comma-separated list of positions
                \begin{itemize}
                    \item \lstinline|L|~left, \lstinline|C|~centre, \lstinline|R|~right
                    \item \lstinline|LO|~left odd page, \lstinline|LE|~left even page, \dots, \lstinline|RE|~right even page
                \end{itemize}
            \item \a{\dots}: can contain~\lstinline|\\| and \alert{marks} (next slide)
        \end{itemize}
    \end{codeexp}

    \begin{pdflist}[listing options={firstline=3}, lefthand width=.6\textwidth, comment style={scale=1.6}]
\documentclass{article}
\usepackage[papersize={4cm,5cm}, top=2cm, bottom=2cm, left=.5cm, right=.5cm]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{HL} \fancyfoot[L]{FL}
\fancyhead[C]{HC} \fancyfoot[C]{FC}
\fancyhead[R]{HR} \fancyfoot[R]{FR}
\begin{document}
Test.
\end{document}
    \end{pdflist}
\end{frame}

\begin{frame}[fragile]{\LaTeX{} marks}
    \begin{itemize}[<+->]
        \item \lstinline|\pagestyle{header}| uses \alert{marks} for header text
        \item \lstinline|\leftmark| is the \enquote{higher-level section information}
            \begin{itemize}[<1->]
                \item Set by \lstinline|\chapter| in \lstinline|book|\slash \lstinline|report|
                \item Set by \lstinline|\section| in \lstinline|article|
            \end{itemize}
        \item \lstinline|\rightmark| is the \enquote{lower-level section information}
            \begin{itemize}[<1->]
                \item Set by \lstinline|\section| in \lstinline|book|\slash \lstinline|report|
                \item Set by \lstinline|\subsection| in \lstinline|article|
            \end{itemize}
        \item \alert{Recommendation:} only use \lstinline|\leftmark| if each \enquote{higher-level section} doesn't appear on a new page (as in \lstinline|article|)
            \begin{itemize}
                \item \LaTeX{} uses a complicated system to determine \lstinline|\leftmark| and \lstinline|\rightmark| for any given page
                \item Possible that \lstinline|\leftmark| is already \enquote{Section~3}, but \lstinline|\rightmark| is still \enquote{Subsection~2.4} on one page
            \end{itemize}
        \item For more info, see the \lstinline|fancyhdr| docs,~§15
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Specifying header and footer}
    \begin{codeexp}<+->
        \lstinline|\renewcommand{\headrulewidth}{§width§}|\\
        \lstinline|\renewcommand{\footrulewidth}{§width§}|
        \begin{itemize}[<+->]
            \item Change width of header\slash footer line to \a{width}
            \item Default header width: \lstinline|.4pt|
            \item<.-> Default footer width: \lstinline|0pt|
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\fancyheadoffset[§pos§]{§length§}|
        \begin{itemize}[<+->]
            \item Let header protrude into the margin
            \item E.\,g.\ \lstinline|\fancyheadoffset[RO,LE]{20pt}| for \lstinline|twoside|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Example header and footer}
    \begin{pdflist}[listing options={firstline=3}, lefthand width=.6\textwidth, sidebyside gap=0pt, comment style={scale=1.8}]
\documentclass{article}
\usepackage[papersize={6cm,5cm}, top=2cm, bottom=2cm, left=.5cm, right=.5cm]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
% Removing caps lock from "SECTION"
\renewcommand{\sectionmark}[1]{
    \markboth{\thesection\quad#1}{}}
\fancyhead[L]{\textit{\leftmark}}
\fancyhead[R]{My name}
\renewcommand{\headrulewidth}{0pt}
\begin{document}
\section{Section}
\end{document}
    \end{pdflist}

    \begin{exampleblock}{Note}
        For customising \lstinline|\leftmark|\slash \lstinline|\rightmark|, redefine \lstinline|\chaptermark|, \lstinline|\sectionmark|, \lstinline|\subsectionmark| with \lstinline|\markboth{§left§}{§right§}| or \lstinline|\markright{§right§}|
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Tips on setting header and footer}
    \begin{alertblock}{Tips}
        \begin{itemize}
            \item Remove header\slash footer lines by setting the width to zero
            \item Clear all fields at once with
                \begin{lstlisting}
                    \fancyhf{}
                \end{lstlisting}
            \item Be careful that fields in header\slash footer won't overlap\\
                \textrightarrow~to set the box width for \lstinline|LE|, etc., see the \lstinline|fancyhdr| docs
            \item If \lstinline|fancyhdr| complaints that \lstinline|\headheight| is to small, then set it to the recommendation\\
                E.\,g.\ \lstinline|\setlength{\headheight}{12.18195pt}|
            \item Starting page for each \lstinline|\chapter| uses \lstinline|\pagestyle{plain}|; for customisation, redefine with
                \begin{lstlisting}
                    \fancypagestyle{plain}{§fancyhdr-definition§}
                \end{lstlisting}
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Changing line spacing}
    \begin{codeexp}<+->
        \lstinline|\usepackage{setspace}|
        \begin{itemize}
            \item Easier interface to change the line spacing
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\singlespacing| (default)\\
        \lstinline|\onehalfspacing|\\
        \lstinline|\doublespacing|
        \begin{itemize}
            \item Changes the line spacing globally
            \item Must be put into the preamble
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\setstretch{§frac§}|
        \begin{itemize}
            \item Change line spacing to \a{frac}, positive float
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Customising sections and table of contents}
    \begin{block}{Recommendation}
        \lstinline|\usepackage{titlesec}|\\
        \lstinline|\usepackage{titletoc}|
        \begin{itemize}
            \item Very advanced and flexible commands to redefine any sectioning command or the table of contents
            \item Both work well together, have similar interfaces
            \item Read the docs
        \end{itemize}
    \end{block}
\end{frame}

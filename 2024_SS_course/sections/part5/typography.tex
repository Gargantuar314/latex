\section{More typography}

\begin{frame}[fragile]{Hyphenation}
    \begin{exampleblock}{Recall}<+->
        Using compound nouns with hyphens deletes all hyphenation points.
    \end{exampleblock}

    \begin{block}{Specifying custom hyphenation points}<+->
        \begin{enumerate}[<+->]
            \item Locally specify hyphenation points with~\lstinline|\-| (all breaking points need to be specified), e.\,g.\\
                \lstinline|fi\-nite-\-di\-men\-sio\-nal|
            \item Globally specify hyphenation points with \lstinline|\hyphenation{§\dots§}| (in the preamble, only for words without an explicit hyphen, all breaking points need to be specified), e.\,g.\\
                \lstinline|\hyphenation{hy-phen-ation}|
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Better hyphens with \texttt{babel}}
    \begin{codeexp}<+->
        \lstinline|\babelhyphen{§type§}|
        \begin{itemize}[<+->]
            \item \lstinline|babel| hyphens don't overwrite other hyphenation points
            \item \a{type}:
                \begin{itemize}
                    \item \lstinline|hard|: explicit hyphen with break opportunity
                    \item \lstinline|nobreak|: explicit hyphen with no break opportunity
                    \item \lstinline|soft|: possible hyphenation point
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Predefined lengths}
    \begin{itemize}[<+->]
        \item \lstinline|\baselineskip|: vertical distance between two lines in a paragraph
        \item \lstinline|\linewidth|: length of a line in local environment
        \item \lstinline|\parindent|: indent of the first line of a paragraph
        \item \lstinline|\parskip|: additional vertical space between two paragraphs
        \item \lstinline|\tabcolsep|: half the separation between two columns in \lstinline|tabular|
        \item \lstinline|\textheight|: height of text area
        \item \lstinline|\textwidth|: width of text area
    \end{itemize}

    \begin{exampleblock}{Note}<+->
        These can be multiplied by any number, e.\,g.\ \lstinline|-2.2\baselineskip|
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Spacing commands}
    \begin{codeexp}<+->
        \lstinline|\enspace|\\
        \lstinline|\quad|\\
        \lstinline|\qquad|
        \begin{itemize}
            \item Inserts a space of 0.5~em, 1~em or 2~em resp.
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\bigskip|\\
        \lstinline|\medskip|\\
        \lstinline|\smallskip|
        \begin{itemize}
            \item Inserts a big, medium or small vertical space
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Spacing commands}
    \begin{codeexp}<+->
        \lstinline|\hspace{§length§}|, \lstinline|\hspace*{§length§}|\\
        \lstinline|\vspace{§length§}|, \lstinline|\vspace*{§length§}|
        \begin{itemize}[<+->]
            \item Produces horizontal\slash vertical space of size \a{length} 
            \item Starred variant: space is not discarded if it appears at the end of a line\slash page
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\hfill|, \lstinline|\vfill|
        \begin{itemize}[<+->]
            \item Inserts maximal stretchable horizontal\slash vertical space
            \item E.\,g.\ \lstinline|a\hfill b| will push \lstinline|b| to the right margin
            \item E.\,g.\ \lstinline|a\vfill b| will push \lstinline|b| to the bottom of the page
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\addvspace{§length§}|
        \begin{itemize}[<+->]
            \item For two successive \lstinline|\addvspace|, only inserted the maximal of the \a{length}s
            \item Has to be used in \emph{vertical mode} \textrightarrow~usually placed after an explicit \lstinline|\par|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Non-breaking spaces}
    \begin{codeexp}<+->
        \lstinline|~|
        \begin{itemize}
            \item Explicit space where no line break is allowed
        \end{itemize}
    \end{codeexp}

    \begin{exampleblock}{Examples from experience}<+->
        \begin{itemize}[<+->]
            \item Single letter at the beginning of a sentence: \lstinline|I~am|, \lstinline|A~tree|
            \item Cross-references: \lstinline|chapter~12|, \lstinline|Theorem~\ref{thm:gauss}|, \lstinline|cf.~\cite{bib}|
            \item Mathematical phrases rendered as words: \lstinline|equals~\(n\)|, \lstinline|modulo~\(p^n\)|, \lstinline|is~\(15\)|, \lstinline|larger than~\(x\)|
            \item Noun and short math: \lstinline|space~\(X\)|, \lstinline|value~\(f(x)\)|
            \item Short math tightly bound to a preposition: \lstinline|of~\(x\)|, \lstinline|from \(0\) to~\(1\)|
            \item Short math in lists with conjunctions: \lstinline|\(1\),~\(2\) and~\(3\)|, \lstinline|\(u\)~and~\(v\)|
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Spacing with full stops}
    \begin{codeexp}<+->
        \lstinline|\frenchspacing|
        \begin{itemize}[<+->]
            \item By default, \LaTeX{} inserts extra space at the end of sentences (i.\,e.\ non-capital letter followed by full stop)
            \item This option disables this, i.\,e.\ all spaces are interword spaces
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline[showspaces]|\ |
        \begin{itemize}
            \item Explicit interword space
            \item Usually used after a~full stop to mark non-sentence-ending full stops
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\@§punct§|
        \begin{itemize}
            \item Insert sentence-separating space at the next space after \a{punct}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\,|
        \begin{itemize}
            \item Thin horizontal space, 1/6~em
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Spacing with full stops}
    \begin{exampleblock}{Examples from experience}<+->
        \begin{itemize}[<+->]
            \item Explicit space after abbreviations if not at the end of a sentence: \lstinline|etc.\|
            \item Thin space in abbreviations:\\
                \lstinline|w.\,l.\,o.\,g.\|\\
                \lstinline|i.\,e.\|\\
                \lstinline|e.\,g.\|
            \item Sentence space in certain constructs:\\
                \lstinline|This is Charles~III\@.|\\
                \lstinline|(A sentence\@!)|\\
                \lstinline|A~long tale~\@\dots{}|
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Spacing commands in math mode}
    \begin{tabular}{|*6{l|}} \hline
        \lstinline|\!| & \lstinline|\,| & \lstinline|\:| & \lstinline|\;| & \lstinline|\quad| & \lstinline|\qquad| \\ \hline
        -3~mu & 3~mu & 4~mu & 5~mu & 18~mu~= 1~em & 36~mu~= 2~em \\ \hline
    \end{tabular}

    \begin{code}<2->
\[f(x) \; g(x) = h(x)\]
\[ f \colon A \to B, \qquad
    x \mapsto f(x) \]
    \end{code}

    \begin{exampleblock}{Note}<3->
        Spacing in math is very intricate.
        Check out the following commands:
        \begin{itemize}
            \item \lstinline|\smash|
            \item \lstinline|\phantom|, \lstinline|\vphantom|, \lstinline|\hphantom|
            \item \lstinline|\mathstrut|
            \item From \lstinline|mathtools|: \lstinline|\smashoperator[§align§]{§op§}|, \lstinline|\adjustlimits|, \lstinline|\cramped{§\dots§}|
        \end{itemize}
    \end{exampleblock}
\end{frame}

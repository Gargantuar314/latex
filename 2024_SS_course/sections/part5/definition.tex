\section{Defining commands and environments}

\begin{frame}[fragile]{Classical commands}
    \begin{codeexp}<+->
        \lstinline|\newcommand{\§cmd§}[§numargs§][§default§]{§\dots§}|\\
        \lstinline|\renewcommand{\§cmd§}[§numargs§][§default§]{§\dots§}|
        \begin{itemize}[<+->]
            \item Defines new or redefines existing \lstinline|\§cmd§|
            \item \a{numarg}: integer between \lstinline|0|~and~\lstinline|9|
            \item If no \a{default} specified:
                \begin{itemize}
                    \item \a{numargs} is number of mandatory arguments
                    \item Use \lstinline|#1|, \lstinline|#2|, etc.\ in \a{\dots} as placeholders for arguments
                \end{itemize}
            \item If \a{default} specified:
                \begin{itemize}
                    \item one optional argument and \a{numargs}\textminus1 mandatory arguments
                    \item \lstinline|#1|~is optional argument, \lstinline|#2| etc.\ are mandatory arguments
                    \item \a{default} is default value for optional argument, can be empty
                \end{itemize}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\newcommand*{\§cmd§}[§numargs§][§default§]{§\dots§}|\\
        \lstinline|\renewcommand*{\§cmd§}[§numargs§][§default§]{§\dots§}|
        \begin{itemize}
            \item Additionally, \a{dots} must not contain paragraph breaks, i.\,e.\ empty lines or \lstinline|\par|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Classical commands}
    \begin{codeexp}<+->
        \lstinline|\newenvironment{§env§}[§numargs§][§default§]{§begin§}{§end§}|\\
        \lstinline|\renewenvironment{§env§}[§numargs§][§default§]{§begin§}{§end§}|\\
        \begin{itemize}[<+->]
            \item Defines new or redefines existing environment \a{env}
            \item \a{begin} is executed before the \a{env} body, \a{end} is executed after the \a{env} body
            \item \a{numargs} and \a{default} work like \lstinline|\newcommand|
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{New syntax}
    \begin{codeexp}<+->
        \lstinline|\NewDocumentCommand{\§cmd§}{§argspec§}{§\dots§}|\\
        \lstinline|\RenewDocumentCommand{\§cmd§}{§argspec§}{§\dots§}|
        \begin{itemize}[<+->]
            \item New addition is \a{argspec}; can consist up to nine of:
                \begin{itemize}
                    \item \lstinline|m|: mandatory argument in braces
                    \item \lstinline|r§token1§§token2§|: mandatory argument in \a{token1} \a{token2}
                    \item \lstinline|o|: optional argument in brackets with no default
                    \item \lstinline|O{§default§}|: optional argument with default
                    \item \lstinline|d§token1§§token2§|: optional argument in \a{token1} \a{token2} with no default
                    \item \lstinline|D§token1§§token2§{§default§}|: optional argument in \a{token1} \a{token2} with default
                    \item \lstinline|s|: optional star
                    \item \lstinline|t§token§|: optional \a{token}
                \end{itemize}
            \item Each of the above can be preceded by:
                \begin{itemize}
                    \item \lstinline|!|: allow paragraph breaks inside argument
                    \item \lstinline|+| (optional arguments only): disallow spaces before argument
                \end{itemize}
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{New syntax}
    \begin{codeexp}<+->
        \lstinline|\NewDocumentEnvironment{§env§}{§argspec§}{§begin§}{§end§}|\\
        \lstinline|\RenewDocumentEnvironment{§env§}{§argspec§}{§begin§}{§end§}|
        \begin{itemize}
            \item Work the same as \lstinline|\NewDocumentCommand| with \a{argspec}
        \end{itemize}
    \end{codeexp}

    \begin{block}{Advantages}<+->
        \begin{itemize}[<+->]
            \item Multiple optional arguments at arbitrary positions
            \item Starred variants possible to define
            \item Other delimiters like \lstinline|<>| possible, e.\,g.\ \lstinline|{ d<> }|
            \item Argument can be default to other optional arguments, e.\,g.\ \lstinline|{ m O{#1} }|
        \end{itemize}
    \end{block}

    \begin{exampleblock}{Technical note}<+->
        Commands defined in the new syntax are \alert{robust}, not \emph{fragile}, i.\,e.\ are protected from expansion by the \LaTeX{} engine if used inside other arguments.
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{New syntax}
    \structure{Use the following in the command or environment definition}
    \begin{codeexp}<+->
        \lstinline|\IfNoValueTF{#§arg§}{§true§}{§false§}|\\
        \lstinline|\IfNoValueT{#§arg§}{§true§}|\\
        \lstinline|\IfNoValueF{#§arg§}{§false§}|
        \begin{itemize}[<+->]
            \item \a{arg}: an argument number of an \lstinline|o|~or~\lstinline|d| type optional argument
            \item<.-> Executes \a{true} if optional argument was not given, \a{false} if it was
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\IfBlankTF{#§arg§}{§true§}{§false§}|\\
        \lstinline|\IfBlankT{#§arg§}{§true§}|\\
        \lstinline|\IfBlankF{#§arg§}{§false§}|
        \begin{itemize}[<+->]
            \item \a{arg}: an argument number of an optional argument
            \item<.-> Executes \a{true} if optional argument was empty, \a{false} if not
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{New syntax}
    \begin{codeexp}<+->
        \lstinline|\IfBooleanTF{#§arg§}{§true§}{§false§}|\\
        \lstinline|\IfBooleanT{#§arg§}{§true§}|\\
        \lstinline|\IfBooleanF{#§arg§}{§false§}|
        \begin{itemize}[<+->]
            \item \a{arg}: an argument number of an \lstinline|s|~or~\lstinline|t| type optional argument
            \item<.-> Executes \a{true} if star or token was given, \a{false} if not
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Copying commands}
    \begin{codeexp}<+->
        \lstinline|\NewCommandCopy{\§newcmd§}{\§oldcmd§}|\\
        \lstinline|\RenewCommandCopy{\§newcmd§}{\§oldcmd§}|
        \begin{itemize}[<+->]
            \item Copy the definition of existing \a{oldcmd} to \a{newcmd}
            \item Sometimes useful when redefining \lstinline|\§oldcmd§|
            \item \lstinline|\§newcmd§| is \alert{robust}
        \end{itemize}
    \end{codeexp}

    \begin{actionenv}<+->
        \begin{lstlisting}
            \newcommand{\a}{0}

            \renewcommand{\a}{0\a} % Breaks, infinite expansion

            \newcommand{\b}{\a}
            \renewcommand{\a}{0\b} % Breaks, infinite expansion

            \NewCommandCopy{\b}{\a}
            \renewcommand{\a}{0\b} % Works
        \end{lstlisting}
    \end{actionenv}
\end{frame}

\begin{frame}[fragile]{Hooks}
    \begin{codeexp}<+->
        \lstinline|\AddToHook{§hook§}{§code§}|
        \begin{itemize}
            \item Inject \a{code} into \a{hook}
        \end{itemize}
    \end{codeexp}

    \begin{codeexp}<+->
        \lstinline|\AddToHookNext{§hook§}{§code§}|
        \begin{itemize}
            \item Inject \a{code} into the next occurrence of \a{hook}
            \item Deletes the injected \a{code} after one usage
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Hooks}
    \begin{block}{Predefined \a{hook}s (selection)}<+->
        \begin{itemize}[<+->]
            \item \lstinline|cmd/§cmdname§/before|, \lstinline|cmd/§cmdname§/after| (\a{cmdname} has no backslash!)\\
                Execute code directly before\slash after \lstinline|\§cmdname§|
            \item \lstinline|env/§envname§/before|, \lstinline|env/§envname§/after|\\
                Execute code outside of \lstinline|\begin{§envname§} \end{§envname§}|
            \item \lstinline|env/§envname§/begin|, \lstinline|env/§envname§/end|\\
                Execute code directly as the first\slash last thing of \lstinline|\begin{§envname§}|\slash \lstinline|\end{§envname§}| (local scope)
            \item \lstinline|begindocument/before|, \lstinline|begindocument/end|\\
                Execute code directly before\slash after \lstinline|\begin{document}|
            \item \lstinline|enddocument|\\
                Execute code directly before \lstinline|\end{document}|
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Hooks}
    \begin{exampleblock}{Exercise}<+->
        \begin{enumerate}
            \item In \lstinline|article|, how do we start each section at a new page?
            \item How can we automatically add the title and table of contents at the document start?
        \end{enumerate}
    \end{exampleblock}

    \begin{actionenv}<+->
        \emph{Solution:} In the preamble:
        \begin{enumerate}
            \item \lstinline|\AddToHook{cmd/section/before}{\clearpage}|
            \item \lstinline|\AddToHook{begindocument/end}{\maketitle\tableofcontents}|
        \end{enumerate}
    \end{actionenv}
\end{frame}

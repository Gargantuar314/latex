\section{Customising lists}

\begin{frame}[fragile]{Customising lists}
    \begin{codeexp}<+->
        \lstinline|\usepackage[§options§]{enumitem}| (\href{https://ctan.org/pkg/enumitem}{CTAN})
        \begin{itemize}[<+->]
            \item<.-> Extensive customisation of lists with many options
            \item Extend list environments (mainly \lstinline|itemize|, \lstinline|enumerate|) with optional parameters:
                \begin{lstlisting}
                    \begin{§list§}[§key-val§]
                        §\dots§
                    \end{§list§}
                \end{lstlisting}
                \a{key-val}: comma-separated key-value list
        \end{itemize}
    \end{codeexp}
\end{frame}

\begin{frame}[fragile]{Labels and cross-referencing}
    \begin{itemize}[<+->]
        \item \lstinline|label=§label§|
            \begin{itemize}
                \item \a{label}: Symbol or number for each item, can be anything
                \item For \lstinline|enumerate|: use the following to enumerate:
                    \begin{center}
                        \begin{tabular}{*5l}
                            \lstinline|\arabic*| & \lstinline|\roman*| & 
                            \lstinline|\Roman*| & \lstinline|\alph*| & \lstinline|\Alph*| \\
                            1, 2, \dots & i, ii, \dots & I, II, \dots & a, b, \dots & A, B, \dots
                        \end{tabular}
                    \end{center}
                \item E.\,g.\ \lstinline|label=\textasteriskcentered| for~\textasteriskcentered\\
                    \phantom{E.\,g.\ }\lstinline|label=(\roman*)| for (i),~(ii),~\dots
            \end{itemize}
    \end{itemize}

    \begin{alertblock}{Tip}<+->
        With package option \lstinline|shortlabels|, we can write:
        \begin{itemize}
            \item \lstinline|(i)| instead of \lstinline|label=(\roman*)|
            \item \lstinline|a.| instead of \lstinline|label=\alph*.|
            \item Must appear as first option in \a{key-val}
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Labels and cross-referencing}
    \begin{itemize}
        \item<+-> \lstinline|ref=§format§|
            \begin{itemize}
                \item Alternative \a{format} when used with cross-referencing
                \item E.\,g.\ \lstinline|label=\arabic*.| vs.\ \lstinline|ref=\arabic*|
            \end{itemize}
        \item<+-> \lstinline|font=§font§|
            \begin{itemize}
                \item \a{font}: Additional settings for label font
                \item E.\,g.\ \lstinline|font=\normalfont| to always get upright labels
            \end{itemize}
        \item<+-> \lstinline|align=§align§|
            \begin{itemize}
                \item \a{align}: any of \lstinline|right| (default), \lstinline|left|
                \item Alignment of label text inside label box
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Horizontal spacing of labels}
    \begin{center}
        \begin{tikzpicture}
            \draw[dashed] (0,0) -- node[left] {text margin} (0,3);
            \draw (3,1.75) rectangle (5.5,2.25) node[below left] {label};
            \draw (8,.5) -- (4.5,.5) |- (6.25,1.5) node[right] {item text} |- (8,2.25);
            \draw[thick, <-] (3,2.5) -- node[above] {\lstinline|labelwidth|} (5.5,2.5);
            \draw[thick, <-] (5.5,3) -- node[above] {\lstinline|labelsep|} (6.25,3);
            \draw[thick, <-] (4.5,.25) -- node[below] {\lstinline|itemindent|} (6.25,.25);
            \draw[thick, ->] (0,2) -- node[above] {\lstinline|labelindent|} (3,2);
            \draw[thick, ->] (0,1) -- node[below] {\lstinline|leftmargin|} (4.5,1);
        \end{tikzpicture}
    \end{center}
    \begin{multline*}
        \text{\lstinline|labelindent|} + \text{\lstinline|labelwidth|} + \text{\lstinline|labelsep|} \\
        = \text{\lstinline|leftmargin|} + \text{\lstinline|itemindent|}
    \end{multline*}
\end{frame}

\begin{frame}[fragile]{Horizontal spacing of labels}
    \begin{itemize}[<+->]
        \item \lstinline|labelwidth| is automatically calculated from \lstinline|label=§label§|:\\[\itemsep]
            take \a{label}, replace \lstinline|\arabic*| by~\lstinline|0|, \lstinline|\roman*| by~\lstinline|viii|, \lstinline|\alph*| by~\lstinline|m|, etc., and set length of this string as \lstinline|labelwidth|
        \item \lstinline|left=§labelindent§|\\
            \lstinline|left=§labelindent§ .. §leftmargin§|
            \begin{itemize}
                \item \a{labelindent}, \a{leftmargin}: length, e.\,g.\ \lstinline|\parindent|
                \item \a{leftmargin} is automatically calculated if not given (recommended), otherwise \emph{label} can overlap with \emph{item text}
                \item Popular settings:\\
                    \lstinline|left=0pt|\\
                    \lstinline|left=0pt .. \parindent|\\
                    \lstinline|left=\parindent .. 2\parindent|
            \end{itemize}
        \item \lstinline|wide|\\
            \lstinline|wide=§labelindent§|
            \begin{itemize}
                \item Each item will look like a paragraph
                \item \a{labelindent}: length (default \lstinline|\parindent|) 
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Horizontal spacing of labels}
    Some alternatives:
    \begin{itemize}[<+->]
        \item \lstinline|leftmargin=*|\\
            Automatically calculate \lstinline|leftmargin|
        \item \lstinline|labelsep=*|\\
            Automatically calculate \lstinline|labelsep|
        \item \lstinline|width=§string§|
            \begin{itemize}
                \item Fake \lstinline|labelwidth| based on \a{string}, will be used for automatic calculations
                \item E.\,g.\ \lstinline|label=\arabic*), widest=00)|\\
                    \phantom{E.\,g.\ }\lstinline|label=(\roman*), widest=iii|
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Numbering and resuming}
    \begin{itemize}[<+->]
        \item \lstinline|start=§integer§|\\
            Start \lstinline|enumerate| at \a{integer} (can be negative)
        \item \lstinline|resume|\\
            Continue numbering from most recent \lstinline|enumerate|
            \begin{lstlisting}
                \begin{enumerate}
                    \item 
                \end{enumerate}
                The above examples are easy.
                \begin{enumerate}[resume]
                    \item 
                \end{enumerate}
            \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Vertical spacing}
    \begin{itemize}[<+->]
        \item \lstinline|noitemsep|\\
            No space between items
        \item \lstinline|nosep|\\
            No space between items and no space around list environment
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Global definitions}
    \begin{itemize}[<+->]
        \item \lstinline|\setlist[§type§]{§key-val§}|\\
            \lstinline|\setlist[§type§, §level§]{§key-val§}|
            \begin{itemize}
                \item \a{type}: Any of \lstinline|itemize|, \lstinline|enumerate|, \lstinline|description|
                \item \a{level}: integer between 0~and~4\\[\itemsep]
                    nesting level, none means all levels
            \end{itemize}
        \item \alert{Check out the docs!}
    \end{itemize}
\end{frame}

# LaTeX course 2024 at the University of Bonn

## PDF presentation

You can find the presentation
[here](https://gargantuar314.gitlab.io/latex/2024_SS_course/latex_presentation.pdf).

Sadly, the precise content of each talk did not went as planned, simply because
the content was excessive.
Here a precise description of each talks:

1. Talk 1: Introduction & Writing text.
2. Talk 2: Writing mathematics I (up to `diffcoeff`).
3. Talk 3: Writing mathematics II (`tikz-cd`, `amsthm`) & Other topics I (up to
   BibLaTeX).
4. Talk 4: Other topics II (TikZ and continuing).
5. Talk 5: Customising and hacking LaTeX & Live TeX-ing.

I will leave the previous version of the README file below.

## Reflection

Thanks to everyone who attended some of the talks; I had fun teaching you
something about LaTeX and hope that you can gain something from this.

Every now and then, I will probably update some parts through suggestions of
others or I will add some content since I forgot to cover them during the talks.
Checkout the PDF slides every now and then.

Most of you have probably noticed that the content was quite excessive.
The number of attendants dropped over the course and the attention span during
each talk also dropped rapidly.
Personally for me, I also noticed that I was simply too ambitious.
Preparing these talks took much longer than expected and was like hell.
If I should redo this course next year, then I will definitely only cover the
first 2 1/2 talks in a timespan of five days and make it much more interactive.
Sorry about that.

## Course objective

This is a series of five talks I will hold at the University of Bonn introducing
the *LaTeX* document preparation system.

As a student or researcher in any of the natural sciences, really, you will
probably sooner or later have to prepare a report, a thesis, a paper or even a
book.
LaTeX is very well suited for this task through its approach of structuring a
document logically, making stylising very easy, through its programmability and
portability and, last but not least, through its superb support for typography
and mathematical typesetting.
In the mathematical community at least, LaTeX is the de facto standard and even
required by many publishing houses and journals.

The goal is to introduce LaTeX in a series of five talks that cover
progressively more advanced topics, starting from the total beginner up to live
TeX-ing during the lecture.

## Organisational information

+ **When:** 2nd - 6th April 2024, 10 a.m. s.t. - 12 a.m. (meaning 10 o'clock
  sharp for probably two hours).
  If the audience desires a break, I will happily follow.
  After that, I will stay for some time (up to an hour or so) for questions,
  amongst others.
+ **Where:** Wegelerstraße 10, 53115 Bonn, University of Bonn.
  Near the Physics Department in the 'Großer Hörsaal': if you enter the
  building, go up the first set of stairs and then turn right; you should enter
  a very big lecture hall (around 250 seats large).
+ **For whom:** Anyone who is interested in learning LaTeX.
  Primarily intended for bachelor and master students of the Department of
  Mathematics.
  As of now, the students from the Department of Computer Science are welcome
  (the size of the lecture hall should not be a problem).
+ **How:** In English.
  The format will be a classic presentation with slides, which will be uploaded
  here and should serve as a reference, together with its code for the
  interested.
  During the talks, I will sprinkle some quick exercises for the attendants,
  and there will be some practical daily homework.
  Questions during the talks are very welcome.
  *However, the talks will not be streamed online* (I don't get paid enough for
  this).
+ **What:** An introduction to LaTeX, basically from zero to hero.
  Topics included ~(as of now)~:
  + A bit of history and the rationale of LaTeX.
  + Basic commands for typing a document.
  + Extensive explanation on typing mathematics, including `tikz-cd` for
    commutative diagrams and `diffcoef` for derivatives.
    Other packages for other areas of mathematics can be requested: I will
    consider covering them.
  + Basic usage of BibLaTeX for bibliographies, indexing, creating drawings and
    diagrams in TikZ.
  + Debugging some of the common compiler errors.
  + Writing pseudo code ~or programs in other languages (this is for you,
    computer scientists)~.
  + Hacking and programming LaTeX.
  + Customising page layouts.
  + Some typography here and there.
  + Pro level: **live TeX-ing during the lecture.**
    Maybe a good example on how to use Neovim.

## Questions

### Who are you?

I'm Tien from the Department of Mathematics at the University of Bonn, currently
a bachelor student.
I would describe myself as a LaTeX enthusiast and use LaTeX probably since ninth
grade.
Currently, I write almost everything related to my studies in LaTeX, even typing
my lecture notes live during the lecture.

You might know me as the author of some of the lecture notes I created.
The lecture notes were published for:

+ *Analysis II* in summer term 2022, held by Prof. Dr. Sturm,
+ *Algebra I* in summer term 2023, held by Dr. Mihatsch,
+ *Einführung in die Algebra* in winter term 2023/24, held by Prof. Dr.
  Huybrechts (these notes were not endorsed by the lecturer);
+ I once publicly uploaded all my lecture notes from my first semester (now
  private);
+ I give access to my private lecture notes to close friends and acquaintances.

The first two should still be online on eCampus, if you want to get an
impression.
(Stylewise, I only did the coloured boxes as it was requested by some students
and many probably liked this; I personally am not a fan.)

### I'm interested in the content of this course, but do I really need to physically come to the talks?

No, you don't have to.

In fact, probably everything important that I will say can be found on the
slides (regarding live TeX-ing, I'm not so sure).
If you think that it suffices to go through these slides alone, then I don't
want to waste your time.

However, you will miss out on the following:
+ Experiencing me as a person (I actually don't know what I mean by that).
+ The possibility to ask questions, either on the course content or on related
  LaTeX topics.
+ Me helping you on various LaTeX problems (e.g. installation of a LaTeX
  distribution, how to solve certain bugs, ideas on how to implement certain
  things) after each talk.

### I only want to learn the bare minimum on LaTeX to survive/I'm only interested in the more advanced stuff. Do I really need to attend all talks?

Again: no, you don't have to, as I don't want to waste your time.

The content of the talks will be grouped by 'advanced-ness' or difficulty; feel
free to visit the ones you find interesting.
As of now, the plan for the talks are the following:

1. Talk 1: Introduction and typing text (novice).
2. Talk 2: Typing mathematics (novice).
3. Talk 3: Creating bibliographies, indices, drawing diagrams (advanced).
4. Talk 4: Customising layout and hacking LaTeX (expert).
5. Talk 5: Live TeX-ing (pro).

I strongly recommend for any beginner to attend talks 1 and 2.
As for talks 3, it might be helpful (you never know), especially if you want to
write a whole thesis.

### Do I need to bring my device/laptop/tablet?

No, you don't have to.

However, I strongly recommend that you bring your laptop to the first talks so
that I can help you install a LaTeX distribution (and probably others can help
you too, e.g. since I have no clue about Apple's Mac OS).
Make sure to also bring your charger as many modern CPUs can be overclocked
while plugged in (from experience, the installation takes quite a while,
especially on old laptops).

For the other talks, you can still bring your device to solve some of the
homework or ask me anything LaTeX related.

### Thank you for this course! I still have some questions/I need some tech (TeX) support. Can I contact you somehow and ask you for help?

Of course, I'm happy to help.
You can write me an e-mail at <tien.nguyen@uni-bonn.de>, write me a message
(with code!) via Signal or find me in the 'Aufenthaltsraum' (student lounge) in
the secondary building of the Mathematics Centre.
Note however that I can only help you with problems within the ecosystems that I
personally use.
These are *pdfLaTeX* with the default document classes, some *Neovim* with
*VimTeX* and *Luasnip*, as well as  *Windows* and *Manjaro* (Arch-based
systems).
Everything outside of this, e.g. *Mac OS*, *LuaLaTeX*, *XeTeX*, *ConTeXt*,
*Emacs*, *KOMA-script*, `memoir`, `bibtex`, etc., I probably cannot help you
with.

## Licensing

The parts of this project are licensed differently:

+ the source code (including the physical PDF) is licensed under the Apache
  License 2.0 (among others, this says that private usage is allowed),
+ the content of the PDF slides is licensed under the CC BY-NC-SA 4.0 (in
  particular, you should give credit to me),
+ all code examples in the PDF slides are licensed under the CC0 1.0 (i.e. do
  whatever the hell you want with them).

Regarding the images:

+ [Kirby](https://gitlab.com/Gargantuar314/latex/-/blob/main/2024_SS_course/kirby.png)
  was taken from
  [Wikipedia](https://en.wikipedia.org/wiki/File:SSU_Kirby_artwork.png).
  All rights of the *Kirby* franchise belong to the respective owners/producers,
  most notably Nintendo Co., Ltd.
+ All other pictures are my own work, and hence are licensed under the Apache
  License 2.0 as well.

## Compiling these files

In order to compile this, *shell escape* must be enabled, e.g. `-shell-escape`
in `pdflatex`.
If you use `latexmk`, then you should also add the option

```shell
-e '$hash_calc_ignore_pattern{q/pdf/} = q/(CreationDate|ModDate|ID) /;'
```

in order to avoid infinite compilation loops (courtesy to
[this question on TeX SE](https://tex.stackexchange.com/a/495681)).

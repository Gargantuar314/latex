for FILE in $(find . -type f -name '*.tex')
do
    # fix: avoid '\section' on the first three lines in file
    if head $FILE | grep '^\\documentclass' &&
        ! head -3 $FILE | grep '^\\section'
    then
        latexmk -cd -pdf -interaction=nonstopmode -shell-escape \
        -e '$hash_calc_ignore_pattern{q/pdf/} = q/(CreationDate|ModDate|ID) /;' \
        -pdflatex='pdflatex %O "\PassOptionsToClass{handout}{beamer}\input{%S}"' \
        $FILE
    fi
done
